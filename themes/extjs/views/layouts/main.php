<?php
$baseUrl = Yii::app()->theme->baseUrl;
$basePath = Yii::app()->theme->basePath;
$files = array();
foreach (glob($basePath .'/images/wallpapers/*.jpg') as $path) {
    $files[] = pathinfo($path, PATHINFO_BASENAME);
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <base href="<?php echo Yii::app()->createAbsoluteUrl('/'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php
        $theme = 'neptune';
        $settings = json_encode($this->settingsModel->attributes);
        Yii::app()->getClientScript()
            ->registerCssFile($baseUrl . '/css/style.css', 'screen, projection')
            ->registerCssFile($baseUrl . '/css/desktop.css', 'screen, projection')
            ->registerCssFile($baseUrl . '/css/rating.css', 'screen, projection')
            ->registerCssFile($baseUrl . '/css/data-view.css', 'screen, projection')
            ->registerCssFile('//cdn.sencha.io/ext/gpl/4.2.1/resources/css/ext-all-neptune.css', 'screen, projection')

            ->registerScript('settings', "Ext.UserSettings = {$settings};", CClientScript::POS_HEAD)
            ->registerScriptFile('https://maps.googleapis.com/maps/api/js?v=3&sensor=false&libraries=places&language=ru', CClientScript::POS_HEAD)
            ->registerScriptFile('//cdn.sencha.com/ext/gpl/4.2.1/ext-all.js', CClientScript::POS_HEAD)

            ->registerScriptFile($baseUrl . '/js/Math.uuid.js', CClientScript::POS_HEAD)
            ->registerScriptFile($baseUrl . '/js/functions.js', CClientScript::POS_HEAD)
            ->registerScriptFile($baseUrl . '/js/app.js', CClientScript::POS_END)
            ->registerScriptFile('//code.jquery.com/jquery-1.8.2.min.js', CClientScript::POS_END)
            ;
    ?>

    <script type="text/javascript">
        Ext.domain = '.<?php echo Yii::app()->params['site']; ?>';
        Ext.baseUrl = '<?php echo $baseUrl; ?>';
        Ext.Wallpaper = {Files: <?php echo json_encode($files); ?>};
        <?php if (!Yii::app()->user->isGuest): ?>
        Ext.User = <?php echo json_encode(Yii::app()->user->getProfile()->attributes); ?>;
        <?php endif; ?>
        Ext.Loader.setPath({
            'Ext.ux': Ext.baseUrl + '/js/extjs/src/ux/'
        });
        Ext.onReady(function () {
            Ext.Loader.setConfig({enabled: true});
        });
    </script>
</head>
<body>
<?php echo $content; ?>
</body>
</html>