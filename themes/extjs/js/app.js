Ext.application({
    name: 'Help',
    appFolder: Ext.baseUrl + '/js/app',
    requires: [
        'Help.desktop.Conf',
        'Help.login.Window',
        'Help.Wallpaper.Settings'
//        'Help.login.LostPassWindow',
//        'Help.login.NewPassWindow'
    ],

    showDesktop: function () {
        Ext.require('Help.desktop.App');
    },

    showLogin: function () {
        var win = Ext.create('Help.login.Window');
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: [win]
        });

        win.show();
    },

    /*showPasswordChange: function () {
     var win = Ext.create('Help.login.NewPassWindow');
     Ext.create('Ext.container.Viewport', {
     layout: 'fit',
     items: [win]
     });

     win.show();
     },*/


    launch: function () {
        var me = this;
        var parameters = {};

        var elem = document.getElementById('global-loading');
        if (elem) {
            elem.parentNode.removeChild(elem);
        }

//        if (Ext.hasOwnProperty('User')) {
            me.showDesktop();
//        } else if (Ext.hasOwnProperty('isRecovery')) {
////            me.showPasswordChange();
//        } else {
//            me.showLogin();
//        }

        Help.log = function () {
            if (!window.console) {
                return;
            }
            console.log.apply(this, arguments);
        };

        Ext.override(Ext.grid.ViewDropZone, {
            handleNodeDrop: function (data, record, position) {
                var me = this,
                    view = me.view,
                    store = view.getStore(),
                    index, records, i, len;

                index = store.indexOf(record);

                if (position !== 'before') {
                    index++;
                }

                Ext.each(data.records, function (r) {
                    if (me.view.fireEvent('dndbeforeadd', r, view) === false) return;
                    store.insert(index++, r);
                });

                if (data.copy) {
                    records = data.records;
                    data.records = [];
                    for (i = 0, len = records.length; i < len; i++) {
                        data.records.push(records[i].copy(records[i].getId()));
                    }
                } else {
                    Ext.each(data.records, function (r) {
                        if (data.view.fireEvent('dndbeforeremove', r, view) === false) return;
                        data.view.store.remove(r, data.view === view);
                    });
                }
            }
        });
    }
});