Ext.define('Help.desktop.App', {
        requires: [
            'Help.desktop.Desktop',
            'Help.desktop.Module',
            'Help.desktop.Conf'/*,
             'Help.utils.ServerEvents'*/
        ],
        mixins: {
            observable: 'Ext.util.Observable'
        },

        modules: null,

        parameters: {},
        lastCtxMenu: null,

        conf: Ext.create('Help.desktop.Conf'),

        constructor: function (config) {
            var me = this;
            me.mixins.observable.constructor.call(this, config);

            if (Ext.isReady) {
                Ext.Function.defer(me.init, 10, me);
            } else {
                Ext.onReady(me.init, me);
            }
        },

        init: function () {
            var me = this;
            Ext.QuickTips.init();
            me.modules = me.getModules();
            me.initModules(me.modules);
            me.desktop = Ext.widget('desktop', {
                app: this,
                wallpaper: Ext.UserSettings.wallpaper,
                wallpaperStretch: Ext.UserSettings.wallpaper_stretch != 0

            });

            me.viewport = Ext.create('Ext.container.Viewport', {
                layout: 'fit',
                items: [me.desktop]
            });


            // Handle loading mask for windows correctly
            var winProtototype = Ext.window.Window.prototype.setLoading;
            Ext.window.Window.prototype.setLoading = function () {
                var me = this;
                var args = arguments;
                var delay = 1;
                if (arguments.length && arguments[0] === false) {
                    delay += 20;
                }
                setTimeout(function () {
                    winProtototype.apply(me, args);
                }, delay);
            };

            // Handle loading mask for formPanels correctly
            var formProtototype = Ext.form.Panel.prototype.setLoading;
            Ext.form.Panel.prototype.setLoading = function () {
                var me = this;
                var args = arguments;
                var delay = 1;
                if (arguments.length && arguments[0] === false) {
                    delay += 20;
                }
                setTimeout(function () {
                    formProtototype.apply(me, args);
                }, delay);
            };

            // Custom vTypes
            Ext.apply(Ext.form.field.VTypes, {
                // percent
                percent: function (val, field) {
                    return /^([0-9]{1,2}|100)$/.test(val);
                },
                percentText: 'Percent value should be in 0..100 range',

                // unsigned integer
                uint: function (val, field) {
                    return /^[0-9]+$/.test(val);
                },
                unitText: 'Значение должно быть 0 или выше.',
                unitMask: /[0-9]/,

                // skype
                skype: function (val, field) {
                    return /[a-zA-Z][a-zA-Z0-9_\-\,\.]{5,31}/.test(val);
                },
                skypeText: 'Неправильный ник Skype',

                // site
                site: function (val, field) {
                    return /^(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?$/.test(val);
                },
                siteText: 'Не валидный URL сайта.',

                landline: function (val, field) {
                    return /^\d{9}$/.test(val);
                },
                landlineText: 'Невалидный городской номер телефона. Должен быть формата 022123456',

                fax: function (val, field) {
                    return /[\+? *[1-9]+]?[0-9 ]+/.test(val);
                },
                faxText: 'Невалидный формат факса',

                //mobile
                mobile: function (val, field) {
                    return /^\+[1-9]{1}[0-9]{7,12}$/.test(val);
                },
                mobileText: 'Невалидный мобильный номер телефона. Должен быть формата +380123456789'
            });
        },

        getModules: function () {
            return Ext.create('Help.ModulesList').get();
        },

        initModules: function (modules) {
            var me = this;
            Ext.each(modules, function (module) {
                module.app = me;
            });
        },

        getModule: function (name) {
            var ms = this.modules;
            for (var i = 0, len = ms.length; i < len; i++) {
                var m = ms[i];
                if (m.id == name) {
                    return m;
                }
            }
            return null;
        },

        getDesktop: function () {
            return this.desktop;
        },

        ctxMenu: function (init) {
            var me = this;
            if (me.lastCtxMenu) {
                me.lastCtxMenu.destroy();
            }
            me.lastCtxMenu = Ext.create('Ext.menu.Menu', init);
            return me.lastCtxMenu;
        },

        log: function () {
            if (window.console) {
                console.log(arguments);
            }
        },

        /**
         *
         * @param num 1
         * @param str1 Комментарий
         * @param str2 комментария
         * @param str3 комментариев
         * @returns {string}
         */
        getMultipleStr: function (num, str1, str2, str3) {
            var $val = num % 100;

            if ($val > 10 && $val < 20) {
                return num + ' ' + str3;
            } else {
                $val = num % 10;
                if ($val == 1) {
                    return num + ' ' + str1;
                } else if ($val > 1 && $val < 5) {
                    return num + ' ' + str2;
                }
                else
                    return num + ' ' + str3;
            }
        },

        errorMessage: function (message) {
            var _messages = [];

            if (typeof message == 'object') {
                for (var i in message) {
                    if (!message.hasOwnProperty(i)) {
                        continue;
                    }

                    if (message.data[i] instanceof Array) {
                        _messages.push(message.data[i].join('\r\n'));
                    } else {
                        _messages.push(message.data[i]);
                    }
                }
                message = _messages.join('\r\n');
            } else if (message instanceof Array) {
                for (var j in message) {
                    if (!message.hasOwnProperty(j)) {
                        continue;
                    }

                    _messages.push(message[j]);
                }

                message = _messages.join('\r\n');
            }

            Ext.MessageBox.show({
                title: 'Сообщение',
                msg: message,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR
            });
        }

    },
    function () {
        window.Help = Ext.create('Help.desktop.App'/*, {
         parameters: window.appParams
         }*/);
        window.app = window.Help;

//    app.ServerEvents = Ext.create('Help.utils.ServerEvents');
    }
)
;