Ext.define('Help.desktop.Desktop', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.desktop',
    id: 'desktop',
    requires: [
        'Help.desktop.TopBar',
        'Help.desktop.TaskBar',
        'Help.desktop.DesktopShortcutsStore',
        'Help.ModulesList'
    ],
    uses: [
        'Ext.ux.desktop.Wallpaper'
    ],
    activeWindowCls: 'ux-desktop-active-win',
    inactiveWindowCls: 'ux-desktop-inactive-win',
    lastActiveWindow: null,
    border: false,
    html: '&#160;',
    layout: 'fit',
    xTickSize: 1,
    yTickSize: 1,
    shortcuts: [],
    shortcutItemSelector: 'div.ux-desktop-shortcut',
    shortcutTpl: [
        '<tpl for=".">',
        '<div class="ux-desktop-shortcut" id="{name}-shortcut">',
        '<div class="ux-desktop-shortcut-icon {iconCls}">',
        '<img src="', Ext.BLANK_IMAGE_URL, '" title="{name}">',
        '</div>', '<span class="ux-desktop-shortcut-text">{name}</span>',
        '</div>',
        '</tpl>',
        '<div class="x-clear"></div>'
    ],
    windowMenu: null,
    panelsHidden: false,
    shortcutsModulesIds: [],

    initComponent: function () {
        var me = this;

        me.shortcutsModulesIds = ['tabModule', 'galleryModule', 'commentsModule', 'userModule', 'filterModule', 'settingsModule'];
        var shortcutsData = [];
        Ext.each(me.shortcutsModulesIds, function (moduleId) {
            var module = me.app.getModule(moduleId);
            shortcutsData.push({
                name: module.launcher.text,
                iconCls: module.launcher.iconCls + '-big',
                module: moduleId
            });
        });

        me.shortcuts = Ext.create('Help.desktop.DesktopShortcutsStore', {
            data: shortcutsData
        });

        me.windowMenu = Ext.create('Ext.menu.Menu', me.createWindowMenu());

        me.tbar = Ext.create('Help.desktop.TopBar', {
            app: this.app
        });

        me.topBar = me.tbar;
        me.bbar = me.taskbar = Ext.create('Help.desktop.TaskBar');
        me.taskBar = me.bbar;
        me.taskbar.windowMenu = me.windowMenu;
        me.windows = Ext.create('Ext.util.MixedCollection');
        me.contextMenu = Ext.create('Ext.menu.Menu', me.createDesktopMenu());
        me.items = [
            {xtype: 'wallpaper', id: me.id + '_wallpaper'},
            me.createDataView()
        ];

        me.callParent();
        me.shortcutsView = me.items.getAt(1);
        me.shortcutsView.on('itemclick', me.onShortcutItemClick, me);

//        me.on('render', function () {
//            $('#globalDataView').append('<div id="desktop-status"></div>');
//            $.post('../adw/users/update-desktop-status');
//        });

        var wallpaper = me.wallpaper;
        me.wallpaper = me.items.getAt(0);
        if (wallpaper) {
            me.setWallpaper(wallpaper, me.wallpaperStretch);
        }
    },

    getWallpaper: function () {
        return this.wallpaper.wallpaper;
    },

    setWallpaper: function (wallpaper, stretch) {
        this.wallpaper.setWallpaper(wallpaper, stretch);
        return this;
    },

    afterRender: function () {
        var me = this;
        me.callParent();
        me.el.on('contextmenu', me.onDesktopMenu, me);
        var modules = {};
    },

    createDataView: function () {
        var me = this;
        return {
            xtype: 'dataview',
            id: 'globalDataView',
            overItemCls: 'x-view-over',
            trackOver: true,
            itemSelector: me.shortcutItemSelector,
            store: me.shortcuts,
            style: {
                position: 'absolute'
            },
            x: 0, y: 0,
            tpl: Ext.create('Ext.XTemplate', me.shortcutTpl)
        };
    },

    createDesktopMenu: function () {
        var me = this, ret = {
            items: /*me.contextMenuItems || */[]
        };

        Ext.each(me.shortcutsModulesIds, function (moduleId) {
            var module = me.app.getModule(moduleId);
            ret.items.push({
                text: module.launcher.text,
                iconCls: module.launcher.iconCls,
                handler: module.launcher.handler,
                scope: me
            });
        });

        ret.items.push(
            { text: 'Изменить настройки', handler: me.onSettings, scope: me}
        );

        if (ret.items.length) {
            ret.items.push('-');
        }

        ret.items.push(
            { text: 'Tile', handler: me.titleWindows, scope: me, minWindows: 1 },
            { text: 'Cascade', handler: me.cascadeWindows, scope: me, minWindows: 1 }
        );

        return ret;
    },

    onSettings: function () {
        var me = this;
        var win = new Ext.create('Help.Wallpaper.Settings',{
            desktop: me
        });
        win.show();
    },

    createWindowMenu: function () {
        var me = this;
        return {
            defaultAlign: 'br-tr',
            items: [
                {
                    text: 'Restore',
                    handler: me.onWindowMenuRestore,
                    scope: me
                },
                {
                    text: 'Minimize',
                    handler: me.onWindowMenuMinimize,
                    scope: me
                },
                {
                    text: 'Maximize',
                    handler: me.onWindowMenuMaximize,
                    scope: me
                },
                '-',
                {
                    text: 'Close',
                    handler: me.onWindowMenuClose,
                    scope: me
                }
            ],
            listeners: {
                beforeshow: me.onWindowMenuBeforeShow,
                hide: me.onWindowMenuHide,
                scope: me
            }
        };
    },

    onDesktopMenu: function (e) {
        var me = this, menu = me.contextMenu;
        e.stopEvent();
        if (!menu.rendered) {
            menu.on('beforeshow', me.onDesktopMenuBeforeShow, me);
        }
        menu.showAt(e.getXY());
        menu.doConstrain();
    },

    onDesktopMenuBeforeShow: function (menu) {
        var me = this, count = me.windows.getCount();

        menu.items.each(function (item) {
            var min = item.minWindows || 0;
            item.setDisabled(count < min);
        });
    },

    onShortcutItemClick: function (dataView, record) {
        var me = this, module = me.app.getModule(record.data.module), win = module && module.createWindow();
        if (win) {
            me.restoreWindow(win);
        }
    },

    onWindowClose: function (win) {
        var me = this;
        me.windows.remove(win);
        me.taskbar.removeTaskButton(win.taskButton);
        me.updateActiveWindow();
    },

    onWindowMenuBeforeShow: function (menu) {
        var items = menu.items.items, win = menu.theWin;
        items[0].setDisabled(win.maximized !== true && win.hidden !== true); // Restore
        items[1].setDisabled(win.minimized === true); // Minimize
        items[2].setDisabled(win.maximized === true || win.hidden === true); // Maximize
    },

    onWindowMenuClose: function () {
        var me = this, win = me.windowMenu.theWin;
        win.close();
    },

    onWindowMenuHide: function (menu) {
        menu.theWin = null;
    },

    onWindowMenuMaximize: function () {
        var me = this, win = me.windowMenu.theWin;
        win.maximize();
        win.toFront();
    },

    onWindowMenuMinimize: function () {
        var me = this, win = me.windowMenu.theWin;
        win.minimize();
    },

    onWindowMenuRestore: function () {
        var me = this, win = me.windowMenu.theWin;
        me.restoreWindow(win);
    },

    setTickSize: function (xTickSize, yTickSize) {
        var me = this, xt = me.xTickSize = xTickSize, yt = me.yTickSize = (arguments.length > 1) ? yTickSize : xt;
        me.windows.each(function (win) {
            var dd = win.dd, resizer = win.resizer;
            dd.xTickSize = xt;
            dd.yTickSize = yt;
            resizer.widthIncrement = xt;
            resizer.heightIncrement = yt;
        });
    },

    cascadeWindows: function () {
        var x = 0, y = 0, zmgr = this.getDesktopZIndexManager();
        zmgr.eachBottomUp(function (win) {
            if (win.isWindow && win.isVisible() && !win.maximized) {
                win.setPosition(x, y);
                x += 20;
                y += 20;
            }
        });
    },

    addWindow: function (win, ignore) {
        this.add(win);
        this.windows.add(win);
        win.taskButton = this.taskbar.addTaskButton(win);
        win.animateTarget = null;

        win.on({
            activate: this.updateActiveWindow,
            beforeshow: this.updateActiveWindow,
            deactivate: this.updateActiveWindow,
            minimize: this.minimizeWindow,
            destroy: this.onWindowClose,
            move: this.moveWindow,
            scope: this
        });

        win.on({
            boxready: function () {
                win.dd.xTickSize = this.xTickSize;
                win.dd.yTickSize = this.yTickSize;
                if (win.resizer) {
                    win.resizer.widthIncrement = this.xTickSize;
                    win.resizer.heightIncrement = this.yTickSize;
                }
            },
            single: true
        });

        win.doClose = function () {
            win.doClose = Ext.emptyFn;
            win.el.disableShadow();
            win.destroy();
        };

        return win;
    },

    moveWindow: function (win, x, y) {

    },

    createWindow: function (config, cls) {
        var me = this, win, cfg = Ext.applyIf(config || {}, {
            stateful: false,
            isWindow: true,
            constrainHeader: true,
            minimizable: true,
            maximizable: true
        });
        cls = cls || Ext.window.Window;
        win = me.add(Ext.create(cls, cfg));
        return me.addWindow(win);
    },

    getActiveWindow: function () {
        var win = null, zmgr = this.getDesktopZIndexManager();
        if (zmgr) {
            zmgr.eachTopDown(function (comp) {
                if (comp.isWindow && !comp.hidden) {
                    win = comp;
                    return false;
                }
                return true;
            });
        }
        return win;
    },

    getDesktopZIndexManager: function () {
        var windows = this.windows;
        return (windows.getCount() && windows.getAt(0).zIndexManager) || null;
    },

    getWindow: function (id) {
        return this.windows.get(id);
    },

    minimizeWindow: function (win) {
        win.minimized = true;
        win.hide();
    },

    restoreWindow: function (win) {
        if (win.isVisible()) {
            win.restore();
            win.toFront();
        } else {
            win.show();
        }
        return win;
    },

    updateActiveWindow: function () {
        var me = this, activeWindow = me.getActiveWindow(), last = me.lastActiveWindow;
        if (activeWindow === last) {
            return;
        }
        if (last && last.el) {
            if (last.el.dom) {
                last.addCls(me.inactiveWindowCls);
                last.removeCls(me.activeWindowCls);
            }
            last.active = false;
        }
        me.lastActiveWindow = activeWindow;
        if (activeWindow) {
            activeWindow.addCls(me.activeWindowCls);
            activeWindow.removeCls(me.inactiveWindowCls);
            activeWindow.minimized = false;
            activeWindow.active = true;
        }
        me.taskbar.setActiveButton(activeWindow && activeWindow.taskButton);
    },

    hidePanels: function (action) {
        var me = this;
        if (action) {
            me.topBar.setDisabled(true);
            me.taskBar.setDisabled(true);
            me.removeDocked(me.topBar, false);
            me.removeDocked(me.taskBar, false);
        } else {
            me.addDocked(me.topBar, 0);
            me.addDocked(me.taskBar, 1);
            me.doLayout();
            me.topBar.setDisabled(false);
            me.taskBar.setDisabled(false);
        }
        me.panelsHidden = action;
    }
});