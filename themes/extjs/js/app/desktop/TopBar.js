Ext.define('Help.desktop.TopBar', {
    extend: 'Ext.toolbar.Toolbar',
    cls: 'ux-taskbar',
    requires: [
//        'SS.desktop.AdwordsMenu',
//        'SS.desktop.SettingsMenu'
    ],

    initComponent: function () {
        var me = this;
        var leftTopLogo = Ext.create('Ext.Component', {
//            html: '<img src="images/runashop-topbar-logo.png" />',
            html: 'Help.md'
        });

        me.items = [leftTopLogo, '-'];
        me.items.push('->');

        var paItems = [
            /*{
                text : 'Price Portals',
                tab : 'portals',
                iconCls : 'domain-icon'
            },
            {
                text : 'Stopwords',
                tab : 'stopwords',
                iconCls : 'stopwords-icon'
            },
            {
                text : 'Ignorewords',
                tab : 'ignorewords',
                iconCls : 'ignorewords-icon'
            },
            {
                text : 'Aliases',
                tab : 'aliases',
                iconCls : 'aliases-icon'
            },
            {
                text : 'Countries',
                tab : 'countries',
                iconCls : 'countries-icon'
            },
            {
                text : 'Merchants',
                tab : 'merchants',
                iconCls : 'merchants-icon'
            },
            {
                text : 'Categories',
                tab : 'categories',
                iconCls : 'categories-icon'
            }*/
        ];

        /*var paMenu = [];
        Ext.Array.each(paItems, function(e){
            paMenu.push({
                text : e.text,
                iconCls : e.iconCls,
                handler : function() {
                    var module = me.app.getModule('portalsModule');
                    module.app = me.app;
                    var win = module.createWindow();
                    win.setActiveTab(e.tab);
                }
            })
        });

        me.items.push(
            {
                iconCls: 'portals-icon',
                text: 'Portals Admin',
                menu : paMenu
            },
            '-',
            {
                xtype: 'button',
                iconCls: 'google-icon',
                menu: Ext.create('SS.desktop.AdwordsMenu', {
                    app: me.app
                }),
                text: 'Google Adwords'
            }
        );

        me.items.push('-');

        me.items.push(
            {
                xtype: 'button',
                iconCls: 'settings-icon',
                menu: Ext.create('SS.desktop.SettingsMenu', {
                    app: me.app
                }),
                text: 'Settings'
            }
        );
         */
//        me.items.push('-');
//
//        me.items.push(
//            {
//                xtype: 'button',
//                iconCls: 'logout-icon',
//                text: 'Выйти (' + Ext.User.nick_name + ')',
//                handler: function () {
//                    Ext.Msg.confirm('Потверждение выхода', 'Вы действительно хотите выйти?', function (btn) {
//                        if (btn == 'yes') {
//                            Ext.Ajax.request({
//                                url: '/user/account/logout',
//                                success: function (/*response*/) {
//                                    window.location.reload();
//                                }
//                            });
//                        }
//                    });
//                }
//            }
//        );

        this.callParent();
        this.on('render', function () {});
    }
});