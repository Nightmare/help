Ext.define('Help.desktop.DesktopShortcutsStore', {
    extend : 'Ext.data.Store',
    fields: [ 'name', 'iconCls', 'module']
});