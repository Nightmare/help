Ext.define('Help.desktop.Conf', {
    dateFormat: 'd.m.Y',
    dateTimeFormat: 'd.m.Y H:i:s',
    timeFormat: 'H:i:s'
});

window.conf = Ext.create('Help.desktop.Conf');