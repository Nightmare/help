Ext.define('Help.desktop.Module', {
    options: {},
    mixins: {
        observable: 'Ext.util.Observable'
    },

    constructor: function(config) {
        this.mixins.observable.constructor.call(this, config);
        this.init();
    },

    init: Ext.emptyFn,

    createWindow: function(options) {
        if (options == undefined || Ext.getClass(options) !== null) {
            options = {
                auto: false,
                moduleId: this.id,
                uuid: Math.uuid(15)
            }
        }
        if (options.moduleId == undefined) options.moduleId = this.id;
        if (options.uuid == undefined) options.uuid = Math.uuid(15);
        this.options = options;
    },

    updateOptions: function(options) {

    }
});