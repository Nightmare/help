Ext.define('Help.Gallery.Window', {
    extend: 'Ext.window.Window',
    require: [
        'Ext.ux.form.SearchField',
        'Help.Gallery.Panel'
    ],
    id: 'gallery-win',
    cls: 'gallery-win',
    title: 'Фотогалерея',
    width: 785,
    height: 480,
    resizable: false,
    iconCls: 'tabs',
    animCollapse: false,
    border: false,
    constrainHeader: true,
    layout: 'fit',
    items: [],
    initComponent: function () {
        var me = this;
        me.callParent(arguments);
        me.add(Ext.create('Help.Gallery.Panel'));
    }
});