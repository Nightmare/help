Ext.define('Help.Gallery.View', {
    extend: 'Ext.view.View',
    reuqire: [
        'Ext.ux.DataView.DragSelector',
        'Ext.ux.DataView.LabelEditor'
    ],
    tpl: [
        '<tpl for=".">',
        '<div class="thumb-wrap" id="{title}">',
        '<div class="thumb">',
        '<div class="is-main">',

        '<tpl if="selected">',
        '<input type="radio" name="is_main" value={id} checked="checked" />',
        '</tpl>',
        '<tpl if="selected == false">',
        '<input type="radio" name="is_main" value={id} />',
        '</tpl>',
        '</div>',
        '<img src="{path}" title="{title}">',
        '</div>',
        '<span class="x-editable">{title}</span></div>',
        '</tpl>',
        '<div class="x-clear"></div>'
    ],
    multiSelect: true,
    trackOver: true,
    overItemCls: 'x-item-over',
    itemSelector: 'div.thumb-wrap',
    emptyText: 'Нет фотографий',
    layout: 'fit',
    selModel: {
        mode: 'SIMPLE'
    },
    listeners: {
        selectionchange: function (dv, nodes) {
            this.up('window').setTitle('фотогалерея: ' + window.app.getMultipleStr(nodes.length, 'фотографию', 'фотографии', 'фотографий') + ' выделено');
        }
    },
    initComponent: function () {
        var me = this;
        me.plugins = [
            Ext.create('Ext.ux.DataView.DragSelector', {}),
            Ext.create('Ext.ux.DataView.LabelEditor', {
                dataIndex: 'title',
                allowBlank: false,
                listeners: {
                    complete: function (ed, value, oldValue, event) {
                        if (value == oldValue) {
                            return;
                        }

                        var record = ed.activeRecord;
                        Ext.Ajax.request({
                            url: '/admin/gallery/save',
                            params: {
                                id: record.get('id'),
                                title: value
                            },
                            success: function (result) {
//                            if (result) {
//                            record.setValue(title, oldValue);
//                            }
                            },
                            failure: function (response) {
                                window.app.errorMessage(response.responseText);
                                record.setValue('title', oldValue);
                            }
                        });
                    }
                }
            })
        ];
        me.callParent(arguments);
    }
});