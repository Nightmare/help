Ext.define('Help.Gallery.ImageStore', {
    extend: 'Ext.data.Store',
    require: [
        'Help.Gallery.ImageModel'
    ],
    model: 'Help.Gallery.ImageModel',
    pageSize: 10
});
