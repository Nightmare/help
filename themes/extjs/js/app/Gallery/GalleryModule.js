Ext.define('Help.Gallery.GalleryModule', {
    extend: 'Help.desktop.Module',
    require: [
        'Help.Gallery.Window'
    ],

    id: 'galleryModule',

    init: function () {
        this.launcher = {
            text: 'Фотогалерея',
            iconCls: 'photo-gallery-icon',
            handler: this.createWindow,
            scope: this
        };
    },

    createWindow: function () {
        this.callParent(arguments);
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('gallery-win');
        if (!win) {
            win = desktop.addWindow(Ext.create('Help.Gallery.Window', {
                options: this.options
            }));
        }
        return win;
    }
});