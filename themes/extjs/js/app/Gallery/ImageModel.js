Ext.define('Help.Gallery.ImageModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id'
        },
        {
            name: 'title'
        },
        {
            name: 'path'
        },
        {
            name: 'is_main'
        },
        {
            name: 'selected'
        }
    ],
    proxy: {
        type: 'ajax',
        url: '/admin/gallery/read',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'resultTotal',
            idProperty: 'id',
            successProperty: 'success'
        },
        writer: {
            type: 'json'
        }
    }
});