Ext.define('Help.Gallery.Panel', {
    extend: 'Ext.Panel',
    require: [
        'Help.Gallery.ImageStore',
        'Help.Gallery.View'
    ],
    layout: 'fit',
    tbar: [
        //,
        /*,
         {
         xtype: 'button',
         iconCls: 'search-icon',
         handler: function () {
         this.up('panel')._store.loadPage(1);
         }
         }*/
    ],
    items: [],

    deletePhotos: function () {
        var me = this,
            view = me.items.items[0],
            records = view.getSelectionModel().getSelection();
        if (records instanceof Array && !records.length) {
            Ext.Msg.alert(
                'Message', 'Выберите фотогрфии'
            );
            return false;
        }

        var multipleIds = [];
        Ext.each(records, function (e) {
            multipleIds.push(e.get('id'));
        });

        Ext.Msg.confirm('Confirm', 'Удалить ' + multipleIds.length + ' фотографии?', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: '/admin/gallery/delete',
                    params: {
                        'ids[]': multipleIds
                    },
                    success: function () {
                        me._store.load();
                    },
                    failure: function (response) {
                        window.app.errorMessage(response.responseText);
                    }
                });
            }
        });

        return true;
    },

    initComponent: function () {
        var me = this;
        var store = Ext.create('Help.Gallery.ImageStore');
        me.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            prependButtons: true,
            border: 1,
            store: store,
            displayMsg: 'Фотографий {0} - {1} из {2}',
            emptyMsg: 'Фотографий не найдено'
        });

        me.tbar = [
            {
                xtype: 'fileuploadfield',
                name: 'photo[]',
                id: 'gallery-image-upload',
                allowBlank: false,
                buttonOnly: true,
                buttonText: 'Загрузить',
                cls: 'add',
                listeners: {
                    render: function (fileInput) {
                        fileInput.fileInputEl.set({
                            multiple: 'multiple'
                        });
                        $(fileInput.fileInputEl.dom).wrap('<form id="gallery-form" method="post" enctype="multipart/form-data" action="/admin/gallery/upload"></form>')
                    },
                    change: function (fileInput, value, eOpts, event) {
                        var me = this.up('panel');

                        $.ajax({
                            url: '/admin/gallery/upload/',
                            type: 'post',
                            data: new FormData($('#gallery-form').get(0)),
                            contentType: false,
                            processData: false,
                            success: function (result) {
                                me._store.loadPage(1);
                            },
                            error: function () {

                            }
                        });
                    }
                }
            },
            {
                text: 'Удалить',
                id: 'btn-delete-photo',
                iconCls: 'remove',
                handler: function () {
                    this.up('panel').deletePhotos();
                }
            },
            '->',
            Ext.create('Ext.ux.form.SearchField', {
                width: 300,
                store: store,
                paramName: 'q'
            })
        ];
        me.callParent(arguments);

        var view = Ext.create('Help.Gallery.View', {store: store.load()});
        me.add(view);

        store.on({
            load: function () {
                var range = view.store.getRange();
                var records = [];
                for (var i = 0; i < range.length; i++) {
                    if (range[i].raw.selected) {
                        records.push(range[i]);
                    }
                }
                view.getSelectionModel().doMultiSelect(records);
            }
        });

        me._store = store;
    }
});