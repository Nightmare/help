Ext.define('Help.login.Window', {
    extend: 'Ext.window.Window',
    requires: [],
    alias: 'widget.LoginWindow',
    title: 'Вход в Help.md администрирование',
    id: 'loginWindow',
    app: null,
    width: 315,
    iconCls: 'auth-icon',
    resizable: false,
    height: 195,
    layout: 'fit',
    closable: false,
    items: [
        {
            xtype: 'form',
            bodyStyle: 'padding: 25px 5px 5px 5px',
            url: '/user/account/login',
            items: [
                {
                    xtype: 'textfield',
                    name: 'LoginForm[nick_name]',
                    itemId: 'nickNameField',
                    fieldLabel: 'Логин',
                    allowBlank: false,
                    anchor: '100%',
                    value: '',
                    listeners: {
                        specialkey: function (field, e) {
                            if (e.getKey() == e.ENTER) field.up('form').doSubmit();
                        }
                    }
                },
                {
                    xtype: 'textfield',
                    itemId: 'passwordField',
                    name: 'LoginForm[password]',
                    fieldLabel: 'Пароль',
                    anchor: '100%',
                    inputType: 'password',
                    allowBlank: false,
                    minLength: 3,
                    value: '',
                    listeners: {
                        specialkey: function (field, e) {
                            if (e.getKey() == e.ENTER) field.up('form').doSubmit();
                        }
                    }
                },
                {
                    xtype: 'checkbox',
                    name: 'LoginForm[rememberMe]',
                    boxLabel: 'Запомнить меня',
                    inputValue: 1,
                    anchor: '100%',
                    checked: true
                },
                {
                    xtype: 'label',
                    id: 'loginWindowMessage',
                    text: '',
                    style: 'color: red'
                }
            ],
            buttons: [
                {
                    text: 'Забыли пароль?',
                    id: 'lostPasswordButton',
                    handler: function () {
                        var win = this.up('window');
                        var lostWindow = Ext.create('Help.login.LostPassWindow', {
                            email: win.down('form').getValues()['email']
                        });

                        lostWindow.on('close', function () {
                            win.show();
                        });

                        lostWindow.show();
                        lostWindow.down('form').getComponent('nickNameField').focus();
                        win.hide();
                    }
                },
                {
                    text: 'Ввойти',
                    id: 'loginButton',
                    handler: function () {
                        this.up('form').doSubmit();
                    }
                }
            ],
            doSubmit: function () {
                var win = this.up('window');
                var form = this.getForm();
                if (form.isValid()) {
                    win.setHeight(215);
                    Ext.getCmp('loginWindowMessage').setText('Авторозиция, пожалуйста подождите...');

                    var getErrorMessage = function(result) {
                        var message = '';
                        for (var i in result) {
                            if (result.hasOwnProperty(i)) {
                                message += result[i].join("\n");
                            }
                        }
                        return message;
                    };

                    form.submit({
                        success: function (form, action) {
                            var result = action.result;
                            if (result.success) {
                                Ext.getCmp('loginWindow').close();
                                window.location.href = result.url;
                            } else {
                                Ext.getCmp('loginWindowMessage').setText(getErrorMessage(result));
                            }
                        },
                        failure: function (form, action) {
                            var result = action.result;
                            Ext.getCmp('loginWindowMessage').setText(getErrorMessage(result));
                        }
                    });
                }
            }
        }
    ],

    emailSent: function () {
        Ext.getCmp('loginWindowMessage').setText('Email sent. Check your mailbox');
    },

    initComponent: function () {
        this.callParent(arguments);
        var me = this;
        me.formPanel = me.down('form');
        me.form = me.formPanel.getForm();
        me.show();
    }
});