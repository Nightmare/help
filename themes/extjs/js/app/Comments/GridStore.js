Ext.define('Help.Comments.GridStore', {
    extend: 'Ext.data.Store',
    storeId: 'CommentsGridStore',
    remoteSort: true,
    pageSize: 20,
    model: 'Help.Comments.GridModel'
});