Ext.define('Help.Comments.GridModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'item_id',
            type: 'int'
        },
        {
            name: 'text'
        },
        {
            name: 'user'
        },
        {
            name: 'created_at',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            name: 'is_approved'
        }
    ],
    proxy: {
        type: 'rest',
        appendId: false,
        url: '/admin/comment/read',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'resultTotal',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }
});