Ext.define('Help.Comments.CommentsModule', {
    extend: 'Help.desktop.Module',
    require: [
        'Help.Comments.Window'
    ],
    id: 'commentsModule',

    init: function () {
        this.launcher = {
            text: 'Комментарии',
            iconCls: 'comments-icon',
            handler: this.createWindow,
            scope: this
        }
    },

    createWindow: function () {
        this.callParent(arguments);
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('comments-win');
        if (!win) {
            win = desktop.addWindow(Ext.create('Help.Comments.Window', {
                options: this.options
            }));
        }
        return win;
    }
});