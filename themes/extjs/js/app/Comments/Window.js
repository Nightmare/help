Ext.define('Help.Comments.Window', {
    extend: 'Ext.window.Window',
    require: [
        'Help.Comments.TabPanel'
    ],
    id: 'comments-win',
    app: null,
    title: 'Комментарии',
    width: 740,
    height: 485,
    iconCls: 'comments',
    animCollapse: false,
    border: false,
    constrainHeader: true,
    layout: 'fit',
    items: [],
    initComponent: function() {
        var me = this;
        this.callParent(arguments);
        me.add(Ext.create('Help.Comments.TabPanel'));
    }
});