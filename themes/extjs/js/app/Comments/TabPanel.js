Ext.define('Help.Comments.TabPanel', {
    extend: 'Ext.tab.Panel',
    require: [
        'Help.Comments.Item'
    ],
    items: [],
    initComponent: function () {
        this.callParent(arguments);
        this.add(
            {
                title: 'Объекты',
                header: false,
                layout: 'fit',
                border: false,
                items: [
                    Ext.create('Help.Comments.Item')
                ]
            }
        );
        this.setActiveTab(0);
    }
});
