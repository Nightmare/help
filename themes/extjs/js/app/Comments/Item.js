Ext.define('Help.Comments.Item', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Help.Comments.GridStore'
    ],
    multiSelect: true,
    scroll: 'vertical',
    id: 'ObjectCommentsGrid',
    plugins: [{
        ptype: 'rowexpander',
        rowBodyTpl : new Ext.XTemplate(
            '<p><strong>Пользователь:</strong> {user}</p>',
            '<p><strong>Добавлен:</strong> {created_at:this.dateFormat}</p>',
            Ext.String.format('<p><a target="item{item_id}_comment{id}" href="http://sport{0}/item/{item_id}#comment{id}">Перейти на комментарий</a></p>', Ext.domain),
            {
//                formatChange: function(v){
//                    var color = v >= 0 ? 'green' : 'red';
//                    return '<span style="color: ' + color + ';">' + Ext.util.Format.usMoney(v) + '</span>';
//                },
                dateFormat: function(date) {
                    return Ext.Date.dateFormat(date, 'Y-m-d H:i:s')
                }
            })
    }],
    viewConfig: {
        emptyText: 'Нет данных',
        deferEmptyText: false,
        getRowClass: function(record, index, rowParams, store) {
            return record.get('is_approved') ? 'approved': 'not_approved' ;
        }
    },
    columns: {
        items: [
            {
                text: 'Комментарий',
                hidden: false,
                renderer: function(val) {
                    return Ext.String.format('<div style="white-space:normal !important;">{0}</div>', val);
                },
                flex: 1,
                sortable: true,
                dataIndex: 'text'
            },
            {
                header: '',
                width: 30,
                type: 'bool',
                sortable: true,
                dataIndex: 'is_approved',
                xtype: 'checkcolumn',
                listeners: {
                    checkchange: function(column, recordIndex/*, checked*/) {
                        var grid = column.up('grid');
                        var record = grid.getStore().getAt(recordIndex);
                        Ext.Ajax.request({
                            url: '/admin/comment/save',
                            params: {
                                id: record.get('id'),
                                is_approved: record.get('is_approved') ? 1: 0
                            },
                            success: function () {
                                grid.getStore().load();
                            },
                            failure : function(response){
                                window.app.errorMessage(response.responseText);
                            }
                        });
                    }
                }
            }
        ]
    },

    initComponent: function () {
        var me = this;
        me.store = Ext.create('Help.Comments.GridStore');
        me.tbar = [
            '->',
            Ext.create('Ext.ux.form.SearchField', {
                width: 300,
                store: me.store,
                paramName: 'q',
                id: 'filter'
            })
        ];
        me.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            prependButtons : true,
            border: 0,
            store: me.store,
            displayMsg: 'Комментариев {0} - {1} из {2}',
            emptyMsg: 'Комментариев не найдено'
        });
        me.store.on({
            beforeload: function (store, options) {
                options.params || (options.params = {});
                Ext.apply(options.params, {
                    type: 'item'
                });
            }
        });
        me.callParent(arguments);
        me.store.load()
    }
});