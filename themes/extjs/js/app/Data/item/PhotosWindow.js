Ext.define('Help.Data.item.PhotosWindow', {
    extend: 'Ext.window.Window',
    requires: [
        'Help.Data.item.PhotosPanel'
    ],
    maximizable: false,
    minimizable: false,
    resizable: false,
    modal: true,
    width: 785,
    height: 510,
    title: 'Добавление фотографий',
    layout: 'fit',
    record: null,
    cls: 'gallery-win is-main-win',
    isSaved: false,
    _records: [],
    item_id: null,
    items: [],
    buttons: [
        {
            text: 'Закрыть',
            handler: function () {
                this.up('window').close();
            }
        },
        {
            text: 'Добавить',
            formBind: true,
            handler: function () {
                var me = this.up('window'),
                    view = me._panel.items.items[0],
                    checkbox = Ext.query('input[name=is_main]:checked');

                me._records = view.getSelectionModel().getSelection();
                var length = me._records.length;

                if (!length) {
                    Ext.Msg.alert('Сообщение', 'Выбирите фотографии');
                    return;
                }

                me.isSaved = true;
                if (checkbox.length) {
                    me.main_id = false;
                    for (var i = 0; i < me._records.length; i++) {
                        if (me._records[i].get('id') == checkbox[0].value) {
                            me.main_id = checkbox[0].value;
                            break;
                        }
                    }

                    if (!me.main_id) {
                        me.main_id = me._records[0].get('id');
                    }
                }
                me.close();
            }
        }
    ],

    initComponent: function () {
        var me = this;
        me.callParent(arguments);
        var panel = Ext.create('Help.Data.item.PhotosPanel', {
            item_id: me.item_id,
            win: me
        });
        me.add(panel);
        me._panel = panel;

        me.show();
    }
});