Ext.define('Help.Data.item.DistrictSelector', {
    extend: 'Ext.form.ComboBox',
    fieldLabel: '',
    alias: 'widget.districtSelector',
    queryMode: 'local',
    displayField: 'title',
    valueField: 'id',
    editable: false,
    store: Ext.getCmp('DistrictGrid').getStore()
});