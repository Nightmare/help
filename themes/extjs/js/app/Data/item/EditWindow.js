Ext.define('Help.Data.item.EditWindow', {
    extend: 'Ext.window.Window',
    requires: [
        'Ext.ux.GMapPanel',
        'Ext.ux.widget.Rating',
        'Help.Data.item.ContactsWindow',
        'Help.Data.item.PhotosWindow',
        'Help.Data.item.TemplateSelector',
        'Help.Data.item.DistrictSelector'
    ],
    maximizable: false,
    minimizable: false,
    resizable: false,
    modal: true,
    width: 550,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    title: 'Добавление объекта',

    record: null,

    items: [],
    recordsPhotos: [],
    gmItemStore: null,

    filterValues: {},
    isFilterValuesChanges: false,

    buttons: [
        {
            text: 'Закрыть',
            handler: function () {
                this.up('window').close();
            }
        },
        {
            text: 'Сохранить',
            formBind: true,
            handler: function () {
                var me = this.up('window');
                var form = me.down('form').getForm();
                if (!form.isValid()) {
                    return;
                }

                form.submit({
                    success: function (form, action) {
                        if (!action.result.success) {
                            try {
                                message = Ext.decode(action.result.data);
                            } catch (e) {
                            }
                            window.app.errorMessage(message);
                            return false;
                        }

                        /*if (null !== me.gmItemStore) {
                            var data = [];
                            for (var i in me.gmItemStore.getRange()) {
                                if (me.gmItemStore.getRange().hasOwnProperty(i)) {
                                    data.push(me.gmItemStore.getRange()[i].data);
                                }
                            }

                            Ext.Ajax.request({
                                method: 'POST',
                                url: '/admin/item/SaveGroupMaterial',
                                params: {items: JSON.stringify(data), itemId: action.result.data.id}*//*,
                                 success: this.onSaveSuccess.createDelegate(this),
                                 failure: this.onSaveFailure.createDelegate(this)*//*
                            });
                        }*/

                        me.close();
                        return true;
                    },
                    failure: function (form, action) {
                        window.app.errorMessage(action.result.data);
                    }
                });
            }

        }
    ],

    initComponent: function () {
        var me = this;
        me.callParent(arguments);
        window.itemWindow = me;

        var form = new Ext.form.Panel({
            url: '/admin/item/save',
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            defaultType: 'textfield',
            bodyStyle: 'padding: 15px',
            labelWidth: 100,
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'Item[id]'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'Item[lng]'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'Item[lat]'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'Item[filter_values]'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'Item[top]'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Навазние',
                            name: 'Item[title]',
                            allowBlank: false,
                            width: 290
                        },
                        {
                            xtype: 'districtSelector',
                            name: 'Item[district_id]',
                            id: 'district-field',
                            fieldLabel: 'Район',
                            allowBlank: false,
                            width: 200,
                            labelWidth: 50,
                            margin: '0 0 0 20'
                        }
                    ]
                },
                {
                    xtype: 'templateSelector',
                    id: 'template-field',
                    fieldLabel: 'Шаблон',
                    name: 'Item[template_id]',
                    allowBlank: true,
                    listeners: {
                        change: function() {
                            this.up('form').getForm().setValues({'Item[filter_values]': ''});
                        }
                    }
                },
                {
                    xtype: 'textarea',
                    fieldLabel: 'Описание',
                    name: 'Item[description]'
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'ratingField',
                            fieldLabel: 'Рейтинг',
                            value: 1,
                            split: 1,
                            minValue: 1,
                            name: 'Item[rate]'
                        },
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            text: 'Сделать/Убрать VIP',
                            handler: function() {
                                var self = this,
                                    me = self.up('window'),
                                    form = me.down('form'),
                                    record = me.record,
                                    values = null;

                                var win = Ext.create('Help.Data.item.TopWindow', {
                                    record: record
                                });

                                win.on('close', function() {
                                    if (win.isSaved) {
                                        values = win.values;
//                                        self.next('label').update(window.app.getMultipleStr(_countValues, 'значение', 'значения', 'значений'));
                                        form.getForm().setValues({'Item[top]': JSON.stringify(values)});
                                    } else if (win.isDeleted) {
                                        values = null;
                                        form.getForm().setValues({'Item[top]': 'delete'});
                                    }
                                });
                            }
                        },
                        {
                            xtype: 'label',
                            text: 'не является VIP',
                            margins: '4 0 0 7'
                        }
                    ]
                },
                {
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Значения фильтров',
                    layout: 'hbox',
                    defaultType: 'button',
                    items: [
                        {
                            text: 'Добавить/Изменить',
                            handler: function(/*button, event*/) {
                                var self = this;
                                var me = this.up('window'),
                                    form = me.down('form'),
                                    templateField = form.down('templateSelector'),
                                    record = me.record;

                                if (!form.getForm().isValid()) {
                                    return;
                                }

                                var win = Ext.create('Help.Data.item.ValueWindow', {
                                    record: record,
                                    templateId: templateField.getValue(),
                                    values: me.filterValues,
                                    changed: me.isFilterValuesChanges
                                });

                                win.on('close', function() {
                                    if (!win.isSaved) {
                                        return;
                                    }

                                    me.isFilterValuesChanges = true;
                                    me.filterValues = win.values;

                                    var _countValues = 0;
                                    for (var k in me.filterValues) {
                                        if (me.filterValues.hasOwnProperty(k)) {
                                            _countValues++
                                        }
                                    }

                                    self.next('label').update(window.app.getMultipleStr(_countValues, 'значение', 'значения', 'значений'));
                                    form.getForm().setValues({'Item[filter_values]': JSON.stringify(me.filterValues)});
                                });
                            }
                        },
                        {
                            xtype: 'label',
                            text: '0 значений',
                            id: 'label-value-info',
                            flex: 1,
                            margins: '4 0 0 7'
                        }
                    ]
                },
                {
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Иконки и котнтакты',
                    layout: 'hbox',
                    defaultType: 'button',
                    items: [
                        {
                            text: 'Контакты',
                            handler: function () {
                                var self = this;
                                var me = this.up('window');
                                var win = Ext.create('Help.Data.item.ContactsWindow', {
                                    record: me.record
                                });
                                win.on('close', function () {
                                    if (!win.isSaved) {
                                        return;
                                    }

                                    var form = me.down('form');
                                    var values = win.down('form').getForm().getValues();

                                    if (form._contactIds) {
                                        for (var k = 0; k < form._contactIds.length; k++) {
                                            form.remove(Ext.getCmp(form._contactIds[k]), true);
                                        }
                                    }

                                    var generateId = function (i, name, value) {
                                        return name + i + value + Math.uuid(15);
                                    };
                                    var createHiddenField = function (i, name, value, id) {
                                        return {
                                            xtype: 'hiddenfield',
                                            name: 'Contacts[' + i + '][' + name + ']',
                                            value: value,
                                            id: id
                                        }
                                    };

                                    var ids = [];

                                    if (!me.record) {
                                        me.record = {raw: {}};
                                    }
                                    me.record.raw['Contact'] = [];
                                    var contactsLength = 1;
                                    if (values['type'] instanceof Array) {
                                        for (var i = 0; i < values['type'].length; i++) {
                                            var idType = generateId(i, 'type', values['type'][i]);
                                            var idValue = generateId(i, 'value', values['value'][i]);

                                            ids.push(idType, idValue);
                                            form.add(createHiddenField(i, 'type', values['type'][i], idType));
                                            form.add(createHiddenField(i, 'value', values['value'][i], idValue));

                                            me.record.raw['Contact'].push({
                                                type: values['type'][i],
                                                value: values['value'][i]
                                            });
                                        }
                                        contactsLength = values['type'].length;
                                    } else {
                                        var _idType = generateId(i, 'type', values['type']);
                                        var _idValue = generateId(i, 'value', values['value']);
                                        ids.push(_idType, _idValue);
                                        form.add(createHiddenField(0, 'type', values['type'], _idType));
                                        form.add(createHiddenField(0, 'value', values['value'], _idValue));

                                        me.record.raw['Contact'].push({
                                            type: values['type'],
                                            value: values['value']
                                        });
                                    }
                                    self.next('label').update(window.app.getMultipleStr(contactsLength, 'контакт', 'контакта', 'контактов'));

                                    form._contactIds = ids;
                                });
                            }
                        },
                        {
                            xtype: 'label',
                            text: '0 контактов',
                            id: 'label-contact-info',
                            flex: 1,
                            margins: '4 0 0 7'
                        },
                        {
                            text: 'Фотографии',
                            handler: function () {
                                var self = this;
                                var me = this.up('window');
                                var itemId = null;
                                try {
                                    itemId = me.record.get('id');
                                } catch (e) {
                                }
                                var win = Ext.create('Help.Data.item.PhotosWindow', {
                                    item_id: itemId,
                                    _records: me.recordsPhotos
                                });
                                win.on('close', function () {
                                    if (!win.isSaved || !win._records.length) {
                                        return;
                                    }

                                    var form = me.down('form');
                                    var mainId = win.main_id;

                                    if (form._photoIds) {
                                        for (var k = 0; k < form._photoIds.length; k++) {
                                            form.remove(Ext.getCmp(form._photoIds[k]), true);
                                        }
                                    }
                                    var ids = [];
                                    me.recordsPhotos = win._records;
                                    Ext.each(win._records, function (e, i) {
                                        var id = Math.uuid(13) + i;
                                        ids.push(id);
                                        form.add({
                                            xtype: 'hiddenfield',
                                            name: 'Photos[' + i + ']',
                                            value: e.get('id'),
                                            id: id
                                        });
                                    });

                                    var id = Math.uuid(13) + mainId;
                                    ids.push(id);

                                    form.add({
                                        xtype: 'hiddenfield',
                                        name: 'main_photo',
                                        value: mainId,
                                        id: id
                                    });

                                    self.next('label').setText(window.app.getMultipleStr(win._records.length, 'фотографию', 'фотографии', 'фотографий'));
                                    form._photoIds = ids;
                                });
                            }
                        },
                        {
                            xtype: 'label',
                            text: '0 фотографий',
                            id: 'label-photo-info',
                            flex: 1,
                            margins: '4 0 0 7'
                        }
                    ]
                },
                {
                    xtype: 'filefield',
                    emptyText: 'Выберите маркер',
                    fieldLabel: 'Маркер',
                    buttonText: false,
                    name: 'Item[logo]',
                    id: 'ItemLogo',
                    buttonConfig: {
                        iconCls: 'upload-icon'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Адрес',
                    id: 'address',
                    name: 'Item[address]',
                    allowBlank: false
                },
                {
                    xtype: 'gmappanel',
                    id: 'gmap-field',
                    height: 200,
                    center: {
                        geoCodeAddr: /*window.itemWindow.down('form').getForm().findField('address').getValue() ||*/ 'Кишинёв, Молдова',
                        marker: {
                            title: this.geoCodeAddr,
                            draggable: true,
                            listeners: {
                                dragend: function (e) {
                                    var form = window.itemWindow.down('form').getForm();
                                    Ext.getCmp('gmap-field').getAddress(e.latLng, function (result, status) {
                                        if (status == google.maps.GeocoderStatus.OK) {
                                            var address = result[0];
                                            form.setValues({
                                                'Item[address]': address['formatted_address']
                                            })
                                        }
                                    });

                                    form.setValues({
                                        'Item[lng]': e.latLng.lng(),
                                        'Item[lat]': e.latLng.lat()
                                    });
                                }
                            }
                        }
                    },
                    mapOptions: {
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        zoom: 15
                    },
                    autoComplete: {
                        itemId: 'address-inputEl',
                        marker: {
                            draggable: true
                        },
                        callback: function (autoComplete) {
                            var location = autoComplete.getPlace().geometry.location;
                            window.itemWindow.down('form').getForm().setValues({
                                'Item[lng]': location.lng(),
                                'Item[lat]': location.lat()
                            });
                        }
                    }
                }
            ]

        });

        if (me.record) {
            me.setTitle('Редактирование объекта - ' + me.record.get('title'));
            form.getForm().setValues({
                'Item[id]': me.record.get('id'),
                'Item[lat]': me.record.get('lat'),
                'Item[lng]': me.record.get('lng'),
                'Item[title]': me.record.get('title'),
                'Item[address]': me.record.get('address'),
                'Item[rate]': me.record.get('rate'),
                'Item[description]': me.record.get('description')
            });
            Ext.getCmp('ItemLogo').emptyText = me.record.get('logo');
            Ext.getCmp('gmap-field').center.geoCodeAddr = me.record.get('address');
            var contacts = me.record.raw['Contact'] || [];
            Ext.getCmp('label-contact-info').setText(window.app.getMultipleStr(contacts.length, 'контакт', 'контакта', 'контактов'));
            Ext.getCmp('label-photo-info').setText(window.app.getMultipleStr(me.record.get('photos_count'), 'фотография', 'фотографии', 'фотографий'));
            Ext.getCmp('label-value-info').setText(window.app.getMultipleStr(me.record.get('value_count'), 'значение', 'значения', 'значений'));

            var districtCombo = Ext.getCmp('district-field');
            var record = districtCombo.getStore().findRecord('id', me.record.get('district_id'));
            districtCombo.select(record);

            var templateCombo = Ext.getCmp('template-field');
            record = templateCombo.getStore().findRecord('id', me.record.get('template_id'));
            templateCombo.select(record);
        } else {
            me.setTitle('Добавление объекта');
        }

        me.add(form);
        me.show();
    }
});