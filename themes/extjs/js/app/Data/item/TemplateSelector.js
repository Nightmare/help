Ext.define('Help.Data.item.TemplateSelector', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.templateSelector',
    queryMode: 'local',
    displayField: 'title',
    valueField: 'id',
    emptyText: 'Выбирите шаблон...',
    store: Ext.getCmp('TemplateGrid').getStore()
});
