Ext.define('Help.Data.item.ContactsWindow', {
    extend: 'Ext.window.Window',
    requires: [],
    maximizable: false,
    minimizable: false,
    resizable: false,
    modal: true,
    width: 350,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    title: 'Добавление контактов',
    record: null,
    isSaved: false,
    items: [
        {
            xtype: 'form',
            name: 'ItemContact'
        }
    ],
    buttons: [
        {
            text: 'Закрыть',
            handler: function () {
                this.up('window').close();
            }
        },
        {
            text: 'Добавить',
            formBind: true,
            handler: function () {
                var me = this.up('window');
                var form = me.down('form');
                if (form && !form.isValid()) {
                    return;
                }
                me.isSaved = true;
                me.close();
            }
        }
    ],

    initComponent: function () {
        var me = this;
        me.callParent(arguments);
        var item = function () {
            return {
                padding: '5 5 0 5',
                xtype: 'fieldcontainer',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'combo',
                        name: 'type',
                        triggerAction: 'all',
                        editable: false,
                        mode: 'local',
                        store: new Ext.data.ArrayStore({
                            id: 0,
                            fields: ['type', 'value'],
                            data: [
                                ['mobile', 'Мобильный'],
                                ['landline', 'Домашний'],
                                ['fax', 'Факс'],
                                ['skype', 'Skype'],
                                ['email', 'E-mail'],
                                ['site', 'Сайт']
                            ]
                        }),
                        valueField: 'type',
                        displayField: 'value',
                        margins: '0 5 0 0',
                        flex: 3,
                        allowBlank: false,
                        listeners: {
                            afterRender: function () {
                                var me = this.up('window');
                                if (!me.record || !me.record.raw['Contact'].length) {
                                    this.setValue('mobile');
                                    this.next('textfield').vtype = 'mobile';
                                }
                            },
                            select: function (combo, record, index) {
                                var valueField = combo.next('textfield');
                                valueField.vtype = combo.getValue();
                            }
                        }
                    },
                    {
                        xtype: 'textfield',
                        name: 'value',
                        vtype: 'phone',
                        margins: '0 5 0 0',
                        allowBlank: false,
                        flex: 4
                    }
                ]
            }
        };

        var removeBtn = function() {
            return {
                xtype: 'button',
                text: '-',
                flex: 1,
                handler: function () {
                    var container = this.up('fieldcontainer');
                    container.removeAll(true);
                    container.destroy();
                }
            }
        };

        var itemRemove = item();
        itemRemove.items.push(removeBtn());
        me._item = itemRemove;

        if (me.record && me.record.raw['Contact'].length) {
            var form = me.down('form');
            for (var i = 0; i < me.record.raw['Contact'].length; i++) {
                var value = me.record.raw['Contact'][i]['value'],
                    type = me.record.raw['Contact'][i]['type'];
                var _item = item();


                if (i == 0) {
                    _item.items.push({
                        xtype: 'button',
                        text: '+',
                        flex: 1,
                        handler: function () {
                            me.down('form').add(me._item);
                        }
                    })
                } else {
                    _item.items.push(removeBtn())
                }

                form.add(_item);
                var combo = form.query('fieldcontainer:last combo')[0];
                var textField = combo.next('textfield');

                combo.setValue(type);

                textField.vtype = type;
                textField.setValue(value);
            }
        } else {
            var itemAdd = item();
            itemAdd.items.push({
                xtype: 'button',
                text: '+',
                flex: 1,
                handler: function () {
                    me.down('form').add(me._item);
                }
            });
            me.down('form').add(itemAdd);
        }

        me.show();
    }
});