Ext.define('Help.Data.item.GridModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'district_id',
            type: 'int'
        },
        {
            name: 'template_id',
            type: 'int'
        },
        {
            name: 'title'
        },
        {
            name: 'address'
        },
        {
            name: 'description'
        },
        {
            name: 'rate',
            type: 'int'
        },
        {
            name: 'view_count',
            type: 'int'
        },
        {
            name: 'value_count',
            type: 'int'
        },
        {
            name: 'lat'
        },
        {
            name: 'lng'
        },
        {
            name: 'logo'
        },
        {
            name: 'Contact'
        },
        {
            name: 'photos_count',
            type: 'int'
        }
    ],
    proxy: {
        type: 'rest',
        appendId: false,
        url: '/admin/item/read',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'resultTotal',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }
});