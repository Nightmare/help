Ext.define('Help.Data.item.Grid', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Help.Data.item.EditWindow'
    ],
    scroll: 'vertical',
    multiSelect: true,
    viewConfig: {
        emptyText: 'Нет данных',
        deferEmptyText: false
    },
    id: 'ItemGrid',
    tbar: [],
    columns: {
        items: [
            {
                text: 'Название',
                width: 130,
                sortable: true,
                dataIndex: 'title'
            },
            {
                text: 'Описание',
                flex: 1,
                sortable: false,
                dataIndex: 'description'
            },
            {
                text: 'Адрес',
                width: 300,
                sortable: false,
                dataIndex: 'address'
            },
            {
                text: 'Рейт.',
                width: 50,
                align: 'center',
                sortable: true,
                dataIndex: 'rate'
            },
            {
                text: 'Просм.',
                width: 60,
                align: 'center',
                sortable: true,
                dataIndex: 'view_count'
            }
        ],
        defaults: {
            menuDisabled: true
        }
    },

    listeners: {
        itemdblclick: function(grid, record, item, index, event) {
            event.stopEvent();
            this.onEdit();
        }
    },

    onEdit: function() {
        var me = this;
//        me.store.sync()
        var records = me.getSelectionModel().getSelection();
        var record = null;
        if (records.length) {
            record = me.getStore().findRecord('id', records[0].get('id'));
        } else {
            Ext.Msg.alert(
                'Message', 'Выберите объект'
            );
            return;
        }
        me.onCreate(record);
    },

    onCreate: function(record) {
        var me = this;
        record = record || null;
        var win = Ext.create('Help.Data.item.EditWindow', {
            record: record
        });
        win.on('close', function () {
            if (window.itemWindow) {
                delete window.itemWindow;
            }
            me.store.loadPage(1);
        });
    },

    removeItem: function(grid) {
        var records = grid.getSelectionModel().getSelection();

        if (records instanceof Array && !records.length) {
            Ext.Msg.alert(
                'Message', 'Выберите объект'
            );
            return;
        }

        var multipleIds = [];
        Ext.each(records, function (e) {
            multipleIds.push(e.get('id'));
        });

        Ext.Msg.confirm('Confirm', 'Удалить ' + multipleIds.length + ' объект(а)?', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: '/admin/item/delete',
                    params: {
                        'ids[]': multipleIds
                    },
                    success: function () {
                        grid.store.load();
                    },
                    failure: function (response) {
                        window.app.errorMessage(response.responseText);
                    }
                });
            }
        });
    },

    initComponent: function () {
        var me = this;
        me.store = Ext.create('Help.Data.item.GridStore');
        me.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            prependButtons : true,
            border: 0,
            store: me.store,
            displayMsg: 'Объектов {0} - {1} из {2}',
            emptyMsg: 'Объектов не найдено'
        });
        me.tbar = [
            {
                text: 'Добавить',
                tooltip: 'Add a new row',
                id: 'btn-add-item',
                iconCls: 'add',
                handler: function () {
                    this.up('grid').onCreate();
                }
            },
            '-',
            {
                text: 'Редактировать',
                tooltip: 'Add a new row',
                id: 'btn-edit-item',
                iconCls: 'edit',
                handler: function () {
                    this.up('grid').onEdit();
                }
            },
            '-',
            {
                text: 'Удалить',
                tooltip: 'Remove the selected item',
                iconCls: 'remove',
                handler: function () {
                    var me = this.up('grid');
                    me.removeItem(me);
                }
            },
            '->',
            Ext.create('Ext.ux.form.SearchField', {
                width: 300,
                store: me.store,
                paramName: 'q'
            })
        ];
        me.callParent(arguments);
        me.store.loadPage(1);
    }
});
