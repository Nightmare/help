Ext.define('Help.Data.item.TopWindow', {
    require: [
        'Ext.form.*',
        'Ext.fx.target.Element',
        'Help.Data.item.Selectors.ViewCount'
    ],
    extend: 'Ext.window.Window',
    resizable: false,
    scroll: 'vertical',
    modal: true,
    width: 400,
    title: 'Изменение VIP',
    layout: 'fit',
    padding: 0,

    record: null,
    isSaved: false,
    isDeleted: false,
    values: null,

    buttons: [
        {
            text: 'Удалить',
            formBind: true,
            handler: function () {
                var me = this.up('window');
                me.isDeleted = true;
                me.close();
            }
        },
        '->',
        {
            text: 'Закрыть',
            handler: function () {
                this.up('window').close();
            }
        },
        {
            text: 'Сохранить',
            formBind: true,
            handler: function () {
                var me = this.up('window');
                me.values = me.down('form').getForm().getValues();
                me.isSaved = true;
                me.close();
            }
        }
    ],
    items: [
        {
            xtype: 'form',
            url: '/admin/top/SaveItem',
            layout: 'anchor',
            id: 'top-form',
            autoScroll: true,
            padding: '10 5 10 10',
            defaults: {
                labelWidth: 100,
                labelAlign: 'left',
                anchor: '100%'
            },
            defaultType: 'textfield',
            items: [
                {
                    xtype: 'combo',
                    triggerAction: 'all',
                    fieldLabel: 'Пользователь',
                    mode: 'local',
                    valueField: 'id',
                    name: 'owner_id',
                    displayField: 'nick_name',
                    allowBlank: false,
                    store: Ext.create('Help.User.Store').load()
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Максимальное кол-во. просм.',
                    triggerAction: 'all',
                    mode: 'local',
                    valueField: 'id',
                    name: 'max_view_count',
                    displayField: 'max_view_count',
                    allowBlank: false,
                    editable: false,
                    store: new Ext.data.ArrayStore({
                        fields: ['id', 'max_view_count'],
                        data: [
                            [-1, 'Бесконечный'],
                            [500, '500'],
                            [1000, '1000'],
                            [1500, '1500'],
                            [2000, '2000'],
                            [3000, '3000'],
                            [5000, '5000']
                        ]
                    })
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Кол-во просмотров',
                    name: 'current_view_count',
                    editor: {
                        xtype: 'textfield',
                        vtype: 'uint',
                        allowBlank: false
                    }
                },
                {
                    fieldLabel: 'Активировано в',
                    xtype: 'textfield',
                    name: 'paid_at',
                    readOnly: true,
                    border: false,
                    renderer: function (date) {
                        return date ? Ext.Date.dateFormat(date, 'Y-m-d H:i:s') : ''
                    }
                }
            ]
        }
    ],


    initComponent: function () {
        var me = this;
        me.callParent(arguments);

        if (!me.values) {
            Ext.Ajax.request({
                method: 'POST',
                url: '/admin/top/readItem',
                params: {item_id: me.record.get('id')},
                success: function(response) {
                    var form = me.down('form'),
                        result = Ext.decode(response.responseText);

                    if (undefined !== result.data && result.data.length) {
                        var record = result.data[0],
                            data = {};

                        for (var i in record) {
                            if (record.hasOwnProperty(i)) {
                                data[i] = record[i];
                            }
                        }
                        form.getForm().setValues(data);
                    }
                }
            });
        } else {
            form.getForm().setValues(me.values);
        }
        me.show();
    }
});