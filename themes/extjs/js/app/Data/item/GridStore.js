Ext.define('Help.Data.item.GridStore', {
    extend: 'Ext.data.Store',
    storeId: 'ItemGridStore',
    autoLoad: false,
    model: 'Help.Data.item.GridModel'
});