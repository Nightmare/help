Ext.define('Help.Data.item.PhotosPanel', {
    extend: 'Ext.Panel',
    require: [
        'Help.Gallery.ImageStore',
        'Help.Gallery.View'
    ],
    layout: 'fit',
    tbar: [
        '->',
        {
            xtype: 'textfield',
            width: 200,
            itemId: 'filter',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == e.ENTER) {
                        this.up('panel')._store.loadPage(1);
                    }
                }
            }
        },
        {
            xtype: 'button',
            iconCls: 'search-icon',
            handler: function () {
                this.up('panel')._store.loadPage(1);
            }
        }
    ],
    item_id: null,
    items: [],
    win: null,

    initComponent: function () {
        var me = this;
        var store = Ext.create('Help.Gallery.ImageStore');
        me.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            prependButtons: true,
            border: 1,
            store: store,
            displayMsg: 'Фотографий {0} - {1} из {2}',
            emptyMsg: 'Фотографий не найдено'
        });
        me.callParent(arguments);

        store.on({
            beforeload: function (store, options) {
                options.params || (options.params = {});
                Ext.apply(options.params, {
                    q: me.down('#filter').getValue(),
                    item_id: me.item_id
                });
            }
        });

        var win = me.win;
        var panel = Ext.create('Help.Gallery.View', {
            store: store.load(),
            listeners: {
                refresh: function(grid) {
                    if (win._records.length) {
                        grid.getSelectionModel().doMultiSelect(win._records);
                        return;
                    }

                    var range = grid.store.getRange(),
                        recordsSelect = [],
                        recordsDeselect = [];

                    for (var i = 0; i < range.length; i++) {
                        if (range[i].raw.selected) {
                            recordsSelect.push(range[i]);
                        } else {
                            recordsDeselect.push(range[i]);
                        }
                    }
                    grid.getSelectionModel().doMultiSelect(recordsSelect);
                    grid.getSelectionModel().doDeselect(recordsDeselect);
                }
            }
        });

        me.add(panel);
        me._store = store;
    }
});