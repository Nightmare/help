Ext.define('Help.Data.item.ValueWindow', {
    require: [
        'Ext.form.*',
        'Ext.fx.target.Element'
    ],
    extend: 'Ext.window.Window',
    resizable: false,
    scroll: 'vertical',
    modal: true,
    width: 600,
    height: 510,
    title: 'Добавление/изменение значения',
    layout: 'fit',
    padding: 0,

    record: null,
    templateId: null,
    itemId: null,
    isSaved: false,
    values: {},
    changed: false,

    buttons: [
        '->',
        {
            text: 'Закрыть',
            handler: function () {
                this.up('window').close();
            }
        },
        {
            text: 'Сохранить',
            formBind: true,
            handler: function () {
                var me = this.up('window'),
                    form = me.down('form');

                me.isSaved = true;
                me.values = form.getValues();
                me.close();
            }
        }
    ],
    items: [
        {
            xtype: 'form',

            url: '/admin/filter/saveItemValues',
            layout: 'anchor',
            autoScroll: true,
            padding: '10 5 10 10',
            defaults: {
                labelWidth: 100,
                labelAlign: 'left',
                anchor: '98%'
            },
            items: []
        }
    ],

    getFieldSet: function (title) {
        return {
            xtype: 'fieldset',
            flex: 1,
            title: title,
            collapsible: true,
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            items: []
        };
    },

    makeCheckbox: function (form, filter) {
        var me = this,
            checkboxes = [];

        for (var i = 0; i < filter.values.length; i++) {
            var value = filter.values[i],
                isActive = false;

            if (me.changed) {
                if (me.values.hasOwnProperty(filter.name)) {
                    var values = me.values[filter.name];
                    if (Object.prototype.toString.call(values) !== '[object Array]') {
                        values = [values];
                    }

                    for (var v in values) {
                        if (values.hasOwnProperty(v) && value.id == values[v]) {
                            isActive = true;
                        }
                    }
                }
            } else {
                isActive = value.active;
            }

            var checkbox = {
                fieldLabel: '',
                inputValue: value.id,
                boxLabel: value.value,
                labelSeparator: '',
                name: filter.name,
                hideEmptyLabel: true,
                checked: isActive
            };

            checkboxes.push(checkbox);
        }

        var fieldSet = me.getFieldSet(filter.title);
        fieldSet.items.push({
                xtype: 'checkboxgroup',
                columns: 4,
//                        columns: [100, 100],
//                        vertical: true,
                items: checkboxes
            }
        );

        form.add(fieldSet);
    },

    makeSelect: function (form, filter) {
        var me = this,
            data = [],
            selectId = null,
            fieldSet = me.getFieldSet(filter.title);

        for (var i = 0; i < filter.values.length; i++) {
            var value = filter.values[i];
            data.push([value.id, value.value]);

            if (me.changed) {
                if (me.values.hasOwnProperty(filter.name) && value.id == me.values[filter.name]) {
                    selectId = value.id;
                }
            } else if (value.active) {
                selectId = value.id;
            }
        }

        var combo = {
            xtype: 'combo',
            fieldLabel: '',
            queryMode: 'local',
            forceSelection: true,
            displayField: 'value',
            valueField: 'id',
            triggerAction: 'all',
            selectOnFocus: true,
            name: filter.name,
            store: new Ext.data.ArrayStore({
                fields: ['id', 'value'],
                data: data
            })
        };

        if (selectId) {
            combo.value = selectId;
        }

        fieldSet.items.push(combo);
        form.add(fieldSet);
    },

    makeRadio: function (form, filter) {
        var me = this,
            radioButtons = [],
            fieldSet = me.getFieldSet(filter.title);

        for (var i = 0; i < filter.values.length; i++) {
            var value = filter.values[i],
                isActive = false;

            if (me.changed) {
                if (me.values.hasOwnProperty(filter.name) && value.id == me.values[filter.name]) {
                    isActive = true;
                }
            } else {
                isActive = value.active;
            }

            var radioButton = {
                    fieldLabel: '',
                    inputValue: value.id,
                    boxLabel: value.value,
                    name: filter.name,
                    labelSeparator: '',
                    hideEmptyLabel: true,
                    checked: isActive
                };

            radioButtons.push(radioButton);
        }

        fieldSet.items.push({
            xtype: 'radiogroup',
            columns: 4,
            items: radioButtons
        });

        form.add(fieldSet);
    },

    initComponent: function () {
        var me = this,
            filterIds = [];

        me.callParent(arguments);

        if (me.record) {
            me.itemId = me.record.data.id;
        }

        var form = me.down('form');
        me.store = Ext.create('Help.Filter.Store');
        me.store.on({
            beforeLoad: {
                fn: function (store, options) {
                    options.params || (options.params = {});
                    Ext.apply(options.params, {templateId: me.templateId});
                },
                scope: this
            }
        });
        me.store.load(function (records, operation, success) {
            if (!success) {
                //todo: add error
                return;
            }

            for (var i = 0; i < records.length; i++) {
                filterIds.push(records[i].data.id);
            }

            Ext.Ajax.request({
                url: '/admin/filter/readFilterValues',
                params: {
                    'filterIds[]': filterIds,
                    itemId: me.itemId
                },
                success: function (response) {
                    if (200 !== response.status) {
                        return;
                    }
                    var result = Ext.decode(response.responseText);
                    if (!result.success || !result.data.length) {
                        return;
                    }

                    for (var i = 0; i < result.data.length; i++) {
                        var filter = result.data[i];
                        form.add(me['make' + filter.type.ucFirst()](form, filter));
                    }
                }
            });
        });

        me.show();
    }
});