Ext.define('Help.Data.TabModule', {
    extend: 'Help.desktop.Module',
    id: 'tabModule',

    init: function () {
        this.launcher = {
            text: 'Порталы',
            iconCls: 'data-icon',
            handler: this.createWindow,
            scope: this
        }
    },

    createWindow: function () {
        this.callParent(arguments);
        var desktop = this.app.getDesktop(),
            win = desktop.getWindow('tab-win');

        if (!win) {
            win = desktop.addWindow(Ext.create('Help.Data.Window', {
                options: this.options
            }));
        }

        return win;
    }
});
