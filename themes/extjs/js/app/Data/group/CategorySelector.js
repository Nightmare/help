Ext.define('Help.Data.category.CategorySelector', {
    extend: 'Ext.form.ComboBox',
    fieldLabel: '',
    alias: 'widget.categorySelector',
    queryMode: 'local',
    displayField: 'title',
    valueField: 'id',
    editable: false,
    store: Ext.getCmp('CategoryGrid').getStore()
});