Ext.define('Help.Data.group.Grid', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Help.Data.group.GridModel',
        'Help.Data.group.GridStore',
        'Help.Data.group.CategorySelector'
    ],
    multiSelect: true,
    scroll: 'vertical',
    id: 'GroupGrid',
    viewConfig: {
        emptyText: 'Нет данных',
        deferEmptyText: false
    },
    tbar: [],

    columns: {
        items: [
            {
                text: 'Название',
                flex: 1,
                sortable: true,
                dataIndex: 'title'
            },
            {
                text: 'Принадлежит к категории',
                flex: 1,
                sortable: true,
                dataIndex: 'category_title'
            }
        ],
        defaults: {
            menuDisabled: true
        }
    },
    listeners: {
        itemdblclick: function(grid, record, item, index, event) {
            event.stopEvent();
            this.onEdit();
        }
    },

    onEdit: function() {
        var grid = this;
        var records = grid.getSelectionModel().getSelection();

        if (records instanceof Array && !records.length) {
            Ext.Msg.alert(
                'Message', 'Выберите группу для редактирования.'
            );
            return;
        }
        var win = Ext.create('Help.Data.group.EditWindow', {
            record: records[0]
        });

        win.on('close', function () {
            grid.store.load();
        });
    },

    removeGroup: function (grid) {
        var records = grid.getSelectionModel().getSelection();

        if (records instanceof Array && !records.length) {
            Ext.Msg.alert(
                'Message', 'Сначала выберите группу'
            );
            return;
        }

        var multipleIds = [];
        Ext.each(records, function (e) {
            multipleIds.push(e.get('id'));
        });

        Ext.Msg.confirm('Confirm', 'Удалить ' + multipleIds.length + ' групп(ы)?', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: '/admin/group/delete',
                    params: {
                        'ids[]': multipleIds
                    },
                    success: function () {
                        grid.store.load();
                    },
                    failure: function (response) {
                        window.app.errorMessage(response.responseText);
                    }
                });
            }
        });
    },

    initComponent: function () {
        var me = this;
        me.store = Ext.create('Help.Data.group.GridStore');
        me.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            prependButtons : true,
            border: 0,
            store: me.store,
            displayMsg: 'Групп {0} - {1} из {2}',
            emptyMsg: 'Групп не найдено'
        });
        me.tbar = [
            {
                text: 'Добавить',
                tooltip: 'Add a new row',
                iconCls: 'add',
                handler: function () {
                    var me = this.up('grid');
                    var win = Ext.create('Help.Data.group.EditWindow');
                    win.on('close', function () {me.store.load();});
                }
            },
            '-',
            {
                text: 'Удалить',
                tooltip: 'Remove the selected item',
                iconCls: 'remove',
                handler: function () {
                    var me = this.up('grid');
                    me.removeGroup(me);
                }
            },
            '->',
            Ext.create('Ext.ux.form.SearchField', {
                width: 300,
                store: me.store,
                paramName: 'q'
            })
        ];
        me.callParent(arguments);
        me.store.load();
    }
});
