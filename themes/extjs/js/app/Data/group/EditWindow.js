Ext.define('Help.Data.group.EditWindow', {
    extend: 'Ext.window.Window',
    id: 'edit-data-group-win',
    app: null,
    title: 'Создание новой группы',
    width: 450,
    iconCls: 'filters',
    animCollapse: false,
    border: false,
    maximizable: false,
    minimizable: false,
    resizable: false,
    modal: true,

    record: null,

    constrainHeader: true,
    layout: {
        type: 'vbox',
        align: 'stretch',
        padding: 5
    },
    buttons: [
        '->',
        {
            text: 'Сохранить',
            formBind: true,
            handler: function () {
                var me = this.up('window');
                var form = me.down('form').getForm();
                if (!form.isValid()) {
                    return;
                }

                var data = [];
                for (var i in me.filterGroupStore.getRange()) {
                    if (me.filterGroupStore.getRange().hasOwnProperty(i)) {
                        data.push(me.filterGroupStore.getRange()[i].data);
                    }
                }

                form.setValues({filters: JSON.stringify(data)});
                form.submit({
                    success: function (form, action) {
                        if (!action.result.success) {
                            window.app.errorMessage(action.result.data);
                            return false;
                        }
                        me.close();
                        return true;
                    },
                    failure: function (form, action) {
                        window.app.errorMessage(action.result.data);
                    }
                });
            }
        },
        {
            text: 'Закрыть',
            handler: function () {
                this.up('window').close();
            }
        }
    ],
    items: [
        {
            xtype: 'form',
            url: '/admin/group/save',
            layout: 'anchor',
            defaults: {
                anchor: '100%',
                padding: '0 5 0 5'
            },
            defaultType: 'textfield',
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'id'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'filters'
                },
                {
                    name: 'title',
                    allowBlank: false,
                    emptyText: 'Введите название группы'
                },
                {
                    name: 'category_id',
                    emptyText: 'Выбирите к какой категории будет принадлежать группа',
                    xtype: 'categorySelector',
                    allowBlank: false
                }
            ]
        }
    ],
    initComponent: function () {
        var me = this;
        me.callParent(arguments);

        me.filterStore = Ext.create('Help.Filter.Store');
        me.filterGroupStore = Ext.create('Help.Filter.Store');

        var filterStoreParams = {},
            filterGroupStoreParams = {};
        if (me.record) {
            me.title = 'Редактирование группы - ' + me.record.get('title');
            me.down('form').loadRecord(me.record);
            filterStoreParams = {excludeGroupId: me.record.get('id')};
            filterGroupStoreParams = {groupId: me.record.get('id')};
        }

        me.add(
            {
                xtype: 'panel',
                height: 400,
                layout: {
                    type: 'hbox',
                    align: 'stretch',
                    padding: 5
                },
                defaults: {
                    xtype: 'grid',
                    scroll: 'vertical',
                    multiSelect: true,
                    hideHeaders: true,
                    autoHeight: true
                },
                items: [
                    {
                        viewConfig: {
                            emptyText: 'Нет данных',
                            deferEmptyText: false,
                            plugins: {
                                dragGroup: 'Filter',
                                dropGroup: 'Group',
                                ptype: 'gridviewdragdrop'
                            }
                        },
                        margins: '0 5 0 0',
                        columns: [
                            {
                                flex: 1,
                                sortable: true,
                                stripeRows: true,
                                dataIndex: 'title'
                            }
                        ],
                        flex: 1,
                        store: me.filterStore,
                        title: 'Свободные фильтры',

                        tbar: [
                            Ext.create('Ext.ux.form.SearchField', {
                                flex: 1,
                                store: me.filterStore,
                                paramName: 'q'
                            })
                        ]
                    },
                    {
                        viewConfig: {
                            emptyText: 'Нет данных',
                            deferEmptyText: false,
                            plugins: {
                                dragGroup: 'Group',
                                dropGroup: 'Filter',
                                ptype: 'gridviewdragdrop'
                            }
                        },
                        columns: [
                            {
                                flex: 1,
                                sortable: true,
                                stripeRows: true,
                                dataIndex: 'title'
                            }
                        ],
                        flex: 1,
                        store: me.filterGroupStore,
                        title: 'Фильтры группы',
                        tbar: [
                            Ext.create('Ext.ux.form.SearchField', {
                                flex: 1,
                                store: me.filterGroupStore,
                                paramName: 'q'
                            })
                        ]
                    }
                ]}
        );

        me.filterStore.on({
            beforeLoad: {
                fn: function (store, options) {
                    options.params || (options.params = {});
                    Ext.apply(options.params, filterStoreParams);
                },
                scope: this
            }
        });
        me.filterGroupStore.on({
            beforeLoad: {
                fn: function (store, options) {
                    options.params || (options.params = {});
                    Ext.apply(options.params, filterGroupStoreParams);
                },
                scope: this
            }
        });

        if (me.record) {
            me.filterStore.load();
            me.filterGroupStore.load();
        } else {
            me.filterStore.load(function(records) {
                var ids = [];
                Ext.each(records, function (e) {
                    ids.push(e.get('id'));
                });
                filterGroupStoreParams = Ext.apply(filterGroupStoreParams, {'excludeFilterIds[]': ids});
                me.filterGroupStore.load();
            });
        }

        me.show();
    }
});