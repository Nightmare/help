Ext.define('Help.Data.group.GridModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'category_id',
            type: 'int'
        },
        {
            name: 'category_title'
        },
        {
            name: 'title'
        }
    ],
    proxy: {
        type: 'rest',
        appendId: false,
        url: '/admin/group/read',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'resultTotal',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }
});