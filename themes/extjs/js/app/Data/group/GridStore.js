Ext.define('Help.Data.group.GridStore', {
    extend: 'Ext.data.Store',
    storeId: 'GroupGridStore',
    model: 'Help.Data.group.GridModel'
});