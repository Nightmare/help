Ext.define('Help.Data.site.Grid', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Help.Data.site.GridModel',
        'Help.Data.site.GridStore'

    ],
    multiSelect: true,
    scroll: 'vertical',
    viewConfig: {
        emptyText: 'Нет данных',
        deferEmptyText: false
    },
    id: 'SiteGrid',
    tbar: [
        {
            text: 'Добавить',
            tooltip: 'Add a new row',
            iconCls: 'add',
            handler: function () {
                var me = this.up('grid');
                me.rowEditing.cancelEdit();
                me.getStore().insert(0, new Help.Data.site.GridModel({
                    title: 'Название сайта',
                    name: 'nazvanie_sayta'
                }));
                me.rowEditing.startEdit(0, 0);
            }
        },
        '-',
        {
            text: 'Удалить',
            tooltip: 'Remove the selected item',
            iconCls: 'remove',
            handler: function () {
                var me = this.up('grid');
                me.removeSite(me);
            }
        },
        '-',
        {
            text: 'Обновить',
            handler: function () {
                this.up('grid').store.load();
            }
        }
    ],
    columns: {
        items: [
            {
                text: 'Название',
                flex: 1,
                sortable: true,
                dataIndex: 'title',
                editor: {
                    xtype: 'textfield',
                    allowBlank: false
                }
            },
            {
                text: 'Время суток',
                flex: 1,
                dataIndex: 'time_of_day',
                editor: {
                    xtype: 'combo',
                    allowBlank: false,
                    editable: false,
                    queryMode: 'local',
                    displayField: 'type',
                    valueField: 'id',
                    store: new Ext.data.ArrayStore({
                        fields: ['id', 'type'],
                        data: [
                            [0, 'Текущее время суток'],
                            [1, 'Всегда день'],
                            [2, 'Всегда ночь']
                        ]
                    })
                },
                renderer: function(value) {
                    var result;
                    switch (value) {
                        case 1:
                            result = 'Всегда день';
                            break;
                        case 2:
                            result = 'Всегда ночь';
                            break;
                        case 0:
                        default:
                            result = 'Текущее время суток';
                            break;
                    }

                    return result;
                }
            },
            {
                text: 'Тип погоды',
                flex: 1,
                dataIndex: 'weather_type',
                editor: {
                    xtype: 'combo',
                    allowBlank: false,
                    editable: false,
                    queryMode: 'local',
                    displayField: 'type',
                    valueField: 'id',
                    store: new Ext.data.ArrayStore({
                        fields: ['id', 'type'],
                        data: [
                            [0, 'Текущая погода'],
                            [1, 'Солнечно'],
                            [2, 'Дождь'],
                            [3, 'Снег'],
                            [4, 'Метель'],
                            [5, 'Облачно'],
                            [6, 'Дождь с грозой'],
                            [7, 'Туман'],
                            [8, 'Переменная облачность'],
                            [9, 'Ясно']
                        ]
                    })
                },
                renderer: function(value) {
                    var result;
                    switch (value) {
                        case 1:
                            result = 'Солнечно';
                            break;
                        case 2:
                            result = 'Дождь';
                            break;
                        case 3:
                            result = 'Снег';
                            break;
                        case 4:
                            result = 'Метель';
                            break;
                        case 5:
                            result = 'Облачно';
                            break;
                        case 6:
                            result = 'Дождь с грозой';
                            break;
                        case 7:
                            result = 'Туман';
                            break;
                        case 8:
                            result = 'Переменная облачность';
                            break;
                        case 9:
                            result = 'Ясно';
                            break;
                        case 0:
                        default:
                            result = 'Текущая погода';
                            break;
                    }

                    return result;
                }
            },
            {
                text: 'Ссылка на сайт',
                flex: 1,
                sortable: true,
                dataIndex: 'name',
                renderer: function (value) {
                    return Ext.String.format('<a href="http://{0}" target="{0}">{0}</a>', value + Ext.domain);
                },
                readOnly: true
            }
        ],
        defaults: {
            menuDisabled: true
        }
    },
    listeners: {
        itemcontextmenu: function (grid, record, item, index, event) {
            event.stopEvent();

            var me = this;

            var items = [
                {
                    text: 'Edit',
                    iconCls: 'edit',
                    handler: function () {
                        grid.ownerCt.rowEditing.startEdit(record, 0);
                    }
                },
                {
                    text: 'Удалить',
                    iconCls: 'remove',
                    handler: function () {
                        me.removeSite(grid);
                    }
                }
            ];

            window.app.ctxMenu({items: items}).showAt(event.xy);
        },

        edit: function (plugin, e) {
            var me = this;
            Ext.Ajax.request({
                url: '/admin/site/save',
                params: e.record.data,
                success: function (response) {
                    me.store.load();
                }
            });
        }
    },

    removeSite: function (grid) {
        grid.rowEditing.cancelEdit();
        var records = grid.getSelectionModel().getSelection();

        if (records instanceof Array && !records.length) {
            Ext.Msg.alert(
                'Message', 'Сначала выберите сайт'
            );
            return;
        }

        var multipleIds = [];
        Ext.each(records, function (e) {
            multipleIds.push(e.get('id'));
        });

        Ext.Msg.confirm('Confirm', 'Удалить ' + multipleIds.length + ' сайт(ы)?', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: '/admin/site/delete',
                    params: {
                        'ids[]': multipleIds
                    },
                    success: function () {
                        grid.store.load();
                    },
                    failure: function (response) {
                        window.app.errorMessage(response.responseText);
                    }
                });
            }
        });
    },

    initComponent: function () {
        var me = this;
        me.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2
        });
        me.rowEditing.on({
            scope: this,
            canceledit: function (pRoweditor, pChanges) {
                if (!pChanges.record.get('id')) {
                    pChanges.grid.store.remove(pChanges.record);
                }
            }
        });
        me.plugins = [me.rowEditing];
        me.store = Ext.create('Help.Data.site.GridStore');
        me.callParent(arguments);

        me.store.load();
    }
});
