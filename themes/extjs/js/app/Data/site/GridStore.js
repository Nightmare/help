Ext.define('Help.Data.site.GridStore', {
    extend: 'Ext.data.Store',
    storeId: 'SiteGridStore',
    autoLoad: false,
    model: 'Help.Data.site.GridModel'
});