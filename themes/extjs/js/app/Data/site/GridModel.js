Ext.define('Help.Data.site.GridModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'name'
        },
        {
            name: 'title'
        },
        {
            name: 'time_of_day',
            type: 'int'
        },
        {
            name: 'weather_type',
            type: 'int'
        }
    ],
    proxy: {
        type: 'rest',
        appendId: false,
        url: '/admin/site/read',
        reader: {
            type: 'json',
            root: 'data',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }
});