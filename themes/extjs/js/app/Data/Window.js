Ext.define('Help.Data.Window', {
    extend: 'Ext.window.Window',
    id: 'tab-win',
    app: null,
    title: 'Порталы',
    width: 740,
    height: 488,
    iconCls: 'data-icon',
    animCollapse: false,
    border: false,
    constrainHeader: true,
    layout: 'fit',
    items: [],

    initComponent: function() {
        var me = this;
        this.callParent(arguments);
        me.add(Ext.create('Help.Data.TabPanel'));
    }
});