Ext.define('Help.Data.category.GridStore', {
    extend: 'Ext.data.Store',
    storeId: 'CategoryGridStore',
    model: 'Help.Data.category.GridModel',
    sorters: [{
        property: 'sequence',
        direction: 'ASC'
    }]
});