Ext.define('Help.Data.category.SiteSelector', {
    requires: [
        'Help.Data.site.GridModel',
        'Help.Data.site.GridStore'
    ],
    extend: 'Ext.form.ComboBox',
    fieldLabel: '',
    alias: 'widget.siteSelector',
    queryMode: 'local',
    displayField: 'name',
    valueField: 'id',
    editable: false,
    store: Ext.getCmp('SiteGrid').getStore()
});