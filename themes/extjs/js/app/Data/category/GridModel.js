Ext.define('Help.Data.category.GridModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'site_id'

        },
        {
            name: 'title',
            type: 'string'
        },
        {
            name: 'sequence',
            type: 'int'
        }
    ],
    proxy: {
        type: 'ajax',
        appendId: false,
        url: '/admin/category/read',
        reader: {
            type: 'json',
            root: 'data',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }
});