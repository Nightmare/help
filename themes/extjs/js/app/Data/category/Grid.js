Ext.define('Help.Data.category.Grid', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Help.Data.category.GridModel',
        'Help.Data.category.GridStore',
        'Help.Data.category.SiteSelector'
    ],

    multiSelect: true,
    scroll: 'vertical',
    id: 'CategoryGrid',

    viewConfig: {
        emptyText: 'Нет данных',
        deferEmptyText: false,
        plugins: {
            ptype: 'gridviewdragdrop',
            dragText: 'Переместите для сортировки категорий'
        },
        listeners: {
            drop: function () {
                //var grid = this,
                //    newData = Ext.Array.pluck(grid.getStore().data.items, 'data'), i, item, data = {};
                //
                //for (i in newData) {
                //    if (newData.hasOwnProperty(i)) {
                //        item = newData[i];
                //        data[item.id] = item.sequence;
                //    }
                //}
                //
                //Ext.Ajax.request({
                //    url: '/admin/category/recognize',
                //    params: {
                //        value: Ext.encode(data)
                //    }
                //});
            },
            beforeDrop: function (node, data, dropRec, dropPosition) {
                ////Get the sequence number of the record dropped on
                //var recordSequence = dropRec.get('sequence');
                ////The record being dragged
                //var dragRecord = data.records[0];
                ////Get the sequence number of the drag record currently
                //var dragRecordCurrSequence = dragRecord.get('sequence');
                //
                ////Calculate what the new sequence number should be for the dragged
                ////row based on where it is dropped.
                //if (dropPosition === 'after' && recordSequence < dragRecordCurrSequence) {
                //    recordSequence++;
                //} else if (dropPosition === 'before' && recordSequence > dragRecordCurrSequence) {
                //    recordSequence--;
                //}
                //
                //dragRecord.set('sequence', recordSequence);
                //
                ////This delays the load of the grid data until after the sync
                ////This is not an ideal solution and an appropriate event listener would be better
                //var delayedLoad = new Ext.util.DelayedTask(function () {
                //    data.view.store.load();
                //});
                //delayedLoad.delay(250);
            }
        }
    },
    tbar: [
        {
            text: 'Добавить',
            tooltip: 'Добавить категорию',
            iconCls: 'add',
            handler: function () {
                var me = this.up('grid');
                me.rowEditing.cancelEdit();
                me.getStore().insert(0, new Help.Data.category.GridModel({
                    site_id: '',
                    title: 'новая категория'
                }));
                me.rowEditing.startEdit(0, 0);
            }
        },
        '-',
        {
            text: 'Удалить',
            tooltip: 'Удалить выбранные категории',
            iconCls: 'remove',
            handler: function () {
                var me = this.up('grid');
                me.removeCategory(me);
            }
        },
        '-',
        {
            text: 'Обновить',
            tooltip: 'Обновить категории',
            handler: function () {
                this.up('grid').store.load();
            }
        }
    ],
    columns: {
        items: [
            {
                text: 'Название',
                flex: 1,
                sortable: true,
                dataIndex: 'title',
                editor: {
                    xtype: 'textfield',
                    allowBlank: false,
                    msgTarget: 'none'
                },
                renderer: function (value, metadata, record, rowIndex, colIndex/*, store*/) {
//                    var siteSelector = Ext.widget('siteSelector');
//                    console.log(siteSelector.getStore());
//                    return Ext.String.format('<a href="{0}" target="{0}">{0}</a>', value);
                    return value;
                }
            },
            {
                text: 'Принадлежит к сайту',
                flex: 1,
                sortable: true,
                dataIndex: 'site_id',
                id: 'site',
                editor: {
                    xtype: 'siteSelector',
                    allowBlank: false
                },
                renderer: function (value, metadata, record, rowIndex, colIndex/*, store*/) {
                    var idx = this.columns[colIndex].field.store.find('id', value);
                    return Ext.String.format(
                        '<a href="http://{0}" target="{0}">{0}</a>',
                        (idx !== -1 ? this.columns[colIndex].field.store.getAt(idx).get('name') : value) + Ext.domain
                    );
                }
            }
        ],
        defaults: {
            menuDisabled: true
        }
    },
    listeners: {
        edit: function (editor, e) {
            var me = this;
            var data = e.record.getData();
            if (!/^\d+$/.test(data.site_id)) {
                data.site_id = Ext.getCmp('SiteGrid').getStore().findRecord('url', data.site_id).data.id;
            }

            Ext.Ajax.request({
                url: '/admin/category/save',
                params: data,
                success: function (/*response*/) {
                    me.store.load();
                }
            });
        }
    },

    removeCategory: function (grid) {
        grid.rowEditing.cancelEdit();
        var records = grid.getSelectionModel().getSelection();

        if (records instanceof Array && !records.length) {
            Ext.Msg.alert('Message', 'Выберите категорию для удаления.');
            return;
        }

        var multipleIds = [];
        Ext.each(records, function (e) {
            multipleIds.push(e.get('id'));
        });

        Ext.Msg.confirm('Confirm', 'Удалить ' + window.app.getMultipleStr(multipleIds.length, 'категорию', 'категории', 'категорий') + '?', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: '/admin/category/delete',
                    params: {
                        'ids[]': multipleIds
                    },
                    success: function () {
                        grid.store.load();
                    },
                    failure: function (response) {
                        window.app.errorMessage(response.responseText);
                    }
                });
            }
        });
    },

    initComponent: function () {
        var me = this;

        me.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', { clicksToEdit: 2 });
        me.rowEditing.on({
            scope: this,
            canceledit: function (rowEditor, changes) {
                if (!changes.record.get('id')) {
                    changes.grid.store.remove(changes.record);
                }
            }
        });
        me.plugins = [me.rowEditing];
        me.store = Ext.create('Help.Data.category.GridStore');
        me.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            prependButtons : true,
            border: 0,
            store: me.store,
            displayMsg: 'Категорий {0} - {1} из {2}',
            emptyMsg: 'Категорий не найдено'
        });
        me.callParent(arguments);

        me.store.load();
    }
});
