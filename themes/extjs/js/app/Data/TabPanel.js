Ext.define('Help.Data.TabPanel', {
    extend: 'Ext.tab.Panel',
    items: [],
    _tabs: {
        'Шаблоны': 'template',
        'Районы': 'district',
        'Типы сайтов': 'site',
        'Категории': 'category',
        'Группы': 'group',
        'Материалы': 'material',
        'Объекты': 'item'
    },

    //defaults: {
    //    listeners: {
    //        beforeactivate: function (tab) {
    //            var tabPanel = this.up('tabpanel');
    //            if (!tab.down('grid')) {
    //                tab.add(Ext.create('Help.Data.' + tabPanel._tabs[tab.title] + '.Grid'));
    //            }
    //        }
    //    }
    //},

    initComponent: function () {
        var me = this;
        me.callParent(arguments);

        for (var grid in me._tabs) {
            if (me._tabs.hasOwnProperty(grid)) {
                me.add({
                    title: grid,
                    header: false,
                    layout: 'fit',
                    border: false,
                    items: [
                        Ext.create('Help.Data.' + me._tabs[grid] + '.Grid')
                    ]
                });
            }
        }

        this.setActiveTab(0);
    }
});
