Ext.define('Help.Data.district.GridModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id'
        },
        {
            name: 'name'
        },
        {
            name: 'title'
        }
    ],
    proxy: {
        type: 'rest',
        appendId: false,
        url: '/admin/district/read',
        reader: {
            type: 'json',
            root: 'data',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }
});