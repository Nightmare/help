Ext.define('Help.Data.district.Grid', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Help.Data.district.GridModel',
        'Help.Data.district.GridStore'

    ],
    multiSelect: true,
    scroll: 'vertical',
    viewConfig: {
        emptyText: 'Нет данных',
        deferEmptyText: false
    },
    id: 'DistrictGrid',
    tbar: [
        {
            text: 'Добавить',
            tooltip: 'Add a new row',
            iconCls: 'add',
            handler: function() {
                var me = this.up('grid');
                me.rowEditing.cancelEdit();
                me.getStore().insert(0, new Help.Data.district.GridModel({
                    title: 'Новый район'
                }));
                me.rowEditing.startEdit(0, 0);
            }
        },
        '-',
        {
            text: 'Удалить',
            tooltip: 'Remove the selected item',
            iconCls: 'remove',
            handler: function() {
                var me = this.up('grid');
                me.removeSite(me);
            }
        },
        '-',
        {
            text: 'Обновить',
            handler: function() {
                this.up('grid').store.load();
            }
        }
    ],
    columns: {
        items: [
            {
                text: 'Название района',
                flex: 1,
                sortable: true,
                dataIndex: 'title',
                editor: {
                    xtype: 'textfield',
                    allowBlank: false
                }
            }
        ],
        defaults: {
            menuDisabled: true
        }
    },
    listeners: {
        itemcontextmenu: function (grid, record, item, index, event) {
            event.stopEvent();

            var me = this;

            var items = [
                {
                    text: 'Редактировать',
                    iconCls: 'edit',
                    handler: function () {
                        grid.ownerCt.rowEditing.startEdit(record, 0);
                    }
                },
                {
                    text: 'Удалить',
                    iconCls: 'remove',
                    handler: function () {
                        me.removeSite(grid);
                    }
                }
            ];

            window.app
                .ctxMenu({
                    items: items
                })
                .showAt(event.xy);
        },

        edit: function(plugin, e) {
            var me = this;
            Ext.Ajax.request({
                url: '/admin/district/save',
                params: e.record.data,
                success: function (response) {
                    me.store.load();
                }
            });
        }
    },

    removeSite: function(grid) {
        grid.rowEditing.cancelEdit();
        var records = grid.getSelectionModel().getSelection();

        if (records instanceof Array && !records.length) {
            Ext.Msg.alert(
                'Message', 'Выберите район'
            );
            return;
        }

        var multipleIds = [];
        Ext.each(records, function (e) {
            multipleIds.push(e.get('id'));
        });

        Ext.Msg.confirm('Confirm', 'Удалить ' + multipleIds.length + ' район(ы)?', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: '/admin/district/delete',
                    params: {
                        'ids[]': multipleIds
                    },
                    success: function () {
                        grid.store.load();
                    },
                    failure : function(response){
                        window.app.errorMessage(response.responseText);
                    }
                });
            }
        });
    },

    initComponent: function () {
        var me = this;
        me.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2
        });
        me.rowEditing.on({
            scope: this,
            canceledit: function(roweditor, changes) {
                if (!changes.record.get('id')) {
                    changes.grid.store.remove(changes.record);
                }
            }
        });
        me.plugins = [me.rowEditing];
        me.store = Ext.create('Help.Data.district.GridStore');
        me.callParent(arguments);

        me.store.load();
    }
});
