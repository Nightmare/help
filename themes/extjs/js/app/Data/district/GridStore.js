Ext.define('Help.Data.district.GridStore', {
    extend: 'Ext.data.Store',
    storeId: 'DistrictGridStore',
    autoLoad: false,
    model: 'Help.Data.district.GridModel'
});