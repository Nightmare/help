Ext.define('Help.Data.template.MaterialGroupWindow', {
    extend: 'Ext.window.Window',
    require: [
        'Help.Data.template.GroupMaterial.GroupModel',
        'Help.Data.template.GroupMaterial.ItemModel',
        'Help.Data.template.GroupMaterial.MaterialModel',

        'Help.Data.template.GroupMaterial.GroupStore',
        'Help.Data.template.GroupMaterial.ItemStore',
        'Help.Data.template.GroupMaterial.MaterialStore'
    ],
    requires: [],
    maximizable: false,
    minimizable: false,
    resizable: false,
    modal: true,
    width: 350,
    id: 'MaterialGroupWindow',

    record: null,

    title: 'Присвоение шаблону материал или группу',
    layout: {
        type: 'vbox',
        align: 'stretch',
        padding: 5
    },
    buttons: [
        {
            text: 'Закрыть',
            handler: function () {
                this.up('window').close();
            }
        },
        {
            text: 'Сохранить',
            formBind: true,
            handler: function () {
                var me = this.up('window');
                var form = me.down('form').getForm();
                if (!form.isValid()) {
                    return;
                }

                var grid = Ext.getCmp('groupMaterialTabPanel').getActiveTab().down('grid'),
                    records = grid.getSelectionModel().getSelection(),
                    ids = [];

                Ext.each(records, function (e) { ids.push(e.get('id')); });
                form.setValues({ids: ids, type: grid.id});
                form.submit({
                    success: function(form, action) {
                        if (!action.result.success) {
                            window.app.errorMessage(action.result.data);
                            return false;
                        }
                        me.close();
                        return true;
                    },
                    failure: function (form, action) {
                        window.app.errorMessage(action.result.data);
                    }
                });
            }
        }
    ],
    items: [
        {
            xtype: 'form',
            url: '/admin/template/save',
            layout: 'anchor',
            defaults: {
                anchor: '100%',
                padding: '0 5 0 5'
            },
            defaultType: 'textfield',
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'id'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'ids'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'type'
                },
                {
                    name: 'title',
                    allowBlank: false,
                    emptyText: 'Введите название шаблона'
                }
            ]
        }
    ],
    initComponent: function () {
        var me = this;
        this.callParent(arguments);

        me.materialStore = Ext.create('Help.Data.template.GroupMaterial.MaterialStore');
        me.groupStore = Ext.create('Help.Data.template.GroupMaterial.GroupStore');

        var id = 0;
        if (me.record) {
            id = me.record.get('id');
            me.down('form').loadRecord(me.record);
        }

        me.add({
            id: 'groupMaterialTabPanel',
            region: 'center',
            xtype: 'tabpanel',
            margin: '5',
            items: [{
                title: 'Материалы',
                items: [{
                    xtype: 'grid',
                    scroll: 'vertical',
                    multiSelect: true,
                    autoHeight: true,
                    height: 400,

                    id: 'material',

                    selModel: {
                        mode: 'SIMPLE'
                    },

                    viewConfig: {
                        emptyText: 'Нет данных',
                        deferEmptyText: false
                    },
                    columns: [{
                        text: 'Название',
                        flex: 1,
                        sortable: true,
                        stripeRows: true,
                        dataIndex: 'title'
                    }],
                    flex: 1,
                    store: me.materialStore,

                    tbar: [
                        Ext.create('Ext.ux.form.SearchField', {
                            flex: 1,
                            store: me.materialStore,
                            paramName: 'q'
                        })
                    ]
                }]
            }, {
                title: 'Группы материалов',
                items: [{
                    xtype: 'grid',
                    scroll: 'vertical',
                    autoHeight: true,
                    height: 400,

                    id: 'group',

                    viewConfig: {
                        emptyText: 'Нет данных',
                        deferEmptyText: false
                    },
                    columns: [{
                        text: 'Название',
                        flex: 1,
                        sortable: true,
                        stripeRows: true,
                        dataIndex: 'title'
                    }],
                    flex: 1,
                    store: me.groupStore,

                    tbar: [
                        Ext.create('Ext.ux.form.SearchField', {
                            flex: 1,
                            store: me.groupStore,
                            paramName: 'q'
                        })
                    ]
                }]
            }]
        });

        (function() {
            var setupStore = function(store, tabIndex) {
                store
                    .load({ params: { excludeTemplateId: id }})
                    .on({
                        beforeLoad: function (store, options) {
                            options.params || (options.params = {});
                            Ext.apply(options.params, {
                                excludeTemplateId: id
                            });
                        },
                        load: function(store, records/*, options*/) {
                            var activeRecords = [];
                            for (var i in records) {
                                if (records.hasOwnProperty(i) && true == records[i].get('active')) {
                                    activeRecords.push(records[i]);
                                }
                            }

                            if (activeRecords.length) {
                                var tabPanel = Ext.getCmp('groupMaterialTabPanel');
                                tabPanel.setActiveTab(tabIndex);
                                tabPanel.getActiveTab().down('grid').getSelectionModel().select(activeRecords);
                            }
                        }
                    });
            };

            setupStore(me.groupStore, 1);
            setupStore(me.materialStore, 0);
        })();

        me.show();
    }
});