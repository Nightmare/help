Ext.define('Help.Data.template.Model', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'title'
        },
        {
            name: 'info'
        }
    ],
    proxy: {
        type: 'rest',
        appendId: false,
        url: '/admin/template/read',
        reader: {
            type: 'json',
            root: 'data',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }
});