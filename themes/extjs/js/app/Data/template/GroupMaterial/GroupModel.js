Ext.define('Help.Data.template.GroupMaterial.GroupModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id'
        },
        {
            name: 'title'
        },
        {
            name: 'active'
        }
    ],
    proxy: {
        type: 'rest',
        appendId: false,
        url: '/admin/group/read',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'resultTotal',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }
});