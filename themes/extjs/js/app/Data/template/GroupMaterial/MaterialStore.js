Ext.define('Help.Data.template.GroupMaterial.MaterialStore', {
    extend: 'Ext.data.Store',
    autoLoad: false,
    model: 'Help.Data.template.GroupMaterial.MaterialModel'
});