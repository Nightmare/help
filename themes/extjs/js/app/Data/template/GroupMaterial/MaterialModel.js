Ext.define('Help.Data.template.GroupMaterial.MaterialModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id'
        },
        {
            name: 'title'
        },
        {
            name: 'active'
        }
    ],
    proxy: {
        type: 'rest',
        appendId: false,
        url: '/admin/material/read',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'resultTotal',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }

});