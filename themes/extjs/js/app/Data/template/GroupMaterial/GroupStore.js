Ext.define('Help.Data.template.GroupMaterial.GroupStore', {
    extend: 'Ext.data.Store',
    autoLoad: false,
    model: 'Help.Data.template.GroupMaterial.GroupModel'
});