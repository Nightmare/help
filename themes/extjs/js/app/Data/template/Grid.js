Ext.define('Help.Data.template.Grid', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Help.Data.template.Model',
        'Help.Data.template.Store'
    ],
    multiSelect: true,
    scroll: 'vertical',
    viewConfig: {
        emptyText: 'Нет данных',
        deferEmptyText: false
    },
    id: 'TemplateGrid',
    columns: {
        items: [
            {
                text: 'Название',
                flex: 1,
                sortable: true,
                dataIndex: 'title'
            },
            {
                text: 'Информация',
                flex: 1,
                sortable: true,
                dataIndex: 'info'
            }
        ]
    },

    listeners: {
        itemdblclick: function(gridView, record, item, index, e) {
            var win = Ext.create('Help.Data.template.MaterialGroupWindow', {
                record: record
            });

            win.on('close', function () {
                gridView.getStore().load();
            });
        }
    },

    initComponent: function () {
        var me = this;
        me.store = Ext.create('Help.Data.template.Store');
        me.tbar = [
            {
                text: 'Добавить',
                tooltip: 'Add a new row',
                iconCls: 'add',
                handler: function() {
                    var me = this.up('grid');
                    var win = Ext.create('Help.Data.template.MaterialGroupWindow');
                    win.on('close', function () {
                        me.getStore().load();
                    });
                }
            },
            '-',
            {
                text: 'Удалить',
                tooltip: 'Remove the selected item',
                iconCls: 'remove',
                handler: function() {
                    var grid = this.up('grid');
                    var records = grid.getSelectionModel().getSelection();

                    if (records instanceof Array && !records.length) {
                        Ext.Msg.alert(
                            'Message', 'Выберите шаблон для удаления.'
                        );
                        return;
                    }

                    var multipleIds = [];
                    Ext.each(records, function (e) {
                        multipleIds.push(e.get('id'));
                    });

                    Ext.Msg.confirm('Confirm', 'Удалить ' + multipleIds.length + ' шаблон(ы)?', function (btn) {
                        if (btn == 'yes') {
                            Ext.Ajax.request({
                                url: '/admin/template/delete',
                                params: {
                                    'ids[]': multipleIds
                                },
                                success: function () {
                                    grid.getStore().load();
                                },
                                failure : function(response){
                                    window.app.errorMessage(response.responseText);
                                }
                            });
                        }
                    });
                }
            },
            '->',
            Ext.create('Ext.ux.form.SearchField', {
                width: 300,
                store: me.store,
                paramName: 'q'
            })
        ];
        me.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            prependButtons : true,
            border: 0,
            store: me.store,
            displayMsg: 'Шаблонов {0} - {1} из {2}',
            emptyMsg: 'Шаблонов не найдено'
        });
        me.callParent(arguments);
        me.store.load();
    }
});
