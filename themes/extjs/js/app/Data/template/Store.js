Ext.define('Help.Data.template.Store', {
    extend: 'Ext.data.Store',
    storeId: 'TemplateStore',
    autoLoad: false,
    model: 'Help.Data.template.Model'
});