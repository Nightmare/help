Ext.define('Help.Data.material.GridStore', {
    extend: 'Ext.data.Store',
    storeId: 'materialGridStore',
    model: 'Help.Data.material.GridModel'
});