Ext.define('Help.Data.material.Grid', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Help.Data.material.GridModel',
        'Help.Data.material.GridStore',
        'Help.Data.material.GroupSelector'
    ],
    multiSelect: true,
    scroll: 'vertical',
    viewConfig: {
        emptyText: 'Нет данных',
        deferEmptyText: false
    },
    tbar: [],

    columns: {
        items: [
            {
                text: 'Название',
                flex: 1,
                sortable: true,
                dataIndex: 'title'
            },
            {
                text: 'Принадлежит к группе',
                flex: 1,
                sortable: true,
                dataIndex: 'group_title'
            }
        ],
        defaults: {
            menuDisabled: true
        }
    },
    listeners: {
        itemdblclick: function(grid, record, item, index, event) {
            event.stopEvent();
            this.onEdit();
        }
    },

    onEdit: function() {
        var grid = this;
        var records = grid.getSelectionModel().getSelection();

        if (records instanceof Array && !records.length) {
            Ext.Msg.alert(
                'Message', 'Выберите материал для редактирования.'
            );
            return;
        }
        var win = Ext.create('Help.Data.material.EditWindow', {record: records[0]});
        win.on('close', function () {grid.store.load();});
    },

    removeMaterial: function (grid) {
        var records = grid.getSelectionModel().getSelection();

        if (records instanceof Array && !records.length) {
            Ext.Msg.alert(
                'Message', 'Сначала выберите материал'
            );
            return;
        }

        var multipleIds = [];
        Ext.each(records, function (e) {
            multipleIds.push(e.get('id'));
        });

        Ext.Msg.confirm('Confirm', 'Удалить ' + multipleIds.length + ' материал(ы)?', function (btn) {
            if (btn == 'yes') {
                Ext.Ajax.request({
                    url: '/admin/material/delete',
                    params: {
                        'ids[]': multipleIds
                    },
                    success: function () {
                        grid.store.load();
                    },
                    failure: function (response) {
                        window.app.errorMessage(response.responseText);
                    }
                });
            }
        });
    },

    initComponent: function () {
        var me = this;
        me.store = Ext.create('Help.Data.material.GridStore');
        me.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            prependButtons : true,
            border: 0,
            store: me.store,
            displayMsg: 'Метериалов {0} - {1} из {2}',
            emptyMsg: 'Метериалов не найдено'
        });
        me.tbar = [
            {
                text: 'Добавить',
                tooltip: 'Add a new row',
                iconCls: 'add',
                handler: function () {
                    var me = this.up('grid');
                    var win = Ext.create('Help.Data.material.EditWindow');
                    win.on('close', function () {
                        win.record = null;
                        me.store.load();
                    });
                }
            },
            '-',
            {
                text: 'Удалить',
                tooltip: 'Remove the selected item',
                iconCls: 'remove',
                handler: function () {
                    var me = this.up('grid');
                    me.removeMaterial(me);
                }
            },
            '->',
            Ext.create('Ext.ux.form.SearchField', {
                width: 300,
                store: me.store,
                paramName: 'q'
            })
        ];
        me.callParent(arguments);
        me.store.load();
    }
});
