Ext.define('Help.Data.group.GroupSelector', {
    extend: 'Ext.form.ComboBox',
    fieldLabel: '',
    alias: 'widget.groupSelector',
    queryMode: 'local',
    displayField: 'title',
    valueField: 'id',
    editable: false,
    store: Ext.getCmp('GroupGrid').getStore()
});