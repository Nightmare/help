Ext.define('Help.Data.material.GridModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'group_id',
            type: 'int'
        },
        {
            name: 'group_title'
        },
        {
            name: 'title'
        }
    ],
    proxy: {
        type: 'rest',
        appendId: false,
        url: '/admin/material/read',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'resultTotal',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }
});