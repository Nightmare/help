Ext.define('Help.Data.material.EditWindow', {
    extend: 'Ext.window.Window',
    id: 'edit-data-material-win',
    app: null,
    title: 'Создание нового материала',
    width: 450,
    iconCls: 'filters',
    animCollapse: false,
    border: false,
    maximizable: false,
    minimizable: false,
    resizable: false,
    modal: true,

    record: null,

    constrainHeader: true,
    layout: {
        type: 'vbox',
        align: 'stretch',
        padding: 5
    },
    buttons: [
        '->',
        {
            text: 'Сохранить',
            formBind: true,
            handler: function () {
                var me = this.up('window');
                var form = me.down('form').getForm();
                if (!form.isValid()) {
                    return;
                }

                var data = [];
                for (var i in me.filterMaterialStore.getRange()) {
                    if (me.filterMaterialStore.getRange().hasOwnProperty(i)) {
                        data.push(me.filterMaterialStore.getRange()[i].data);
                    }
                }

                form.setValues({filters: JSON.stringify(data)});
                form.submit({
                    success: function (form, action) {
                        if (!action.result.success) {
                            window.app.errorMessage(action.result.data);
                            return false;
                        }
                        me.close();
                        return true;
                    },
                    failure: function (form, action) {
                        window.app.errorMessage(action.result.data);
                    }
                });
            }
        },
        {
            text: 'Закрыть',
            handler: function () {
                this.up('window').close();
            }
        }
    ],
    items: [
        {
            xtype: 'form',
            url: '/admin/material/save',
            layout: 'anchor',
            defaults: {
                anchor: '100%',
                padding: '0 5 0 5'
            },
            defaultType: 'textfield',
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'id'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'filters'
                },
                {
                    name: 'title',
                    allowBlank: false,
                    emptyText: 'Введите название материала'
                },
                {
                    name: 'group_id',
                    emptyText: 'Выбирите к какой группе будет принадлежать материал',
                    xtype: 'groupSelector',
                    allowBlank: false,
                    listeners: {
                        change: function(combo, newValue/*, oldValue, eOpts*/) {
                            var me = this.up('window');
                            Ext.getCmp('parentGroup').getStore().load({params: {groupId: newValue}});

                            me.filterStoreParams.excludeGroupId = newValue;
                            me.filterMaterialStoreParams.excludeGroupId = newValue;

                            if (me.record) {
                                me.filterStore.load();
                                me.filterMaterialStore.load();
                            } else {
                                me.filterStore.load(function(records) {
                                    var ids = [];
                                    Ext.each(records, function (e) {
                                        ids.push(e.get('id'));
                                    });
                                    me.filterMaterialStoreParams = Ext.apply(me.filterMaterialStoreParams, {'excludeFilterIds[]': ids});
                                    me.filterMaterialStore.load();
                                });
                            }
                        }
                    }
                }
            ]
        }
    ],

    initComponent: function () {
        var me = this;
        me.filterStoreParams = {};
        me.filterMaterialStoreParams = {};
        me.callParent(arguments);

        me.filterStore = Ext.create('Help.Filter.Store');
        me.filterMaterialStore = Ext.create('Help.Filter.Store');
        me.filterGroupStore = Ext.create('Help.Filter.Store');

        me.add(
            {
                xtype: 'grid',
                scroll: 'vertical',
                disableSelection: true,
                hideHeaders: true,
                id: 'parentGroup',
                height: 150,
                viewConfig: {
                    emptyText: 'Нет данных',
                    deferEmptyText: false
                },
                columns: [
                    {
                        flex: 1,
                        stripeRows: true,
                        dataIndex: 'title'
                    }
                ],
                flex: 1,
                store: me.filterGroupStore,
                title: 'Фильтры родительской группы'
            },
            {
                xtype: 'panel',
                height: 300,
                layout: {
                    type: 'hbox',
                    align: 'stretch',
                    padding: 5
                },
                defaults: {
                    xtype: 'grid',
                    scroll: 'vertical',
                    multiSelect: true,
                    hideHeaders: true,
                    autoHeight: true
                },
                items: [
                    {
                        viewConfig: {
                            emptyText: 'Нет данных',
                            deferEmptyText: false,
                            plugins: {
                                dragGroup: 'Filter',
                                dropGroup: 'Material',
                                ptype: 'gridviewdragdrop'
                            }
                        },
                        margins: '0 5 0 0',
                        columns: [
                            {
                                flex: 1,
                                sortable: true,
                                stripeRows: true,
                                dataIndex: 'title'
                            }
                        ],
                        flex: 1,
                        store: me.filterStore,
                        title: 'Свободные фильтры',

                        tbar: [
                            Ext.create('Ext.ux.form.SearchField', {
                                flex: 1,
                                store: me.filterStore,
                                paramName: 'q'
                            })
                        ]
                    },
                    {
                        viewConfig: {
                            emptyText: 'Нет данных',
                            deferEmptyText: false,
                            plugins: {
                                dragGroup: 'Material',
                                dropGroup: 'Filter',
                                ptype: 'gridviewdragdrop'
                            }
                        },
                        columns: [
                            {
                                flex: 1,
                                sortable: true,
                                stripeRows: true,
                                dataIndex: 'title'
                            }
                        ],
                        flex: 1,
                        store: me.filterMaterialStore,
                        title: 'Фильтры материала',
                        tbar: [
                            Ext.create('Ext.ux.form.SearchField', {
                                flex: 1,
                                store: me.filterMaterialStore,
                                paramName: 'q'
                            })
                        ]
                    }
                ]}
        );
        me.filterStore.on({
            beforeLoad: {
                fn: function (store, options) {
                    options.params || (options.params = {});
                    Ext.apply(options.params, me.filterStoreParams);
                },
                scope: this
            }
        });
        me.filterMaterialStore.on({
            beforeLoad: {
                fn: function (store, options) {
                    options.params || (options.params = {});
                    Ext.apply(options.params, me.filterMaterialStoreParams);
                },
                scope: this
            }
        });

        if (me.record) {
            me.title = 'Редактирование материала - ' + me.record.get('title');

            var form = me.down('form');
            form.loadRecord(me.record);

            me.filterStoreParams.excludeMaterialId = me.record.get('id');
            me.filterMaterialStoreParams.materialId = me.record.get('id');
        } else {
            me.filterMaterialStoreParams.materialId = 0;
            me.filterStore.load();
        }

        me.show();
    }
});