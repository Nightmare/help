Ext.define('Help.Settings.Grid', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Help.Settings.Store'
    ],
    multiSelect: true,
    scroll: 'vertical',
    id: 'SettingsGrid',
    viewConfig: {
        emptyText: 'Нет данных',
        deferEmptyText: false
    },
    columns: {
        items: [
        ]
    },
    listeners: {
        edit: function (editor, e) {
            var me = this;

            var data = e.record.getData();
            data.blocked_at = data.blocked_at == 'Заблокировать'
                ? Ext.Date.dateFormat(new Date(), 'Y-m-d H:i:s')
                : null;

            var _data = {};
            for (var i in data) {
                if (data.hasOwnProperty(i)) {
                    var val = data[i];
                    if (i == 'gender') {
                        val = 'unknown';
                        switch (data[i]) {
                            case 'мужской':
                                val = 'male';
                                break;
                            case 'женский':
                                val = 'female';
                                break;
                        }
                    }
                    _data['User[' + i + ']'] = val;
                }
            }

            Ext.Ajax.request({
                url: '/admin/user/save',
                params: _data,
                success: function (response) {
                    me.store.load();
                }
            });
        },

        itemcontextmenu: function (grid, record, item, index, event) {
            event.stopEvent();
            var items = [
                {
                    text: 'Edit',
                    iconCls: 'edit-icon',
                    handler: function () {
                        var win = Ext.create('SS.users.EditWindow', {
                            record_id: record.get('id')
                        });

                        win.on('close', function () {
                            grid.getStore().load();
                        });

                        win.show();
                    }
                },
                {
                    text: 'Delete',
                    iconCls: 'delete-icon',
                    handler: function () {
                        Ext.Msg.confirm('Confirm', 'Удалить ' + record.get('name') + '?', function (btn) {
                            if (btn == 'yes') {
                                Ext.Ajax.request({
                                    url: '../adw/users/users-store',
                                    params: {
                                        delete: 1,
                                        ids: record.get('id')
                                    },
                                    success: function () {
                                        grid.store.load();
                                    }
                                });
                            }
                        });
                    }
                },
                {
                    text: 'Login As',
                    iconCls: 'loginas-icon',
                    handler: function () {
                        Ext.Ajax.request({
                            url: Ext.baseUrl + '/admin/account/login',
                            params: {
                                user_id: record.get('id')
                            },
                            success: function () {
                                window.location.reload();
                            }
                        });
                    }
                }
            ];

            window.app.ctxMenu({items: items}).showAt(event.xy);
        }
    },

    initComponent: function () {
        var me = this;
        me.store = Ext.create('Help.User.Store');
        me.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2
        });
        me.rowEditing.on({
            scope: this,
            canceledit: function (pRoweditor, pChanges) {
                if (!pChanges.record.get('id')) {
                    pChanges.grid.store.remove(pChanges.record);
                }
            }
        });
        me.plugins = [me.rowEditing];
        me.tbar = [
            '->',
            Ext.create('Ext.ux.form.SearchField', {
                width: 300,
                store: me.store,
                paramName: 'q'
            })
        ];
        me.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            prependButtons : true,
            border: 0,
            store: me.store,
            displayMsg: 'Пользователей {0} - {1} из {2}',
            emptyMsg: 'Пользователей не найдено'
        });
        me.callParent(arguments);
        me.store.load();
    }
});