Ext.define('Help.Settings.Store', {
    extend: 'Ext.data.Store',
    storeId: 'SettingsStore',
    remoteSort: true,
    model: 'Help.Settings.Model'
});