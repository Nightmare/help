Ext.define('Help.Settings.Window', {
    extend: 'Ext.window.Window',
    id: 'settings-win',
    app: null,
    title: 'Настройки',
    width: 950,
    height: 485,
    iconCls: 'settings',
    animCollapse: false,
    border: false,
    constrainHeader: true,
    layout: 'fit',
    items: [],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.add(Ext.create('Help.Settings.Grid'));
    }
});