Ext.define('Help.Settings.Module', {
    extend: 'Help.desktop.Module',
    require: [
        'Help.Settings.Window'
    ],
    id: 'settingsModule',

    init: function () {
        this.launcher = {
            text: 'Настройки',
            iconCls: 'settings-icon',
            handler: this.createWindow,
            scope: this
        }
    },

    createWindow: function () {
        this.callParent(arguments);
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('settings-win');
        if (!win) {
            win = desktop.addWindow(Ext.create('Help.Settings.Window', {
                options: this.options
            }));
        }
        return win;
    }
});