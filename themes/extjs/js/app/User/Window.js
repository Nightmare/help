Ext.define('Help.User.Window', {
    extend: 'Ext.window.Window',
    id: 'user-win',
    app: null,
    title: 'Пользователи',
    width: 950,
    height: 485,
    iconCls: 'comments',
    animCollapse: false,
    border: false,
    constrainHeader: true,
    layout: 'fit',
    items: [],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.add(Ext.create('Help.User.Grid'));
    }
});