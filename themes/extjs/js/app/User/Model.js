Ext.define('Help.User.Model', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'nick_name'
        },
        {
            name: 'email'
        },
        {
            name: 'first_name'
        },
        {
            name: 'last_name'
        },
        {
            name: 'gender',
            convert: function (value, record) {
                var gender = 'не указан';
                switch (value) {
                    case 'male':
                        gender = 'мужской';
                        break;
                    case 'female':
                        gender = 'женский';
                        break;
                }
                return gender;
            }
        },
        {
            name: 'role'
        },
        {
            name: 'activated_at',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            name: 'created_at',
            type: 'date',
            dateFormat: 'Y-m-d H:i:s'
        },
        {
            name: 'blocked_at',
            convert: function (value, record) {
                return !!value;
            }
        }
    ],
    proxy: {
        type: 'rest',
        appendId: false,
        url: '/admin/user/read',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'resultTotal',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }
});