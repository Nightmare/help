Ext.define('Help.User.Store', {
    extend: 'Ext.data.Store',
    storeId: 'UserStore',
    remoteSort: true,
    model: 'Help.User.Model'
});