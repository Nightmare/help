Ext.define('Help.User.Module', {
    extend: 'Help.desktop.Module',
    require: [
        'Help.User.Window'
    ],
    id: 'userModule',

    init: function () {
        this.launcher = {
            text: 'Пользователи',
            iconCls: 'user-icon',
            handler: this.createWindow,
            scope: this
        }
    },

    createWindow: function () {
        this.callParent(arguments);
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('user-win');
        if (!win) {
            win = desktop.addWindow(Ext.create('Help.User.Window', {
                options: this.options
            }));
        }
        return win;
    }
});