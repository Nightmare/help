Ext.define('Help.Wallpaper.Settings', {
    extend: 'Ext.window.Window',

    uses: [
        'Ext.tree.Panel',
        'Ext.tree.View',
        'Ext.form.field.Checkbox',
        'Ext.layout.container.Anchor',
        'Ext.layout.container.Border',

        'Ext.ux.desktop.Wallpaper',
        'Help.Wallpaper.Model'
    ],

    layout: 'anchor',
    title: 'Change Settings',
    modal: true,
    width: 640,
    height: 480,
    border: false,

    initComponent: function () {
        var me = this;

        me.selected = me.desktop.getWallpaper();
        me.stretch = me.desktop.wallpaper.stretch;

        me.preview = Ext.create('widget.wallpaper');
        me.preview.setWallpaper(me.selected);
        me.tree = me.createTree();

        me.buttons = [
            { text: 'OK', handler: me.onOK, scope: me },
            { text: 'Cancel', handler: me.close, scope: me }
        ];

        me.items = [
            {
                anchor: '0 -30',
                border: false,
                layout: 'border',
                items: [
                    me.tree,
                    {
                        xtype: 'panel',
                        title: 'Preview',
                        region: 'center',
                        layout: 'fit',
                        items: [ me.preview ]
                    }
                ]
            },
            {
                xtype: 'checkbox',
                boxLabel: 'Растянуть на весь экран',
                checked: me.stretch,
                listeners: {
                    change: function (comp) {
                        me.stretch = comp.checked;
                    }
                }
            }
        ];

        me.callParent();
    },

    createTree: function () {
        var me = this;

        function child(img) {
            return { img: img, text: me.getTextOfWallpaper(img), iconCls: '', leaf: true };
        }

        var storeChildren = [{ text: 'Нету', iconCls: '', leaf: true }];

        Ext.each(Ext.Wallpaper.Files, function(file) {
            storeChildren.push(child(file));
        });

        return new Ext.tree.Panel({
            title: 'Desktop Background',
            rootVisible: false,
            lines: false,
            autoScroll: true,
            width: 150,
            region: 'west',
            split: true,
            minWidth: 100,
            listeners: {
                afterrender: { fn: this.setInitialSelection, delay: 100 },
                select: this.onSelect,
                scope: this
            },
            store: new Ext.data.TreeStore({
                model: 'Help.Wallpaper.Model',
                root: {
                    text: 'Wallpaper',
                    expanded: true,
                    children: storeChildren
                }
            })
        });
    },

    getTextOfWallpaper: function (path) {
        var text = path, slash = path.lastIndexOf('/');
        if (slash >= 0) {
            text = text.substring(slash + 1);
        }
        var dot = text.lastIndexOf('.');
        text = Ext.String.capitalize(text.substring(0, dot));
        text = text.replace(/[-]/g, ' ');
        return text;
    },

    onOK: function () {
        var me = this;
        if (me.selected) {
            me.desktop.setWallpaper(me.selected, me.stretch);

            var stretch = me.stretch ? 1: 0;

            // change user settings
            Ext.UserSettings.wallpaper = me.selected;
            Ext.UserSettings.wallpaper_stretch = stretch;

            Ext.Ajax.request({
                url: '/admin/settings/SaveWallpaper',
                params: {
                    wallpaper: me.selected,
                    wallpaper_stretch: stretch

                }/*,
                success: function () {
                    grid.store.load();
                },
                failure: function (response) {
                    window.app.errorMessage(response.responseText);
                }*/
            });
        }
        me.destroy();
    },

    onSelect: function (tree, record) {
        var me = this;

        me.selected = record.data.img
            ? Ext.baseUrl + '/images/wallpapers/' + record.data.img
            : Ext.BLANK_IMAGE_URL;

        me.preview.setWallpaper(me.selected);
    },

    setInitialSelection: function () {
        var s = this.desktop.getWallpaper();
        if (s) {
            var path = '/Wallpaper/' + this.getTextOfWallpaper(s);
            this.tree.selectPath(path, 'text');
        }
    }
});
