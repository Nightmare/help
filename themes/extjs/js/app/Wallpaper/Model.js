Ext.define('Help.Wallpaper.Model', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'text' },
        { name: 'img' }
    ]
});