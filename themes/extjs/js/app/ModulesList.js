Ext.define('Help.ModulesList', {
    requires: [
        'Help.Data.TabModule',
        'Help.Gallery.GalleryModule',
        'Help.Comments.CommentsModule',
        'Help.User.Module',
        'Help.Settings.Module',
        'Help.Filter.Module'
    ],

    get: function () {
        return [
            Ext.create('Help.Data.TabModule'),
            Ext.create('Help.Gallery.GalleryModule'),
            Ext.create('Help.Comments.CommentsModule'),
            Ext.create('Help.User.Module'),
            Ext.create('Help.Settings.Module'),
            Ext.create('Help.Filter.Module')
        ];
    }
});