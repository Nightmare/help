Ext.define('Help.Filter.Model', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'title'
        },
        {
            name: 'name'
        },
        {
            name: 'type'
        }
    ],
    proxy: {
        type: 'rest',
        appendId: false,
        url: '/admin/filter/read',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'resultTotal',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }
});