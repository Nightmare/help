Ext.define('Help.Filter.ValueModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {
            name: 'id',
            type: 'int'
        },
        {
            name: 'filter_id',
            type: 'int'
        },
        {
            name: 'value'
        },
        {
            name: 'active',
            type: 'boolean'
        }
    ],
    proxy: {
        type: 'rest',
        appendId: false,
        url: '/admin/filter/readValue',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'resultTotal',
            idProperty: 'id',
            successProperty: 'result'
        },
        writer: {
            type: 'json'
        }
    }
});