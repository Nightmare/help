Ext.define('Help.Filter.ValueGrid', {
    extend: 'Ext.grid.Panel',
    scroll: 'vertical',
    multiSelect: true,
    autoHeight : true,
    title: false,
    viewConfig: {
        emptyText: 'Нет данных'
    },
    columns: [
        {
            text: 'Значение',
            flex: 1,
            sortable: true,
            stripeRows: true,
            dataIndex: 'value',
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        }
    ],
    flex: 1,
    initComponent: function() {
        var me = this;
        me.callParent(arguments);

        if (!me.store) {
            me.store = Ext.create('Help.Filter.ValueStore');
            me.store.load();
        }
        me.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {clicksToEdit: 2});
        me.rowEditing.on({
            scope: this,
            canceledit: function(rowEditor, changes) {
                if (!changes.record.get('id')) {
                    changes.grid.store.remove(changes.record);
                }
            }
        });
        me.plugins = [me.rowEditing];
    }
});