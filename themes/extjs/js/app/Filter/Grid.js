Ext.define('Help.Filter.Grid', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Help.Filter.Store',
        'Help.Filter.EditWindow'
    ],
    multiSelect: true,
    scroll: 'vertical',
    id: 'FilterGrid',
    viewConfig: {
        emptyText: 'Нет данных',
        deferEmptyText: false
    },
    columns: {
        items: [
            {
                text: 'Название',
                dataIndex: 'title',
                flex: 1
            }
        ]
    },
    listeners: {
        itemdblclick: function(grid, record, item, index, event) {
            event.stopEvent();
            this.onEdit();
        }
    },

    onEdit: function() {
        var grid = this;
        var records = grid.getSelectionModel().getSelection();

        if (records instanceof Array && !records.length) {
            Ext.Msg.alert(
                'Message', 'Выберите фильтр для редактирования.'
            );
            return;
        }
        var win = Ext.create('Help.Filter.EditWindow', {
            record: records[0]
        });

        win.on('close', function () {
            grid.store.load();
        });
    },

    initComponent: function () {
        var me = this;
        me.store = Ext.create('Help.Filter.Store');
        me.tbar = [
            {
                text: 'Добавить',
                tooltip: 'Добавить новый фильтр',
                iconCls: 'add',
                handler: function() {
                    var me = this.up('grid');
                    var win = Ext.create('Help.Filter.EditWindow');
                    win.on('close', function () {
                        me.store.load();
                    });
                }
            },
            {
                text: 'Редактировать',
                tooltip: 'Редактировать фильтр',
                iconCls: 'add',
                handler: function() {
                    var grid = this.up('grid');
                    grid.onEdit();
                }
            },
            '-',
            {
                text: 'Удалить',
                tooltip: 'Удалить выбраный фильтр',
                iconCls: 'remove',
                handler: function() {
                    var grid = this.up('grid');
                    var records = grid.getSelectionModel().getSelection();

                    if (records instanceof Array && !records.length) {
                        Ext.Msg.alert(
                            'Message', 'Выберите фильтр(ы).'
                        );
                        return false;
                    }

                    var multipleIds = [];
                    Ext.each(records, function (e) {
                        multipleIds.push(e.get('id'));
                    });

                    Ext.Msg.confirm('Confirm', 'Удалить ' + multipleIds.length + ' фильрт(ы)?', function (btn) {
                        if (btn == 'yes') {
                            Ext.Ajax.request({
                                url: '/admin/filter/delete',
                                params: {
                                    'ids[]': multipleIds
                                },
                                success: function () {
                                    grid.store.load();
                                },
                                failure : function(response){
                                    window.app.errorMessage(response.responseText);
                                }
                            });
                        }
                    });

                    return true;
                }
            },
            '->',
            Ext.create('Ext.ux.form.SearchField', {
                width: 300,
                store: me.store,
                paramName: 'q'
            })
        ];
        me.bbar = Ext.create('Ext.PagingToolbar', {
            displayInfo: true,
            prependButtons: true,
            border: 0,
            store: me.store,
            displayMsg: 'Фильтров {0} - {1} из {2}',
            emptyMsg: 'Фильтров не найдено'
        });
        me.callParent(arguments);
        me.store.load();
    }
});