Ext.define('Help.Filter.Window', {
    extend: 'Ext.window.Window',
    id: 'filter-win',
    app: null,
    title: 'Фильтры',
    width: 750,
    height: 485,
    iconCls: 'filters',
    animCollapse: false,
    border: false,
    constrainHeader: true,
    layout: 'fit',
    items: [],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.add(Ext.create('Help.Filter.Grid'));
    }
});