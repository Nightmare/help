Ext.define('Help.Filter.ValueStore', {
    extend: 'Ext.data.Store',
    storeId: 'FilterValueStore',
    remoteSort: false,
    model: 'Help.Filter.ValueModel'
});