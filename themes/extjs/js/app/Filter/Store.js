Ext.define('Help.Filter.Store', {
    extend: 'Ext.data.Store',
    storeId: 'FilterStore',
    remoteSort: true,
    model: 'Help.Filter.Model'
});