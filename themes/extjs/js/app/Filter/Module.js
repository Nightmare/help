Ext.define('Help.Filter.Module', {
    extend: 'Help.desktop.Module',
    require: [
        'Help.Filter.Window'
    ],
    id: 'filterModule',

    init: function () {
        this.launcher = {
            text: 'Фильтры',
            iconCls: 'filter-icon',
            handler: this.createWindow,
            scope: this
        }
    },

    createWindow: function () {
        this.callParent(arguments);
        var desktop = this.app.getDesktop();
        var win = desktop.getWindow('filter-win');
        if (!win) {
            win = desktop.addWindow(Ext.create('Help.Filter.Window', {
                options: this.options
            }));
        }
        return win;
    }
});