Ext.define('Help.Filter.EditWindow', {
    extend: 'Ext.window.Window',
    id: 'edit-filter-win',
    app: null,
    title: 'Создание нового фильтра',
    width: 450,
    iconCls: 'filters',
    animCollapse: false,
    border: false,
    maximizable: false,
    minimizable: false,
    resizable: false,
    modal: true,

    record: null,

    constrainHeader: true,
    layout: {
        type: 'vbox',
        align: 'stretch',
        padding: 5
    },
    buttons: [
        {
            text: 'Добавить',
            tooltip: 'Добавить значение',
            iconCls: 'add',
            handler: function() {
                var grid = this.up('window').down('grid');
                grid.rowEditing.cancelEdit();
                grid.getStore().insert(0, Ext.create('Help.Filter.ValueModel', {value: 'новое значение'}));
                grid.rowEditing.startEdit(0, 0);
            }
        },
        '-',
        {
            text: 'Удалить',
            tooltip: 'Удалить значение',
            iconCls: 'remove',
            handler: function() {
                var grid = this.up('window').down('grid');
                var records = grid.getSelectionModel().getSelection();

                if (records instanceof Array && !records.length) {
                    Ext.Msg.alert(
                        'Message', 'Выберите значения фильтра.'
                    );
                    return;
                }

                Ext.each(records, function (e) {
                    grid.store.remove(e)
                });
            }
        },
        '->',
        {
            text: 'Закрыть',
            handler: function () {
                this.up('window').close();
            }
        },
        {
            text: 'Сохранить',
            formBind: true,
            handler: function () {
                var me = this.up('window');
                var form = me.down('form').getForm();
                if (!form.isValid()) {
                    return;
                }

                var data = [];
                for (var i in me.valueStore.getRange()) {
                    if (me.valueStore.getRange().hasOwnProperty(i)) {
                        data.push(me.valueStore.getRange()[i].data);
                    }
                }

                form.setValues({filter_values: JSON.stringify(data)});
                form.submit({
                    success: function(form, action) {
                        if (!action.result.success) {
                            window.app.errorMessage(action.result.data);
                            return false;
                        }
                        me.close();
                        return true;
                    },
                    failure: function (form, action) {
                        window.app.errorMessage(action.result.data);
                    }
                });
            }
        }
    ],
    items: [
        {
            xtype: 'form',
            url: '/admin/filter/save',
            layout: 'anchor',
            defaults: {
                anchor: '100%',
                padding: '0 5 0 5'
            },
            defaultType: 'textfield',
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'id'
                },
                {
                    xtype: 'hiddenfield',
                    name: 'filter_values'
                },
                {
                    name: 'title',
                    allowBlank: false,
                    emptyText: 'Введите название фильтра'
                },
                {
                    name: 'type',
                    emptyText: 'Выбирите тип фальтра',
                    allowBlank: false,
                    xtype: 'combo',
                    editable: false,
                    queryMode: 'local',
                    displayField: 'type',
                    valueField: 'id',
                    store: new Ext.data.ArrayStore({
                        fields: ['id', 'type'],
                        data: [
                            ['select', 'Выбор одного значения из выпадающего списка (select)'],
                            ['checkbox', 'Выбор нескольких значений из предоставленных (checkbox)'],
                            ['radio', 'Выбор одного значения из предоставленных (radio button)']
                        ]
                    })
                }
            ]
        }
    ],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);

        me.valueStore = Ext.create('Help.Filter.ValueStore');

        var valueStoreParams = {};
        if (me.record) {
            me.title = 'Редактирование фильтра - '+ me.record.get('title');
            me.down('form').loadRecord(me.record);
            valueStoreParams = {filterId: me.record.get('id')}
        }
        me.add(Ext.create('Help.Filter.ValueGrid', {
            store: me.valueStore,
            height: 300
        }));
        me.valueStore.load({params: valueStoreParams});
        me.show();
    }
});