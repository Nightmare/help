/**
 * @class Ext.ux.GMapPanel
 * @extends Ext.Panel
 * @author Shea Frederick
 */
Ext.define('Ext.ux.GMapPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.gmappanel',
    requires: ['Ext.window.MessageBox'],

    markers: [],

    initComponent: function () {
        Ext.applyIf(this, {
            plain: true,
            gmapType: 'map',
            border: false
        });

        this.callParent();
    },

    afterFirstLayout: function () {
        var center = this.center;
        this.callParent();

        if (center) {
            if (center.geoCodeAddr) {
                this.lookupCode(center.geoCodeAddr, center.marker);
            } else {
                this.createMap(center);
            }
        } else {
            Ext.Error.raise('center is required');
        }

    },

    createMap: function (center, marker) {
        var options = Ext.apply({}, this.mapOptions);
        options = Ext.applyIf(options, {
            zoom: 14,
            center: center,
            mapTypeId: google.maps.MapTypeId.HYBRID
        });
        this.gmap = new google.maps.Map(this.body.dom, options);
        if (marker) {
            marker = this.addMarker(Ext.applyIf(marker, {
                position: center
            }));
        }

        Ext.each(this.markers, this.addMarker, this);
        this.markers.push(marker);

        if (this.autoComplete) {
            this.addAutoComplete(this.autoComplete);
        }
    },

    addMarker: function (marker) {
        var me = this;
        marker = Ext.apply({
            map: this.gmap
        }, marker);

        if (!marker.position) {
            marker.position = new google.maps.LatLng(marker.lat, marker.lng);
        }
        var o = new google.maps.Marker(marker);
        Ext.Object.each(marker.listeners, function (name, fn) {
            google.maps.event.addListener(o, name, fn);
        });
        return o;
    },

    lookupCode: function (addr, marker) {
        this.geocoder = new google.maps.Geocoder();
        this.geocoder.geocode({
            address: addr
        }, Ext.Function.bind(this.onLookupComplete, this, [marker], true));
    },

    onLookupComplete: function (data, response, marker) {
        if (response != 'OK') {
            Ext.MessageBox.alert('Error', 'An error occured: "' + response + '"');
            return;
        }
        this.createMap(data[0].geometry.location, marker);
    },

    afterComponentLayout: function (w, h) {
        this.callParent(arguments);
        this.redraw();
    },

    redraw: function () {
        var map = this.gmap;
        if (map) {
            google.maps.event.trigger(map, 'resize');
        }
    },

    getAddress: function (latLng, callback) {
        this.geocoder.geocode(
            {
                latLng: latLng
            },
            callback || Ext.emptyFn()
        );
    },

    addAutoComplete: function (autoCompleteOptions) {
        var options = autoCompleteOptions,
            marker;
        if (typeof options != 'object') {
            return false;
        }

        var map = this.gmap;
        var autoComplete = new google.maps.places.Autocomplete(document.getElementById(options.itemId));
        autoComplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();

        if (this.markers.length) {
            marker = this.markers[0];
        } else {
            marker = new google.maps.Marker(
                Ext.apply({
                    map: map
                }, autoCompleteOptions.marker)
            );
        }

        google.maps.event.addListener(autoComplete, 'place_changed', function () {
            infowindow.close();
            var place = autoComplete.getPlace();
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(14);
            }
//            alert(autoComplete.getBounds());
            var image = new google.maps.MarkerImage(
                place.icon, new google.maps.Size(71, 71),
                new google.maps.Point(0, 0), new google.maps.Point(17, 34),
                new google.maps.Size(35, 35)
            );

            marker.setIcon(image);
            marker.setPosition(place.geometry.location);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] &&
                        place.address_components[0].short_name || ''),
                    (place.address_components[1] &&
                        place.address_components[1].short_name || ''),
                    (place.address_components[2] &&
                        place.address_components[2].short_name || '')].join(' ');
            }

            infowindow.setContent('<div><b>' + place.name + '</b><br>' + address +'</div>');
            infowindow.open(map, marker);

            options.callback(autoComplete);
        });

        return true;
    }
})
;
