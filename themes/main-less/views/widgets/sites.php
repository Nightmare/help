<?php
/** @var $this SiteListWidget */
/** @var $sites Site[] */
$host = Yii::app()->params['site'];
$hostParts = explode('.', $host);
$lastPart = array_pop($hostParts);
?>
<ul id="site-nav">
    <?php foreach ($sites as $site) : ?>
    <li>
        <a href="http://<?php echo $site->name; ?>.<?php echo $host; ?>" title="<?php echo $site->title; ?>">
            <mark><?php echo $site->name; ?></mark>.<?php echo join('.', $hostParts); ?>.<span><?php echo $lastPart; ?></span>
        </a>
    </li>
    <?php endforeach; ?>
</ul>