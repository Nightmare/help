<?php
/** @var $mapper array */
$host = Yii::app()->params['site'];
?>
<map name="map">
    <?php foreach ($mapper as $site => $data) : ?>
    <area class="area-<?php echo $site ?>" shape="poly" coords="<?php echo implode(', ', $data['coords']) ?>" href="http://<?php echo $site; ?>.<?php echo $host ?>" />
    <?php endforeach; ?>
</map>