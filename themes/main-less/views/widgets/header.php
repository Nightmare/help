<?php
/** @var $this HeaderWidget */
/** @var $weather Weather */
/** @var $site Site */
$temperature = $weather->temperature;

if (Site::DAY_TIME_NORMAL == $site->time_of_day) {
    $dayType = new DateTime($weather->sunset) <= new DateTime() ? Weather::DATE_TIME_NIGHT : Weather::DATE_TIME_DAY;
} else {
    switch ($site->time_of_day) {
        case Site::DAY_TIME_DARK:
            $dayType = Weather::DATE_TIME_NIGHT;
            break;
        case Site::DAY_TIME_LIGHT:
        default:
            $dayType = Weather::DATE_TIME_DAY;
            break;
    }
}

if ($site->weather_type == Site::WEATHER_TYPE_NORMAL) {
    $type = YText::translit($weather->type);
} else {
    switch ($site->weather_type) {
        case Site::WEATHER_TYPE_BLIZZARD;
            $type = 'metel';
            break;
        case Site::WEATHER_TYPE_CHANCE_OF_RAIN;
            $type = 'peremennaya-oblachnost';
            break;
        case Site::WEATHER_TYPE_SNOW;
            $type = 'sneg';
            break;
        case Site::WEATHER_TYPE_CLEAR;
        case Site::WEATHER_TYPE_SUNNY;
            $type = 'solnechno';
            break;
        case Site::WEATHER_TYPE_FOG;
            $type = 'tuman';
            break;
        case Site::WEATHER_TYPE_RAIN;
            $type = 'dojd';
            break;
        case Site::WEATHER_TYPE_RAIN_AND_THUNDERSTORMS;
            $type = 'dojd-s-grozami';
            break;
        case Site::WEATHER_TYPE_CLOUDY;
        default:
            $type = 'oblachno';
            break;
    }
}

if ($temperature > 0) {
    $temperature = '+' . $temperature;
}
?>
<div id="header-back" class="<?php echo $dayType; ?>">
    <div class="center-box">
        <?php $this->widget('application.widgets.ClockWidget'); ?>
        <div id="weather" class="<?php echo $type; ?>">
            <div class="temperature"><?php echo $temperature ?>°C</div>
            <div class="weather-type"><?php echo Yii::t('temperature', $weather->type); ?></div>
        </div>
    </div>
</div>