<?php
/** @var $this GroupWidget */
/** @var $menuParams array */
?>
<?php if ($menuParams) : ?>
<div class="side-box">
    <nav class="wrapper">
    <?php
$this->widget('zii.widgets.CMenu', array(
    'htmlOptions' => array(
        'class' => 'side-nav',
    ),
    'items' => $menuParams,
));
?>
    </nav>
</div>
<?php endif; ?>