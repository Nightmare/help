<?php
/** @var ClockWidget $this */
$time = new DateTime();
?>
<div id="time">
    <div class="hour-minute">
        <span id="clock-hour"><?php echo $time->format('H'); ?></span>
        <span class="d-dot">:</span>
        <span id="clock-minute"><?php echo $time->format('i'); ?></span>
    </div>
    <div id="clock-day" class="day"><?php echo Yii::t('days', $time->format('l')); ?></div>
</div>