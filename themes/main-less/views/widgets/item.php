<?php
/** @var ItemWidget $this */
/** @var Item[] $items */
?>
<?php if ($items) : ?>
<?php
    $urlParams = array();

    if ($this->category) {
        $urlParams['category'] = $this->category->name;
    }

    if ($this->group) {
        $urlParams['group'] = $this->group->name;
        $group = $this->group;
    }
?>
    <div class="items">
        <?php foreach ($items as $item) : ?>
            <?php
                if (Item::TYPE_MATERIAL == $item->type) {
                    $material = $this->material ? : $item->materials[0];
                    $urlParams['material'] = $material->name;

                    if (!isset($group)) {
                        $group = $material->group;
                    }
                }

                if (!isset($urlParams['group'])) {
                    if (!isset($group)) {
                        $group = $item->group;
                    }

                    $urlParams['group'] = $group->name;
                }

                if (!isset($urlParams['category'])) {
                    $urlParams['category'] = $group->category->name;
                }

                $urlParams['item'] = $item->name;
                $itemFavorite = $item->itemFavorites /*(array('scopes' => array('isFavorite')))*/
                ;
//                if (!empty($itemFavorite)) {
//                    $itemFavorite = reset($itemFavorite);
//                }

                $title = CHtml::encode($item->title);
            ?>

            <div class="item<?php if (!empty($item->topItem)) : ?> vip<?php endif; ?>" data-id="<?php echo $item->id; ?>">
                <div class="inner-top">
                    <?php if (!Yii::app()->user->isGuest) : ?>
                    <div class="is-favorite<?php if ($itemFavorite) : ?> active<?php endif; ?>"></div>
                    <?php endif; ?>
                </div>
                <div class="inner-bottom">
                    <div class="thumbnail">
                        <?php
                        $itemPhotos = $item->itemPhotos;
                        $src = !empty($itemPhotos) ? $itemPhotos[0]->photo->getThumbSmall() : Photo::getNoImage();
                        ?>
                        <img src="<?php echo $src; ?>" alt="<?php echo $title; ?>">
                    </div>
                    <h3 class="item-title">
                        <a href="<?php echo Yii::app()->createUrl('item/view', $urlParams) ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a>
                    </h3>
                    <p><?php echo CHtml::encode($item->description); ?></p>
                    <div class="views"><strong><?php echo $item->view_count; ?></strong></div>
                    <div class="rating-box">
                        <ul>
                            <?php echo str_repeat('<li></li>', $item->rate) ?>
                            <?php echo str_repeat('<li class="empty"></li>', Item::MAX_RATE - $item->rate) ?>
                        </ul>
                    </div>
                    <p><?php echo Yii::t('app', 'Район'); ?>: <strong><?php echo CHtml::encode($item->district->title); ?></strong></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>