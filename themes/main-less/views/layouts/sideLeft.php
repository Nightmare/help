<?php
/** @var $content string */
/** @var $this Controller */
$this->beginContent('//layouts/main'); ?>
    <div id="container">
        <div id="content">
            <?php
            foreach ($this->contentWidgets as $class => $properties) {
                $this->widget($class, $properties);
            }
            ?>
            <?php echo $content; ?>
        </div>
        <!-- #content-->
    </div>
    <!-- #container-->

    <aside id="sideLeft">
        <?php
        foreach ($this->widgets as $class => $properties) {
            $this->widget($class, $properties);
        }
        ?>
    </aside>
    <!-- #sideLeft -->
<?php $this->endContent(); ?>