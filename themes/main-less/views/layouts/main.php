<?php
/** @var $this Controller */
/** @var $content string */
$baseUrl = Yii::app()->theme->baseUrl;
/** @var CClientScript $clientScript */
$clientScript = Yii::app()->getClientScript();
$clientScript
    ->registerCoreScript('jquery', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.selectbox.min.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseUrl . '/js/main.js', CClientScript::POS_END)
    ->registerScript('baseUrl', sprintf('var baseUrl = "%s";', Yii::app()->baseUrl), CClientScript::POS_HEAD)
;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <!--[if IE]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/styles.css?v=<?php echo time(); ?>"/>
    <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
</head>

<body class="<?php echo isset($this->getSite()->name) ? $this->getSite()->name : 'stroy' ?>-page">

<div id="wrapper">

    <header id="header">
        <hr class="top-line">
        <div class="top-navigation">
            <div class="center-box">
                <a href="http://<?php echo Yii::app()->params['site']; ?>" title="" id="logo"></a>
                <?php $this->widget('widgets.SiteListWidget'); ?>
                <div class="header-auth-wrap">
                    <?php $this->widget('widgets.Account.'. (Yii::app()->user->isGuest ? 'Auth' : 'Profile')); ?>
                </div>
                <ul id="lang-nav">
                    <li><a href="#" title="#">rus</a></li>
                    <li><a href="#" title="#">eng</a></li>
                    <li><a href="#" title="#">mda</a></li>
                </ul>
            </div>
        </div>
        <?php $this->widget('widgets.HeaderWidget'); ?>
        <nav id="main-nav">
            <div class="center-box">
                <?php
                $this->widget('widgets.CategoryWidget', array(
                    'site' => $this->getSite(),
                    'category' => $this->getCategory(),
                ));

                $this->widget('zii.widgets.CMenu', array(
                    'htmlOptions' => array(
                        'id' => 'services-menu',
                    ),
                    'items' => array(
                        array(
                            'label' => Yii::t('app', 'услуги'),
                            'url' => array(
                                'service/list',
                            ),
                            'linkOptions' => array('title' => 'услуги'),
                        ),
                    ),
                ));
                ?>
            </div>
        </nav>
    </header>
    <!-- #header-->

    <section id="middle">
        <div class="center-box">
            <?php echo $content; ?>
        </div>
    </section>
    <!-- #middle-->

</div>
<!-- #wrapper -->

<footer id="footer">
    <div class="center-box">
        <strong>Footer:</strong>
    </div>
</footer>
<!-- #footer -->
</body>
</html>