<?php
/** @var $this Controller */
/** @var $content string */
$baseUrl = Yii::app()->theme->baseUrl;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <!--[if IE]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <title></title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="stylesheet" href="<?php echo $baseUrl ?>/css/home.css" type="text/css" media="screen, projection">
</head>

<body>

<div id="wrapper">

    <header id="header">
    </header>
    <!-- #header-->
    <div id="img-map">
        <div class="cc">
            <nav id="top-menu">
                <ul>
                    <li>stroi</li>
                    <li>avto</li>
                    <li>sport</li>
                    <li>otdyh</li>
                    <li>shopping</li>
                </ul>
            </nav>

            <?php echo CHtml::image($baseUrl . '/img/clear.gif', '', array('usemap' => '#map', 'id' => 'menu-map-img')) ?>
            <div id="map-menu-hovers">
                <div class="map-stroy"></div>
                <div class="map-avto"></div>
                <div class="map-sport"></div>
                <div class="map-otdyih"></div>
                <div class="map-shoping"></div>
            </div>
            <?php $this->widget('widgets.HomeMapMenuWidget'); ?>
            <nav id="bottom-menu">
                <ul>
                    <li>stroi</li>
                    <li>avto</li>
                    <li>sport</li>
                    <li>otdyh</li>
                    <li>shopping</li>
                </ul>
            </nav>
        </div>
    </div>
    <section id="middle">
        <div id="nav">
            <nav class="fl-l">
                <ul>
                    <li><a href="#" title="">новости портала</a></li>
                    <li><a href="#" title="">реклама на портале</a></li>
                    <li><a href="#" title="">наши контакты</a></li>
                    <li><a href="#" title="">условия использования</a></li>
                    <li><a href="#" title="">в разработке</a></li>
                </ul>
            </nav>
            <nav id="social" class="fl-r">
                <ul>
                    <li class="vk"><a href="#" title=""></a></li>
                    <li class="facebook"><a href="#" title=""></a></li>
                    <li class="twitter"><a href="#" title=""></a></li>
                    <li class="odnoklassniki"><a href="#" title=""></a></li>
                </ul>
            </nav>
        </div>
        <section class="holder">
            <div id="container">
                <div id="content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquid, cupiditate impedit -
                        <strong>help.md</strong></p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At ducimus eaque et fugiat incidunt
                        iste tempora vero. Ab blanditiis, eligendi excepturi fuga fugiat id, nemo nihil nisi, ratione
                        recusandae saepe.</p>

                    <p>
                        <strong>stroi</strong> - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci
                        aliquid
                        <br/>
                        <strong>avto</strong> - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci
                        aliquid
                        <br/>
                        <strong>sport</strong> - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci
                        aliquid
                        <br/>
                        <strong>otdyh</strong> - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci
                        aliquid
                        <br/>
                        <strong>shopping</strong> - Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci
                        aliquid
                    </p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda aut deserunt dicta molestias
                        nihil quia suscipit! Alias expedita itaque iure neque nisi perspiciatis recusandae voluptate
                        voluptates. Eos illum laboriosam quibusdam.</p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur esse et facilis id maxime!
                        Ab at dicta doloremque eos maxime minus nam numquam omnis possimus sint ut, voluptate. Beatae,
                        cumque dicta dolorem ducimus eaque ex expedita ipsam optio perferendis quae.</p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque, nobis, veritatis? Accusamus
                        dolores dolorum eveniet ex fugiat laudantium magni sint.</p>

                    <div class="advertisement">
                        По вопросам размещения рекламы на портале посетите раздел <a href="#" title="">реклама на
                            портале</a>
                    </div>
                    <?php echo CHtml::image($baseUrl . '/img/home/content_img.png'); ?>
                </div>
                <!-- #content-->
            </div>
            <!-- #container-->

            <aside id="sideLeft">
                <div class="list">
                    <div class="list-item">
                        <h1>
                            <span class="fl-l">Заголовок новости портала</span>
                            <span class="date fl-r">18.08.2013</span>
                        </h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci dolorem ea est incidunt
                            ipsam nobis perspiciatis quis recusandae sapiente soluta.</p>
                    </div>
                    <div class="list-item">
                        <h1>
                            <span class="fl-l">Расширенные обьявления</span>
                            <span class="date fl-r">11.08.2013</span>
                        </h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                    <div class="list-item">
                        <h1>
                            <span class="fl-l">Последний месяц лета</span>
                            <span class="date fl-r">01.08.2013</span>
                        </h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci dolorem ea est incidunt
                            ipsam nobis perspiciatis quis recusandae sapiente soluta.ipsam nobis perspiciatis quis
                            recusandae sapiente soluta.</p>
                    </div>
                    <div class="list-item">
                        <h1>
                            <span class="fl-l">Социальные сети</span>
                            <span class="date fl-r">11.08.2013</span>
                        </h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                    <div class="list-item">
                        <h1>
                            <span class="fl-l">В разработке</span>
                            <span class="date fl-r">18.08.2013</span>
                        </h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci dolorem ea est incidunt
                            ipsam nobis perspiciatis quis recusandae sapiente soluta.</p>
                    </div>
                    <div class="list-item">
                        <h1>
                            <span class="fl-l">Расширенные обьявления</span>
                            <span class="date fl-r">11.08.2013</span>
                        </h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
            </aside>
            <!-- #sideLeft -->
        </section>

    </section>
    <!-- #middle-->

</div>
<!-- #wrapper -->

<footer id="footer">
    company name :: <a href="#" title="">help.md</a> © 2012-2013 :: all rights reserved
</footer>
<!-- #footer -->

<script src="//code.jquery.com/jquery-1.8.2.min.js"></script>
<script>
    $(document).ready(function() {
        var $d = $(document);

        $d
            .on('mouseover', 'map area', function() {
                var $this = $(this),
                    cssClass = $.trim($this.attr('class')).replace('area-', '');
                $('.map-' + cssClass).fadeIn('fast');
            })
            .on('mouseleave', 'map area, [class^=map-]', function() {
                var $this = $(this),
                    cssClass = $.trim($this.attr('class')).replace('area-', '');
                $('.map-' + cssClass).fadeOut('fast');
            });
    });
</script>
</body>
</html>