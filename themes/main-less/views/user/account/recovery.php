<?php //$this->pageTitle = Yii::t('UserModule.user', 'Восстановление пароля'); ?>

<h2><?php echo Yii::t('UserModule.user', 'Восстановление пароля'); ?></h2>

<?php //$this->widget('application.modules.yupe.widgets.FlashMessages'); ?>

<p><?php echo Yii::t('UserModule.user', 'Для восстановления пароля - введите email, указанный при регистрации.'); ?></p>

<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'recovery-form',
        'action' => array('/user/account/recovery'),
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'afterValidate' => 'js:function(form, data, hasError) {
                if (!hasError) {
                    window.location.href = data.url;
                }
            }',
        ),
        'htmlOptions' => array(
            'class' => 'form-horizontal',
            'method' => 'post'
        ),
    )); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email'); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="row submit">
        <?php echo CHtml::submitButton(Yii::t('UserModule.user', 'Восстановить пароль')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->