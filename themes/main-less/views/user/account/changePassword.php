<?php $this->pageTitle = Yii::t('UserModule.user', 'Сменя пароля'); ?>

<h1><?php echo Yii::t('UserModule.user', 'Восстановление пароля'); ?></h1>

<p><?php echo Yii::t('UserModule.user', 'Укажите свой новый пароль!'); ?></p>

<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'change-password-form',
        'action' => array('/user/account/recoveryPassword'),
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'afterValidate' => 'js:function(form, data, hasError) {
                if (!hasError) {
//                    window.location.href = data.url;
                }
            }',
        ),
        'htmlOptions' => array(
            'class' => 'form-horizontal',
            'method' => 'post'
        ),
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'password'); ?>
        <?php echo $form->passwordField($model, 'password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'cPassword'); ?>
        <?php echo $form->passwordField($model, 'cPassword'); ?>
    </div>

    <div class="row submit">
        <?php echo CHtml::submitButton(Yii::t('UserModule.user', 'Изменить пароль')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->