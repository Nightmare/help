<div class="login-box">
    <div class="icons">
        <a href=""><i class="halflings-icon home"></i></a>
        <a href=""><i class="halflings-icon cog"></i></a>
    </div>
    <h2>Регистрация</h2>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'registration-form',
        'action' => array('/user/account/registration'),
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'afterValidate' => 'js:function(form, data, hasError) {
                if (!hasError && data.hasOwnProperty("url")) {
                    window.location.href = data.url;
                }
            }',
        ),
        'htmlOptions' => array(
            'class' => 'form-horizontal',
            'method' => 'POST'
        ),
    )); ?>

    <div class="input-prepend control-group" title="Nick name">
        <span class="add-on"><i class="halflings-icon user"></i></span>
        <?php echo $form->textField($model, 'nick_name', array('class' => 'input-large span10', 'placeholder' => 'type nick name')); ?>
        <?php echo $form->error($model, 'nick_name'); ?>
    </div>
    <div class="clearfix"></div>

    <div class="input-prepend control-group" title="Email">
        <span class="add-on"><i class="halflings-icon user"></i></span>
        <?php echo $form->textField($model, 'email', array('class' => 'input-large span10', 'placeholder' => 'type email')); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>
    <div class="clearfix"></div>

    <div class="input-prepend control-group" title="Password">
        <span class="add-on"><i class="halflings-icon lock"></i></span>
        <?php echo $form->passwordField($model, 'password', array('class' => 'input-large span10', 'placeholder' => 'type password')); ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>
    <div class="clearfix"></div>
    <div class="input-prepend control-group" title="Repeated password">
        <span class="add-on"><i class="halflings-icon lock"></i></span>
        <?php echo $form->passwordField($model, 'cPassword', array('class' => 'input-large span10', 'placeholder' => 'type password 2')); ?>
        <?php echo $form->error($model, 'cPassword'); ?>
    </div>

    <div class="button-login">
        <?php echo CHtml::submitButton('Register me!', array('class' => 'btn btn-primary')); ?>
    </div>
    <div class="clearfix"></div>
    <?php $this->endWidget(); ?>
</div>