<div class="login-box" id="login-form-wrap">
    <div class="icons">
        <a href="http://<?php echo Yii::app()->params['site']; ?>"><i class="halflings-icon home"></i></a>
        <a href=""><i class="halflings-icon cog"></i></a>
    </div>
    <h2>Авторизация</h2>

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'action' => array('/login'),
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'afterValidate' => 'js:function(form, data, hasError) {
                if (!hasError) {
                    window.location.href = data.url;
                }
            }',
        ),
        'htmlOptions' => array(
            'class' => 'form-horizontal'
        ),
    )); ?>

    <div class="input-prepend control-group" title="Username">
        <span class="add-on"><i class="halflings-icon user"></i></span>
        <?php echo $form->textField($model, 'nick_name', array('class' => 'input-large span10', 'placeholder' => 'Введите никнейм')); ?>
        <?php echo $form->error($model, 'nick_name'); ?>
    </div>
    <div class="clearfix"></div>

    <div class="input-prepend control-group" title="Password">
        <span class="add-on"><i class="halflings-icon lock"></i></span>
        <?php echo $form->passwordField($model, 'password', array('class' => 'input-large span10', 'placeholder' => 'Введите пароль')); ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>
    <div class="clearfix"></div>

    <label class="remember" for="LoginForm_rememberMe">
        <div class="checker">
            <?php echo $form->checkBox($model, 'rememberMe'); ?>
        </div>
        Запомнить меня
    </label>

    <div class="button-login">
        <?php echo CHtml::submitButton(Yii::t('app', 'Ввойти'), array('class' => 'btn btn-primary')); ?>
    </div>
    <div class="clearfix"></div>
    <hr />

    <!--    <h3>Забыли пароль?</h3>-->
    <!--    <p>Без проблем, --><?php //echo CHtml::link('кликните сюда', '/user/account/ForgotPassword'); ?><!-- для получение нового пароля.</p>-->
    <?php $this->endWidget(); ?>
</div>