(function(window) {
    'use strict';

    window.initializeGoogleMaps = function() {
        var geoPosition = new google.maps.LatLng(_GG.lat, _GG.lng),
            mapOptions = {
                zoom: 15,
                center: geoPosition
            },
            map = new google.maps.Map(document.getElementById('gg-map'), mapOptions);

        new google.maps.Marker({
            position: geoPosition,
            map: map,
            title: _GG.address,
            icon: _GG.marker
        });
    };

    window.onload = function() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?v=3.10&sensor=false&callback=initializeGoogleMaps';
        document.body.appendChild(script);
    };
})(window);