var Clock = function(options) {
    'use strict';
    /**
     *
     * @type {Date}
     */
    var date,
        days = ['Воскресение', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        _convertTime = function(time) {
            return ((time.toString().length > 1) ? '' : '0') + time;
        },
        _getDay = function() {
            return days[date.getDay()];
        };

    setInterval(function() {
        date = new Date();
        options.update(_getDay(), _convertTime(date.getHours()), _convertTime(date.getMinutes()));
    }, 1000);
};

(function (window, document) {
    var _minute = document.getElementById('clock-minute'),
        _hour = document.getElementById('clock-hour'),
        _day = document.getElementById('clock-day');

    return new Clock({
        update: function (day, hour, minute) {
            if (_minute.innerText != minute) {
                _minute.textContent = minute;
            }

            if (_hour.innerText != hour) {
                _hour.textContent = hour;
            }

            if (_day.innerText != day) {
                _day.textContent = day;
            }
        }
    });
})(window, window.document);