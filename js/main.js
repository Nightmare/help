(function (window, document, $) {
    var $d = $(document),
        $profileBox = $d.find('.profile-info');

    window.baseUrl = window.baseUrl || '/';

    $d.on('click', '.is-favorite', function () {
        var $this = $(this);
        $this.toggleClass('active');

//        var $favoritesCountEl = $('#favorites_count');
        $.ajax({
            url: window.baseUrl + '/item/ToggleFavorites/' + $this.closest('.item').data('id'),
            dataType: 'json',
            success: function (result) {
                if (result.success) {
//                    $favoritesCountEl.text(parseInt($favoritesCountEl.text(), 10) + (result.data.type == 'minus' ? -1 : 1));
                }
            }
        });
    });

    $d
        .on('click', function () {
            $profileBox.removeClass('active');
        })
        .on('click', '.header-auth', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $profileBox.toggleClass('active');
        });

    //todo refactor
    $('select').selectbox();
    if ($.fancybox) {
        $('.swiper-slide').fancybox({
            padding: 14,
            helpers: {
                overlay: {
                    locked: false
                },
                thumbs: {
                    width: 50,
                    height: 50
                }
            }
        });

        var swipe = new Swiper('.swiper-container', {
            slidesPerView: 'auto'
        });
    }


    $d.on('click', '.item-carousel > .arrow', function (e) {
        e.preventDefault();
        swipe['swipe' + ($(this).hasClass('next') ? 'Next' : 'Prev')]();
    });


})(window, window.document, jQuery);