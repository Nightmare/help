<?php
class YandexCommand extends CConsoleCommand
{
    const WEATHER_URL = 'http://export.yandex.ru/weather-ng/forecasts/';

    /**
     * @param int $cityId - Chisinau ID
     * @return bool
     */
    public function actionWeather($cityId = 33815)
    {
        try {
            $weatherXml = simplexml_load_file(self::WEATHER_URL . sprintf('%d.xml', (int)$cityId));
        } catch (CHttpException $e) {
            echo 'CANNOT PARSE WEATHER';
            return false;
        }

        $day = $weatherXml->day[0];
        $date = (string)$day['date'];

        $weather = new Weather();
        $weather->setAttributes(array(
            'temperature' => (string)$weatherXml->fact->temperature,
            'type' => (string)$weatherXml->fact->weather_type,
            'sunrise' => date('Y-m-d H:i:s', strtotime($date . ' ' . $day->sunrise)),
            'sunset' => date('Y-m-d H:i:s', strtotime($date . ' ' . $day->sunset)),
            'datetime' => new CDbExpression('NOW()'),
        ));

        return (bool)$weather->save();
    }
}