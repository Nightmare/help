<?php

Yii::import('application.models._base.BaseSite');

class Site extends BaseSite
{
    const DAY_TIME_NORMAL = 0;
    const DAY_TIME_LIGHT = 1;
    const DAY_TIME_DARK = 2;

    const WEATHER_TYPE_NORMAL = 0;
    const WEATHER_TYPE_SUNNY = 1;
    const WEATHER_TYPE_RAIN = 2;
    const WEATHER_TYPE_SNOW = 3;
    const WEATHER_TYPE_BLIZZARD = 4;
    const WEATHER_TYPE_CLOUDY = 5;
    const WEATHER_TYPE_RAIN_AND_THUNDERSTORMS = 6;
    const WEATHER_TYPE_FOG = 7;
    const WEATHER_TYPE_CHANCE_OF_RAIN = 8;
    const WEATHER_TYPE_CLEAR = 9;

    private static $_site;
    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function setCurrentSite(Site $site)
    {
        self::$_site = $site;
    }

    /**
     * @return $this
     */
    public static function getCurrentSite()
    {
        return self::$_site;
    }
}