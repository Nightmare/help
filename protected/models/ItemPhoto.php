<?php

Yii::import('application.models._base.BaseItemPhoto');
/**
 * Class ItemPhoto
 * @property Photo $photo
 * @method Photo photo($params = array())
 *
 * @property Item $item
 * @method Item item($params = array())
 */
class ItemPhoto extends BaseItemPhoto
{
    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return CMap::mergeArray(
            parent::relations(),
            array(
                'photo' => array(self::BELONGS_TO, 'Photo', 'photo_id'),
                'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
            )
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias(true);
        return array(
            'onlyMain' => array(
                'condition' => $alias . '.is_main = 1'
            ),
            'orderByMain' => array(
                'order' => $alias . '.is_main ASC'
            )
        );
    }
}