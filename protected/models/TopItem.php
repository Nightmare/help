<?php
Yii::import('application.models._base.BaseTopItem');
class TopItem extends BaseTopItem
{
    const SHOW_INFINITE = -1;
    /**
    * @param string $className
    * @return $this
    */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->paid_at = new CDbExpression('NOW()');
        }

        return parent::beforeValidate();
    }
}