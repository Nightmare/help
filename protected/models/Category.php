<?php

Yii::import('application.models._base.BaseCategory');
/**
 * Class Category
 * @property Material $materials
 * @method Material materials($params = array())
 * @method Group[] groups($params = array())
 */
class Category extends BaseCategory
{
    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return CMap::mergeArray(
            parent::relations(),
            array(
                'materials' => array(self::HAS_MANY, 'Material', array('id' => 'group_id'), 'through' => 'groups'),
            )
        );
    }

    /**
     * @return $this
     */
    public function bySequence()
    {
        $this->getDbCriteria()->mergeWith(array(
            'order' => 'sequence ASC',
        ));

        return $this;
    }
}