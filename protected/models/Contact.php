<?php

Yii::import('application.models._base.BaseContact');

class Contact extends BaseContact
{
    const TYPE_SITE = 'site';
    const TYPE_FAX = 'fax';
    const TYPE_EMAIL = 'email';
    const TYPE_MOBILE = 'mobile';
    const TYPE_SKYPE = 'skype';
    const TYPE_LANDLINE = 'landline';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}