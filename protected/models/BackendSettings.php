<?php
Yii::import('application.models._base.BaseBackendSettings');
class BackendSettings extends BaseBackendSettings
{
    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}