<?php

Yii::import('application.models._base.BaseWeather');

class Weather extends BaseWeather
{
    const DATE_TIME_DAY = 'day';
    const DATE_TIME_NIGHT = 'night';

    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}