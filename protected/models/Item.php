<?php

Yii::import('application.models._base.BaseItem');
/**
 * Class Item
 * @property ItemPhoto[] $itemPhotos
 * @method ItemPhoto[] itemPhotos($params = array())
 * @method Item orderByRate()
 * @method Item orderByVies()
 *
 * @method Material[] materials($params = array())
 * @property Material[] $materials
 *
 * @method Group group($params = array())
 * @property Group $group
 *
 * @method ItemFilterValue[] itemFilterValues($params = array())
 * @property ItemFilterValue[] $itemFilterValues
 *
 * @property CUploadedFile $marker
 * @method TopItem topItem($params = array())
 */
class Item extends BaseItem
{
    const TYPE_GROUP = 1;
    const TYPE_MATERIAL = 0;
    const MAX_RATE = 5;

    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return CMap::mergeArray(
            parent::rules(),
            array(
                array(
                    'marker', 'file',
                    'types' => 'jpg, gif, png',
                    'maxSize' => 5242880, // 5Mb in bytes
                    'on' => 'create',
                ),
            )
        );
    }

    public function relations()
    {
        $relations = parent::relations();
        unset($relations['photos'], $relations['filterValues']);

        return CMap::mergeArray(
            $relations,
            array(
                'itemFilterValues' => array(self::HAS_MANY, 'ItemFilterValue', 'item_id'),
                'filterValues' => array(self::HAS_MANY, 'FilterValue', 'filter_value_id', 'through' => 'itemFilterValues'),

                'itemPhotos' => array(self::HAS_MANY, 'ItemPhoto', 'item_id'),
                'photos' => array(self::HAS_MANY, 'Photo', 'photo_id', 'through' => 'itemPhotos'),

                'templateMaterial' => array(self::BELONGS_TO, 'Template', 'template_id'),
                'templateMaterials' => array(self::HAS_MANY, 'TemplateMaterial', array('id' => 'template_id'), 'through' => 'templateMaterial'),
                'materials' => array(self::HAS_MANY, 'Material', array('material_id' => 'id'), 'through' => 'templateMaterials'),

                'templateGroup' => array(self::BELONGS_TO, 'Template', 'template_id'),
                'group' => array(self::HAS_ONE, 'Group', array('group_id' => 'id'), 'through' => 'templateGroup'),
            )
        );
    }

    public function scopes()
    {
        return array(
            'orderByRate' => array(
                'order' => 'rate DESC',
            ),
            'orderByVies' => array(
                'order' => 'view_count DESC',
            )
        );
    }

    /**
     * @return string
     */
    public function getMarkerPath()
    {
        return Yii::app()->getBasePath() . "/../img/upload/marker/{$this->id}";
    }

    /**
     * @return string
     */
    public function getMarkerUrl()
    {
        return Yii::app()->getBaseUrl() . "/img/upload/marker/{$this->id}";
    }

    public function getMarker($isAbsolute = false)
    {
        return ($isAbsolute ? $this->getMarkerPath() : $this->getMarkerUrl()) . '/' . $this->marker;
    }

    public function getThumbMarker($isAbsolute = false)
    {
        return ($isAbsolute ? $this->getMarkerPath() : $this->getMarkerUrl()) . '/100x_-' . $this->marker;
    }
}