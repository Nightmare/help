<?php

Yii::import('application.models._base.BaseItemFavorites');

/**
 * Class ItemFavorites
 * @property Item $item
 * @property User $user
 */
class ItemFavorites extends BaseItemFavorites
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function scopes()
    {
        return array(
            'onlyFavorite' => array(
                'condition' => $this->getTableAlias(true) . '.user_id = :user_id',
                'params' => array(
                    ':user_id' => Yii::app()->user->id,
                ),
            ),
        );
    }
}