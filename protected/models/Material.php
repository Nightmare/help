<?php

Yii::import('application.models._base.BaseMaterial');

/**
 * Class Material
 * @method Item[] items($params = array())
 * @method TemplateMaterial[] templateMaterials($params = array())
 * @method Template[] templates($params = array())
 *
 * @property TemplateMaterial[] $templateMaterials
 * @property FilterMaterial[] $filterMaterials
 * @property Template[] $templates
 * @property Item[] $items
 */
class Material extends BaseMaterial
{
    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array_merge(
            parent::relations(),
            array(
                'templateMaterials' => array(self::HAS_MANY, 'TemplateMaterial', 'material_id'),
                'templates' => array(self::HAS_MANY, 'Template', array('template_id' => 'id'), 'through' => 'templateMaterials'),
                'items' => array(self::HAS_MANY, 'Item', array('id' => 'template_id'), 'through' => 'templates'),
                'filterMaterials' => array(self::HAS_MANY, 'FilterMaterial', array('material_id' => 'id')),
                'filters' => array(self::HAS_MANY, 'Filter', array('filter_id' => 'id'), 'through' => 'filterMaterials'),
            )
        );
    }
}