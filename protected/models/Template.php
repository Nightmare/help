<?php

Yii::import('application.models._base.BaseTemplate');
/**
 * Class Template
 *
 * @property FilterGroup[] $templateFilterGroups
 * @property FilterMaterial[] $templateFilterMaterials
 *
 * @method Group group($params = array())
 * @method Material[] materials($params = array())
 */
class Template extends BaseTemplate
{
    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'items' => array(self::HAS_MANY, 'Item', 'template_id'),
            'group' => array(self::BELONGS_TO, 'Group', 'group_id'),

            'templateMaterials' => array(self::HAS_MANY, 'TemplateMaterial', 'template_id'),
            'materials' => array(self::HAS_MANY, 'Material', array('material_id' => 'id'), 'through' => 'templateMaterials'),

            'templateFilterMaterials' => array(self::HAS_MANY, 'FilterMaterial', array('material_id' => 'material_id'), 'through' => 'templateMaterials'),
            'templateFilterGroups' => array(self::HAS_MANY, 'FilterGroup', array('id' => 'group_id'), 'through' => 'group'),
        );
    }
}