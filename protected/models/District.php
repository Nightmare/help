<?php
Yii::import('application.models._base.BaseDistrict');
class District extends BaseDistrict
{
    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}