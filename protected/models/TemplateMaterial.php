<?php

Yii::import('application.models._base.BaseTemplateMaterial');
/**
 * Class TemplateMaterial
 * @property Material $material
 * @property Template $template
 */
class TemplateMaterial extends BaseTemplateMaterial
{
    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return CMap::mergeArray(
            parent::relations(),
            array(
                'template' => array(self::BELONGS_TO, 'Template', 'template_id'),
                'material' => array(self::BELONGS_TO, 'Material', 'material_id'),
            )
        );
    }
}