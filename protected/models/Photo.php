<?php

Yii::import('application.models._base.BasePhoto');

class Photo extends BasePhoto
{
    const THUMB_SIZE = 200;
    const THUMB_SMALL_SIZE = 135;
    const THUMB_LARGE_SIZE = 800;
    const THUMB_ITEM_SIZE_WIDTH = 150;
    const THUMB_ITEM_SIZE_HEIGHT = 95;

    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getNoImage()
    {
        return Yii::app()->baseUrl . '/img/no-image.gif';
    }

    public function getThumb()
    {
        return $this->getImageUrl() . sprintf('/%dx%d-%s', self::THUMB_SIZE, self::THUMB_SIZE, $this->file_name);
    }

    public function getThumbItemSize()
    {
        return $this->getImageUrl() . sprintf('/%dx%d-%s', self::THUMB_ITEM_SIZE_WIDTH, self::THUMB_ITEM_SIZE_HEIGHT, $this->file_name);
    }

    public function getImageUrl()
    {
        return Yii::app()->getBaseUrl() . "/img/upload/photo/{$this->id}";
    }

    public function getThumbSmall()
    {
        return $this->getImageUrl() . sprintf('/%dx%d-%s', self::THUMB_SMALL_SIZE, self::THUMB_SMALL_SIZE, $this->file_name);
    }

    public function getThumbLarge()
    {
        return $this->getImageUrl() . sprintf('/%dx%d-%s', self::THUMB_LARGE_SIZE, self::THUMB_LARGE_SIZE, $this->file_name);
    }

    public function getImage()
    {
        return $this->getImageUrl() . "/{$this->file_name}";
    }

    public function delete($deleteFiles = true)
    {
        if (($isSuccess = parent::delete()) && $deleteFiles) {
            YFile::rmDir($this->getImagePath());
        }

        return $isSuccess;
    }

    public function getImagePath()
    {
        return Yii::app()->getBasePath() . "/../img/upload/photo/{$this->id}";
    }
}