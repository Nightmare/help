<?php

/**
 * This is the model base class for the table "group".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Group".
 *
 * Columns in table "group" available as properties of the model,
 * followed by relations of table "group" available as properties of the model.
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $title
 *
 * @property Filter[] $filters
 * @property Category $category
 * @property Material[] $materials
 * @property Template[] $templates
 */
abstract class BaseGroup extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'group';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Group|Groups', $n);
	}

	public static function representingColumn() {
		return 'name';
	}

	public function rules() {
		return array(
			array('category_id, name, title', 'required'),
			array('category_id', 'numerical', 'integerOnly'=>true),
			array('name, title', 'length', 'max'=>50),
			array('id, category_id, name, title', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'filters' => array(self::MANY_MANY, 'Filter', 'filter_group(group_id, filter_id)'),
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
			'materials' => array(self::HAS_MANY, 'Material', 'group_id'),
			'templates' => array(self::HAS_MANY, 'Template', 'group_id'),
		);
	}

	public function pivotModels() {
		return array(
			'filters' => 'FilterGroup',
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'category_id' => null,
			'name' => Yii::t('app', 'Name'),
			'title' => Yii::t('app', 'Title'),
			'filters' => null,
			'category' => null,
			'materials' => null,
			'templates' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('title', $this->title, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}