<?php

Yii::import('application.models._base.BaseGroup');
/**
 * Class GroupController
 * @method Material[] materials($params = array())
 *
 * @method Item[] items($params = array())
 * @property Item[] items
 * @property FilterGroup[] $filterGroups
 */
class Group extends BaseGroup
{
    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return CMap::mergeArray(
            parent::relations(),
            array(
//                'materialItems' => array(self::HAS_MANY, 'ItemMaterial', array('id' => 'material_id'), 'through' => 'materials'),
                'items' => array(self::HAS_MANY, 'Item', array('id' => 'template_id'), 'through' => 'templates'),
                'filterGroups' => array(self::HAS_MANY, 'FilterGroup', array('group_id' => 'id')),
            )
        );
    }
}