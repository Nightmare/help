<?php
class SeoRule extends CBaseUrlRule
{
    const SECONDS_IN_DAY = 86400;

    const TYPE_RULE_CATEGORY = 'category';
    const TYPE_RULE_GROUP = 'group';
    const TYPE_RULE_MATERIAL = 'material';
    const TYPE_RULE_ITEM = 'item';

    public $connectionID = 'db';

    /**
     * @param CUrlManager $manager
     * @param string $route
     * @param array $params
     * @param string $ampersand
     *
     * @return bool|string
     */
    public function createUrl($manager, $route, $params, $ampersand)
    {
        $routes = $this->_getRoutes();

        if (false === array_search($route, $routes)) {
            return false;
        }

        if ($route === $routes[self::TYPE_RULE_CATEGORY]) {
            return isset($params['category'])
                ? $params['category']
                : false;
        }

        if ($route === $routes[self::TYPE_RULE_GROUP]) {
            return isset($params['category'], $params['group'])
                ? $params['category'] . '/' . $params['group']
                : false;
        }

        if ($route === $routes[self::TYPE_RULE_MATERIAL]) {
            return isset($params['category'], $params['group'], $params['material'])
                ? $params['category'] . '/' . $params['group'] . '/' . $params['material']
                : false;
        }

        if ($route === $routes[self::TYPE_RULE_ITEM]) {
            if (isset($params['category'], $params['group'], $params['item'])) {
                $url = $params['category'] . '/' . $params['group'] . '/';
                if (isset($params['material'])) {
                    $url .= $params['material'] . '/';
                }
                return $url . $params['item'];
            }
        }

        return false;
    }

    /**
     * @static
     *
     * @return array
     */
    protected static function _getRoutes()
    {
        return array(
            self::TYPE_RULE_CATEGORY => 'category/view',
            self::TYPE_RULE_GROUP => 'group/view',
            self::TYPE_RULE_MATERIAL => 'material/view',
            self::TYPE_RULE_ITEM => 'item/view',
        );
    }

    /**
     * @param CUrlManager $manager
     * @param CHttpRequest $request
     * @param string $pathInfo
     * @param string $rawPathInfo
     *
     * @return bool|string
     */
    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
    {
        if ($pathInfo === '') {
            return false;
        }

        $paths = explode('/', $pathInfo);

        if (($count = count($paths)) > 4) {
            return false;
        }

        $siteName = str_replace('.' . Yii::app()->params['site'], '', $_SERVER['HTTP_HOST']);
        $siteName == Yii::app()->params['site'] && ($siteName = '');

        /** @var Site $site */
        if (!$siteName || !($site = Site::model()->cache(self::SECONDS_IN_DAY)->findByAttributes(array('name' => $siteName)))) {
            return false;
        }

        /** @var $category Category */
        if (!($category = Category::model()->cache(self::SECONDS_IN_DAY)->findByAttributes(array('name' => $paths[0], 'site_id' => $site->id)))) {
            return false;
        }
        $routes = $this->_getRoutes();
        $params = array('category' => $category->name);

        if ($count == 1) {
            $this->_assignGetParams($params);
            return $routes[self::TYPE_RULE_CATEGORY];
        }

        /** @var $group Group */
        if (!($group = Group::model()->cache(self::SECONDS_IN_DAY)->findByAttributes(array('name' => $paths[1], 'category_id' => $category->id)))) {
            return false;
        }
        $params['group'] = $group->name;

        if ($count == 2) {
            $this->_assignGetParams($params);
            return $routes[self::TYPE_RULE_GROUP];
        }

        /** @var $material Material */
        if (!($material = Material::model()->cache(self::SECONDS_IN_DAY)->findByAttributes(array('name' => $paths[2], 'group_id' => $group->id)))) {
            if ($items = $group->items(array(
                'condition' => 'name = :name',
                'params' => array(':name' => $paths[2]),
                'limit' => 1,
                ))
            ) {
                $params['item'] = $items[0]->name;
                $params['material'] = '';
                $routType = self::TYPE_RULE_ITEM;
            } else {
                return false;
            }
        } else {
            $params['material'] = $material->name;
            $routType = self::TYPE_RULE_MATERIAL;
        }

        if ($count == 3) {
            $this->_assignGetParams($params);
            return $routes[$routType];
        }

        /** @var $item Item */
        if (!($item = Item::model()->cache(self::SECONDS_IN_DAY)->with(array('materials' => array('id' => $material->id)))->findByAttributes(array('name' => $paths[3])))) {
            return false;
        }
        $params['item'] = $item->name;
        $this->_assignGetParams($params);

        return $routes[self::TYPE_RULE_ITEM];
    }

    /**
     * Assign params to $_GET
     *
     * @param $params
     * @return $this
     */
    protected function _assignGetParams($params)
    {
        foreach ($params as $key => $value) {
            $_GET[$key] = $value;
        }

        return $this;
    }
}