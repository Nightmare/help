<?php
class HelperBehavior extends CBehavior
{
    private $helpers = array();

    /**
     * @param $helper
     * @return Object
     */
    public function getHelper($helper)
    {
        if (!array_key_exists($helper, $this->helpers)) {
            $class = ucfirst($helper);
            Yii::import('application.helpers.'. $class);
            $this->helpers[$helper] = new $class();
        }
        return $this->helpers[$helper];
    }
} 