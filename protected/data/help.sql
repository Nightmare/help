-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2014 at 10:11 PM
-- Server version: 5.6.14
-- PHP Version: 5.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `help`
--

-- --------------------------------------------------------

--
-- Table structure for table `backend_settings`
--

CREATE TABLE IF NOT EXISTS `backend_settings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `wallpaper` varchar(255) DEFAULT NULL,
  `wallpaper_stretch` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `backend_settings`
--

INSERT INTO `backend_settings` (`id`, `user_id`, `wallpaper`, `wallpaper_stretch`) VALUES
(1, 21, '/themes/extjs/images/wallpapers/1275357739_summer-feelings-1920-1200-5439.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `name` varchar(35) NOT NULL,
  `title` varchar(35) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_id_2` (`site_id`,`name`),
  KEY `site_id` (`site_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `site_id`, `name`, `title`) VALUES
(7, 1, 'novyiy-taytl', 'новый тайтл'),
(9, 3, 'novaya-kategoriya', 'новая категория');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `text` text NOT NULL,
  `created_at` datetime NOT NULL,
  `is_approved` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('mobile','landline','skype','email','site','fax') NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `type`, `value`) VALUES
(29, 'email', 'chi_no@ukr.net'),
(30, 'skype', 'zloy.nightmare'),
(31, 'email', 'chi_no@ukr.net'),
(32, 'site', 'http://www.site.com'),
(33, 'mobile', '+3759383468');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE IF NOT EXISTS `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `title` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `name`, `title`) VALUES
(2, 'chekanyi', 'Чеканы'),
(4, 'novyiy-rayon', 'Новый район');

-- --------------------------------------------------------

--
-- Table structure for table `filter`
--

CREATE TABLE IF NOT EXISTS `filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` enum('select','checkbox','radio') NOT NULL DEFAULT 'checkbox',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `filter`
--

INSERT INTO `filter` (`id`, `title`, `name`, `type`) VALUES
(2, 'Марка', 'marka', 'checkbox'),
(3, 'Тип двигателя', 'tip-dvigatelya', 'checkbox'),
(4, 'Цвет', 'tsvet', 'checkbox'),
(5, 'Test', 'test', 'checkbox');

-- --------------------------------------------------------

--
-- Table structure for table `filter_group`
--

CREATE TABLE IF NOT EXISTS `filter_group` (
  `filter_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`filter_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `filter_group`
--

INSERT INTO `filter_group` (`filter_id`, `group_id`) VALUES
(3, 1),
(4, 1),
(3, 2),
(4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `filter_material`
--

CREATE TABLE IF NOT EXISTS `filter_material` (
  `filter_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  PRIMARY KEY (`filter_id`,`material_id`),
  KEY `material_id` (`material_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `filter_value`
--

CREATE TABLE IF NOT EXISTS `filter_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filter_id_2` (`filter_id`,`name`),
  KEY `filter_id` (`filter_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `filter_value`
--

INSERT INTO `filter_value` (`id`, `filter_id`, `title`, `name`) VALUES
(24, 2, 'Mercedes', 'mercedes'),
(25, 2, 'BMW', 'bmw'),
(26, 2, 'Audi', 'audi'),
(27, 3, 'Бензин', 'benzin'),
(28, 3, 'Дизель', 'dizel'),
(29, 4, 'Белый', 'belyiy'),
(30, 4, 'Красный', 'krasnyiy'),
(31, 5, 'мроар', 'mroar');

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE IF NOT EXISTS `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_id_2` (`category_id`,`name`),
  KEY `category_id` (`category_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`id`, `category_id`, `name`, `title`) VALUES
(1, 9, 'group', 'group'),
(2, 9, 'gruppa1', 'Группа1'),
(3, 9, 'dsadsadsa', 'dsadsadsa');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `district_id` int(11) NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `marker` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `rate` smallint(6) NOT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `district_id` (`district_id`),
  KEY `template_id` (`template_id`),
  KEY `name` (`name`),
  KEY `rate` (`rate`,`view_count`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `district_id`, `template_id`, `marker`, `type`, `name`, `title`, `description`, `address`, `lat`, `lng`, `rate`, `view_count`) VALUES
(13, 2, 21, 'cart.png', 1, 'test', 'Test', 'asdasdasd', 'Alexandru Diordiţă, Кишинёв, Молдова', 47.0276176, 28.836705400000028, 3, 6),
(14, 4, 21, NULL, 1, 'tetra', 'Tetra', NULL, 'улица Деревянко, Харьков, Харьковская область, Украина', 50.0348159, 36.24164070000006, 4, 7);

-- --------------------------------------------------------

--
-- Table structure for table `item_comment`
--

CREATE TABLE IF NOT EXISTS `item_comment` (
  `item_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`,`comment_id`),
  KEY `comment_id` (`comment_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `item_contact`
--

CREATE TABLE IF NOT EXISTS `item_contact` (
  `item_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`,`contact_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item_contact`
--

INSERT INTO `item_contact` (`item_id`, `contact_id`) VALUES
(13, 29),
(14, 30),
(14, 31),
(14, 32),
(14, 33);

-- --------------------------------------------------------

--
-- Table structure for table `item_favorites`
--

CREATE TABLE IF NOT EXISTS `item_favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_filter_value`
--

CREATE TABLE IF NOT EXISTS `item_filter_value` (
  `item_id` int(11) NOT NULL,
  `filter_value_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`,`filter_value_id`),
  KEY `filter_value_id` (`filter_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item_filter_value`
--

INSERT INTO `item_filter_value` (`item_id`, `filter_value_id`) VALUES
(13, 24),
(13, 25),
(13, 27);

-- --------------------------------------------------------

--
-- Table structure for table `item_photo`
--

CREATE TABLE IF NOT EXISTS `item_photo` (
  `item_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  `is_main` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`,`photo_id`),
  KEY `item_id` (`item_id`),
  KEY `photo_id` (`photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item_photo`
--

INSERT INTO `item_photo` (`item_id`, `photo_id`, `is_main`) VALUES
(14, 10, 0),
(14, 11, 0),
(14, 12, 0),
(14, 13, 0),
(14, 14, 1),
(14, 15, 0);

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE IF NOT EXISTS `material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id_2` (`group_id`,`name`),
  KEY `group_id` (`group_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`id`, `group_id`, `name`, `title`) VALUES
(1, 2, 'material', 'material'),
(2, 2, 'material1', 'материал1');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`id`, `owner_id`, `title`, `file_name`) VALUES
(10, NULL, '1', '1.jpeg'),
(11, NULL, '032_NewYear2009Wallpapers_111208_Go2Load.com', '032_NewYear2009Wallpapers_111208_Go2Load.com.jpg'),
(12, NULL, 'happy-new-year-hd-desktop-widescreen-high-definition-px', 'happy-new-year-hd-desktop-widescreen-high-definition-px.jpg'),
(13, NULL, 'img1354657750', 'img1354657750.jpg'),
(14, NULL, 'New_Year_background', 'New_Year_background.jpg'),
(15, NULL, 'new-year-wallpaper-wallpapers_16415_3872x2592', 'new-year-wallpaper-wallpapers_16415_3872x2592.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `recovery_password`
--

CREATE TABLE IF NOT EXISTS `recovery_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `code` char(32) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE IF NOT EXISTS `site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `title` varchar(20) NOT NULL,
  `time_of_day` tinyint(4) NOT NULL DEFAULT '0',
  `weather_type` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `site`
--

INSERT INTO `site` (`id`, `name`, `title`, `time_of_day`, `weather_type`) VALUES
(1, 'sport', 'спорт', 0, 0),
(3, 'stroy', 'строй', 0, 0),
(4, 'otdyih', 'отдых', 0, 0),
(6, 'avto', 'авто', 0, 0),
(7, 'shoping', 'шопинг', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

CREATE TABLE IF NOT EXISTS `template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`id`, `group_id`, `title`) VALUES
(21, 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `template_material`
--

CREATE TABLE IF NOT EXISTS `template_material` (
  `template_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  PRIMARY KEY (`template_id`,`material_id`),
  KEY `item_id` (`material_id`),
  KEY `template_id` (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `top_item`
--

CREATE TABLE IF NOT EXISTS `top_item` (
  `item_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `max_view_count` int(11) NOT NULL DEFAULT '500',
  `current_view_count` int(11) NOT NULL DEFAULT '0',
  `paid_at` datetime NOT NULL,
  `expired_at` datetime DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `top_item`
--

INSERT INTO `top_item` (`item_id`, `owner_id`, `max_view_count`, `current_view_count`, `paid_at`, `expired_at`) VALUES
(13, 1, 500, 0, '2013-11-21 14:18:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `nick_name` varchar(50) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` enum('male','female','unknown') NOT NULL DEFAULT 'unknown',
  `password` char(32) NOT NULL,
  `salt` char(32) NOT NULL,
  `role` enum('user','moderator','admin') NOT NULL DEFAULT 'user',
  `created_at` datetime NOT NULL,
  `activate_key` char(32) NOT NULL,
  `activated_at` datetime DEFAULT NULL,
  `blocked_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`,`nick_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `nick_name`, `first_name`, `last_name`, `gender`, `password`, `salt`, `role`, `created_at`, `activate_key`, `activated_at`, `blocked_at`) VALUES
(1, 'chi_no@ukr.net', 'Nightmare', 'Ivan', 'Zubok', 'male', '123', '123', 'admin', '2013-11-20 00:00:00', 'asdasd asd asd asd asd', '2013-11-20 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `weather`
--

CREATE TABLE IF NOT EXISTS `weather` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temperature` tinyint(1) NOT NULL,
  `type` varchar(60) NOT NULL,
  `sunrise` datetime NOT NULL,
  `sunset` datetime NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `date` (`datetime`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `weather`
--

INSERT INTO `weather` (`id`, `temperature`, `type`, `sunrise`, `sunset`, `datetime`) VALUES
(5, 22, 'малооблачно', '2013-09-17 06:44:00', '2013-09-17 19:13:00', '2013-09-17 12:04:21'),
(6, 10, 'дождь с грозой', '2013-10-01 07:02:00', '2013-10-01 18:45:00', '2013-10-01 17:31:31'),
(7, 6, 'облачно с прояснениями', '2013-10-03 07:05:00', '2013-10-03 18:41:00', '2013-10-03 18:59:09');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`site_id`) REFERENCES `site` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `filter_group`
--
ALTER TABLE `filter_group`
  ADD CONSTRAINT `filter_group_ibfk_1` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `filter_group_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `filter_material`
--
ALTER TABLE `filter_material`
  ADD CONSTRAINT `filter_material_ibfk_1` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `filter_material_ibfk_2` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `filter_value`
--
ALTER TABLE `filter_value`
  ADD CONSTRAINT `filter_value_ibfk_1` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `group`
--
ALTER TABLE `group`
  ADD CONSTRAINT `group_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `item_ibfk_2` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `item_ibfk_1` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `item_comment`
--
ALTER TABLE `item_comment`
  ADD CONSTRAINT `item_comment_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `item_comment_ibfk_4` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `item_contact`
--
ALTER TABLE `item_contact`
  ADD CONSTRAINT `item_contact_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `item_contact_ibfk_4` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `item_favorites`
--
ALTER TABLE `item_favorites`
  ADD CONSTRAINT `item_favorites_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `item_favorites_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `item_filter_value`
--
ALTER TABLE `item_filter_value`
  ADD CONSTRAINT `item_filter_value_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `item_filter_value_ibfk_2` FOREIGN KEY (`filter_value_id`) REFERENCES `filter_value` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `item_photo`
--
ALTER TABLE `item_photo`
  ADD CONSTRAINT `item_photo_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `item_photo_ibfk_4` FOREIGN KEY (`photo_id`) REFERENCES `photo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `material`
--
ALTER TABLE `material`
  ADD CONSTRAINT `material_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `photo`
--
ALTER TABLE `photo`
  ADD CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `recovery_password`
--
ALTER TABLE `recovery_password`
  ADD CONSTRAINT `recovery_password_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `template`
--
ALTER TABLE `template`
  ADD CONSTRAINT `template_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `template_material`
--
ALTER TABLE `template_material`
  ADD CONSTRAINT `template_material_ibfk_4` FOREIGN KEY (`material_id`) REFERENCES `material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `template_material_ibfk_3` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `top_item`
--
ALTER TABLE `top_item`
  ADD CONSTRAINT `top_item_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `top_item_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
