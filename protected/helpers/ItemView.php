<?php
class ItemView
{
    CONST COOKIE_NAME = '__ivc';

    /**
     * @param Item $item
     */
    public function increaseViewCount(Item $item)
    {
        $cookies = Yii::app()->getRequest()->getCookies();

        //@TODO: add base64_encode/decode to value
        if (empty($cookies[self::COOKIE_NAME])) {
            $cookies[self::COOKIE_NAME] = $this->_prepareCookie(CJSON::encode(array($item->id)));
            $item->saveCounters(array('view_count' => 1));
        } else {
            $value = CJSON::decode(base64_decode($cookies[self::COOKIE_NAME]->value));
            if (!$value || !in_array($item->id, $value)) {
                $value[] = $item->id;
                $cookies[self::COOKIE_NAME] = $this->_prepareCookie(CJSON::encode($value));
                $item->saveCounters(array('view_count' => 1));
            }
        }
    }

    /**
     * Prepare cookie for saving
     *
     * @param $value
     * @return CHttpCookie
     */
    private function _prepareCookie($value)
    {
        $tomorrow = new DateTime('tomorrow');
        $cookie = new CHttpCookie(self::COOKIE_NAME, base64_encode($value));
        $cookie->expire = $tomorrow->getTimestamp();
        $cookie->domain = '.' . Yii::app()->params['site'];

        return $cookie;
    }
}