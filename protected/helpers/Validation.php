<?php
class Validation
{
    /**
     * @param CActiveRecord[]|CFormModel[]|CModel[] $models
     * @param bool $isReturnErrors
     * @return bool
     *
     * @throws Exception
     */
    public static function performAjaxValidation($models, $isReturnErrors = false)
    {
        if (!is_array($models)) {
            $models = array($models);
        }

        $errorsDecoded = array();
        foreach ($models as $model) {
            if (!($model instanceof CActiveRecord || $model instanceof CModel)) {
                continue;
            }

            try {
                $errors = CActiveForm::validate($model);
                $errorsDecoded = array_merge($errorsDecoded, CJSON::decode($errors));
            } catch (Exception $e) {
                throw new Exception($e->getMessage(), $e->getCode());
            }
        }

        if (empty($errorsDecoded)) {
            return true;
        }

        if (!$isReturnErrors) {
            echo CJSON::encode($errorsDecoded);
            Yii::app()->end();
        }

        return $errorsDecoded;
    }
}