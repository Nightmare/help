<?php
/** @var $this MaterialController */
/** @var $material Material */
$this->widget('widgets.ItemWidget', array(
    'site' => $this->getSite(),
    'category' => $this->getCategory(),
    'group' => $this->getGroup(),
    'material' => $this->getMaterial(),
));