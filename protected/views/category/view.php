<?php
/** @var CategoryController $this */
/** @var Category $category */
$this->widget('widgets.ItemWidget', array(
    'site' => $this->getSite(),
    'category' => $this->getCategory(),
    'group' => $this->getGroup(),
    'material' => $this->getMaterial(),
));