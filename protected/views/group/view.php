<?php
/** @var $this GroupController */
/** @var $group Group */
$this->widget('widgets.ItemWidget', array(
    'site' => $this->getSite(),
    'category' => $this->getCategory(),
    'group' => $this->getGroup(),
    'material' => $this->getMaterial(),
));