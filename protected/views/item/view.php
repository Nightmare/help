<section class="item-view">
    <?php
    /** @var $this ItemController */
    /** @var Item $item */
    /** @var CClientScript $clientScript */
    $_gg = json_encode(array(
        'lat' => $item['lat'],
        'lng' => $item['lng'],
        'address' => $item['address'],
        'marker' => $item->getThumbMarker(),
    ));
    $clientScript = Yii::app()->getClientScript();
    $clientScript
        ->registerPackage('swipe')
        ->registerPackage('fancybox')
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/map.js', CClientScript::POS_END)
        ->registerScript('GG_MAPS', "var _GG = $_gg;", CClientScript::POS_HEAD);

    $contacts = $item->contacts;
    $photos = $item->photos;
    ?>
    <div class="thumbnail">
        <?php echo CHtml::image($photos ? $photos[0]->getThumbItemSize() : Photo::getNoImage() , $photos ? $photos[0]->title : 'No Image') ?>
    </div>
    <div class="holder">
        <h1><?php echo CHtml::encode($item->title); ?><span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab blanditiis culpa eius inventore!</span>
        </h1>

        <div class="action-box">
            <ul>
                <li class="back"><?php echo CHtml::link('Вернуться', Yii::app()->request->urlReferrer); ?></li>
                <li class="print"><a href="#"></a></li>
                <li class="article"><a href="#"></a></li>
                <li class="comment"><a href="#"></a></li>
            </ul>
        </div>
        <div class="rating-box">
            <label><strong>Рейтинг:</strong></label>
            <ul>
                <?php echo str_repeat('<li></li>', $item->rate) ?>
                <?php echo str_repeat('<li class="empty"></li>', Item::MAX_RATE - $item->rate) ?>
            </ul>
            <div class="skills">[<?php echo $item->rate; ?>]</div>
        </div>
    </div>
    <p class="text">
        <?php echo CHtml::encode($item->description); ?>
    </p>

    <div class="box-action">
        <h1><?php echo CHtml::encode($item->title); ?></h1>
    </div>

    <?php if ($photos) : ?>
    <div class="item-carousel">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($photos as $photo) : ?>
                    <a class="swiper-slide" href="<?php echo $photo->getThumbLarge() ?>" data-fancybox-group="item-gallery">
                        <?php echo CHtml::image($photo->getThumbItemSize(), $photo->title) ?>
                    </a>
                <?php endforeach ?>
            </div>
        </div>

        <a href="#" class="arrow prev"><span class="triangle"></span></a>
        <a href="#" class="arrow next"><span class="triangle"></span></a>
    </div>
    <?php endif; ?>

    <section class="item-info">
        <address>
            <div class="address"><?php echo CHtml::encode($item->address); ?></div>
            <?php
            if ($contacts) :
                foreach ($contacts as $contact) : ?>
                    <p>
                        <strong><?php echo ucfirst(Yii::t('contact', $contact->type)); ?>:</strong>
                        <?php echo $contact->value; ?>
                    </p>
                <?php
                endforeach;
            endif;
            ?>
        </address>
    </section>

    <div id="gg-map"></div>
</section>