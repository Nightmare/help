<?php
return CMap::mergeArray(
include __DIR__ . '/main.php',
array(
    'preload' => array(
//        'log'
        'debug',
    ),

    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => false,
            'ipFilters' => array('127.0.0.1', '::1'),
            'generatorPaths' => array(
                'ext.giix-core',
            ),
        ),
        'user',
        'search',
    ),

    'components' => array(
        'debug' => array(
            'class' => 'ext.yii2-debug.Yii2Debug',
        ),

        'cache' => array(
            'class' => 'CFileCache',
        ),

        'db' => array(
            'connectionString' => 'mysql:host=127.0.0.1;dbname=help',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '123',
            'charset' => 'utf8',
            'schemaCachingDuration' => 86400,
            'enableParamLogging' => true,
            'enableProfiling' => true,

        ),
    ),

    'params' => array(
        'adminEmail' => 'webmaster@example.com',
        'site' => 'help.lan',
    ),
)
);