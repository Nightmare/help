<?php
return array(
    'basePath' => __DIR__ . DIRECTORY_SEPARATOR . '..',
    'name' => 'My Console Application',

    'import' => array(
        'application.models.*',
        'ext.giix-components.*',
    ),

    'preload' => array('log'),

    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=help',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '123',
            'charset' => 'utf8',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
);