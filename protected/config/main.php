<?php
return array(
    'aliases' => array(
        'widgets' => 'application.widgets',
    ),
    'basePath' => dirname(__DIR__),
    'name' => 'Help-md.tk',
    'timeZone' => 'Europe/Chisinau',

    'import' => array(
        'application.models.*',
        'application.components.Controller',
        'ext.giix-components.*',
        'application.widgets.Widget',
        'application.widgets.FlashMessages',
        'application.modules.search.models.*',
        'application.modules.user.forms.*',
        'application.modules.user.models.*',
        'application.helpers.Validation',
        'application.components.TabView',
    ),

    'theme' => 'main-less',

    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => false,
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
        'admin',
        'search',
    ),

    'components' => array(
        'request' => array(
            'enableCookieValidation' => true,
        ),
        'ajax' => array(
            'class' => 'application.components.AsynchronousResponse',
        ),
        'user' => array(
            'allowAutoLogin' => true,
            'autoRenewCookie' => true,
            'class' => 'application.modules.user.components.WebUser',
            'returnUrl' => array('/site/index'),
            'loginUrl' => array('/user/account/login/'),
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
//                '<_c:\w+>/<id:\d+>' => '<_c>/view',
                '<_c:\w+>/<_a:\w+>/<id:\d+>' => '<_c>/<_a>',
//                '<_c:\w+>/<_a:\w+>' => '<_c>/<_a>',

                '/login' => 'user/account/login',
                '/logout' => 'user/account/logout',
                '/registration' => 'user/account/registration',
                '/recovery' => 'user/account/recovery',
//                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                'uslugi' => array('service/list'),

                array(
                    'class' => 'application.rules.SeoRule',
                    'connectionID' => 'db',
                ),
            ),
        ),

        'clientScript' => array(
            'scriptMap' => array(
                'clock.js' => '/js/clock.js',
                'yiiTabLive' => '/js/jquery.yiiTabLive.js',
            ),
            'packages' => array(
                'jquery'=>array(
                    'baseUrl' => '/',
                    'js' => array('/code.jquery.com/jquery-1.8.2.min.js') ,
                ),
                'swipe' => array(
                    'baseUrl' => '/themes/main-less/',
                    'css' => array('css/idangerous.swiper.css'),
                    'js' => array('js/idangerous.swiper.js'),
                    'depends' => array('jquery'),
                ),
                'fancybox' => array(
                    'baseUrl' => '/themes/main-less/',
                    'css' => array('css/jquery.fancybox.css', 'css/jquery.fancybox-thumbs.css'),
                    'js' => array('js/jquery.fancybox.js', 'js/jquery.fancybox-thumbs.js'),
                    'depends' => array('jquery'),
                ),
            ),
        ),

        'cache' => array(
            'class' => 'CDummyCache',
        ),
        'db' => array(
            'connectionString' => 'mysql:host=127.0.0.1;dbname=help',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'counter16warik3',
            'charset' => 'utf8',
            'schemaCachingDuration' => 86400,
        ),
        'errorHandler' => array(
            'errorAction' => 'error/error',
        ),
    ),

    'params' => array(
        'adminEmail' => 'webmaster@example.com',
        'site' => 'help-md.cf',
    ),
);