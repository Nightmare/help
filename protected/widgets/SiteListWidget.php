<?php
class SiteListWidget extends Widget
{
    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->order = '`name` = \'stroy\' DESC, `name` = \'avto\' DESC, `name` = \'sport\' DESC, `name` = \'otdyih\' DESC, `name` = \'shoping\' DESC';
        $this->render('sites', array('sites' => Site::model()->cache(self::SECONDS_IN_DAY)->findAll($criteria)));
    }
}