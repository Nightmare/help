<?php
Yii::import('application.modules.search.forms.FilterForm');
Yii::import('application.helpers.YText');
class ItemFilter extends CWidget
{
    /** @var Group|Material */
    public $model = null;

    public function init()
    {
        /*if (!($this->model instanceof Group)
            || !($this->model instanceof Material)
        ) {
            Yii::log('Error', 'Model must be Group or Material');
            return;
        }*/
    }

    public function run()
    {
        $filterForm = new FilterForm();
        if ($filterFormQuery = Yii::app()->getRequest()->getQuery('FilterForm')) {
            $filterForm->setAttributes($filterFormQuery);
        }
        $this->render('item', array(
            'typeModel' => $this->model,
            'model' => $filterForm,
            'districtModels' => District::model()->findAll(),
        ));
    }
}
