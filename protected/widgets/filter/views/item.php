<?php
/** @var ItemFilter $this */
/** @var FilterForm $model */
/** @var Material|Group $typeModel */
/** @var District[] $districtModels */
/** @var CActiveForm $form */
/** @var CActiveForm $form */
?>
<section class="side-box">
    <div class="wrapper">
        <?php echo CHtml::beginForm('/search', 'get'); ?>
            <div class="filter-selectbox">
                <?php
                    echo Chtml::dropDownList(YText::translit('район'), false, CHtml::listData($districtModels, 'name', 'title'), array('empty' => 'Выбирете район')),
                        Chtml::dropDownList(YText::translit('рейтинг'), false, range(0, 5), array('empty' => 'Выбирете рейтинг')),
                        Chtml::dropDownList(YText::translit('сортировка'), false,  FilterForm::getSortStates(), array('empty' => 'Выбирете тип'));
                ?>
                <div class="clear"></div>
                <?php if ($typeModel instanceof Material || $typeModel instanceof Group) :
                    $type = get_class($typeModel);
                    /** @var FilterGroup|FilterMaterial $typeFilter */
                    foreach ($typeModel->{sprintf('filter%ss', $type)} as $typeFilter) :
                        $this->widget(
                            'widgets.control.' . ucfirst($typeFilter->filter->type),
                            array('model' => $typeFilter->filter)
                        );
                    ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <div class="clear"></div>
            </div>
            <div class="buttons-box">
                <?php
                    echo Chtml::submitButton('Искать', array('name' => null)),
                        Chtml::resetButton('Сбросить фильтры', array('name' => null));
                ?>
                <p>Воспользуйтесь фильтрами для быстрого поиска интересующего Вас заведения</p>
            </div>
            <div class="clear"></div>
        <?php echo CHtml::endForm(); ?>
    </div>
</section>