<?php
class ClockWidget extends Widget
{
    public function init()
    {
        $this->_addClientScript();
        parent::init();
    }

    private function _addClientScript()
    {
        /** @var CClientScript $cs */
        $cs = Yii::app()->clientScript;
        $cs->registerScriptFile(Yii::app()->baseUrl . '/js/clock.js', CClientScript::POS_END);
    }

    public function run()
    {
        $this->render('clock', array());
    }
}