<?php

abstract class Widget extends CWidget
{
    const SECONDS_IN_DAY = 86400;

    public function getViewPath($checkTheme = false)
    {
        return sprintf(
            '%1$s%2$s%3$s%2$sviews%2$swidgets',
            Yii::app()->themeManager->basePath,
            DIRECTORY_SEPARATOR,
            Yii::app()->theme->name
        );
    }
}