<?php
class Profile extends CWidget
{
    public function run()
    {
        $this->render('profile', array('model' => Yii::app()->user->getProfile()));
    }
}