<a href="" class="header-auth">Авторизоваться</a>
<div class="profile-info">
    <?php
    $this->widget('TabView', array(
        'tabs' => array(
            'login-tab' => array(
                'title' => 'Login',
                'view' => '//user/account/login',
                'data' => array('model' => new LoginForm() /*, 'message' => $message*/),
            ),
            'register-tab' => array(
                'title' => 'Register',
                'view' => '//user/account/registration',
                'data' => array('model' => new RegistrationForm()),
            ),
            'recovery-tab' => array(
                'title' => 'Recovery password',
                'view' => '//user/account/recovery',
                'data' => array('model' => new RecoveryForm()),
            ),
        ),
//        'cssFile' => Yii::app()->theme->baseUrl . '/css/yiitab-auth.css'
    ));
    ?>
</div>