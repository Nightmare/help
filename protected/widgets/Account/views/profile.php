<a href="" class="header-auth">Привет, <?php echo $model->nick_name ?></a>
<div class="profile-info">
    <div>
        <a href="<?php echo Yii::app()->createUrl('/item/favorites'); ?>">Избранное (<span id="favorites_count"><?php echo count($model->itemFavorites); ?></span>)</a>
    </div>
    <div>
        <?php echo CHtml::link(sprintf('Сообщения (%d)', count($model->itemFavorites)), array('/profile')); ?>
    </div>
    <div>
        <?php echo CHtml::link('Личный кабинет', array('/profile')); ?>
    </div>
    <div>
        <?php echo CHtml::link('Выйти', array('/logout')); ?>
    </div>
</div>