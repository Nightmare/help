<?php
class GroupWidget extends Widget
{
    public $category;
    public $group;
    public $material;
    public $item;
    public $site;

    public function run()
    {
        $this->render('group', array(
            'menuParams' => $this->_getMenuParams(),
        ));
    }

    /**
     * @return array
     */
    protected function _getMenuParams()
    {
        $isCategoryExist = $this->category instanceof Category;

        $criteria = new CDbCriteria();
        $criteria->alias = 'g';
        $criteria->group = 'g.id, m.id, iM.id, iG.id';
        $criteria->order = 'COUNT(iG.id) DESC, COUNT(iM.id) DESC';
        $criteria->with = array(
            'materials' => array(
                'alias' => 'm',
                'together' => true,
                'with' => array(
                    'templates' => array(
                        'alias' => 'tplM',
                        'select' => false,
                        'with' => array(
                            'items' => array(
                                'alias' => 'iM',
                                'select' => false,
                            ),
                        ),
                    ),
                ),
            ),
            'templates' => array(
                'alias' => 'tplG',
                'select' => false,
                'with' => array(
                    'items' => array(
                        'alias' => 'iG',
                        'select' => false,
                    ),
                ),
            ),
        );

        if ($isCategoryExist) {
            $criteria->compare('g.category_id', $this->category->id);
        } else {
            if ($this->site instanceof Site) {
                $criteria->compare('c.site_id', $this->site->id);
            }

            $criteria->with['category'] = array(
                'alias' => 'c',
                'together' => true,
                'joinType' => 'INNER JOIN',
            );
        }

        /** @var Group[] $groups */
        if (!($groups = Group::model()->findAll($criteria))) {
            return array();
        }
        $menu = array();

        $activeGroupId = $this->group instanceof Group ? $this->group->id : null;
        $activeMaterialId = $this->material instanceof Material ? $this->material->id : null;

        foreach ($groups as $group) {
            $categoryName = $isCategoryExist ? $this->category->name : $group->category->name;
            $groupName = $group->name;

            $menu[] = array(
                'label' => $group->title,
                'url' => array(
                    '/group/view',
                    'category' => $categoryName,
                    'group' => $groupName,
                ),
                'linkOptions' => array('title' => $group->title),
                'active' => $activeGroupId == $group->id,
                'items' => $this->_addMaterialsMenuParams($group->materials, $activeMaterialId, $categoryName, $groupName),
            );
        }

        return $menu;
    }

    /**
     * @param Material[] $materials
     * @param int|null $activeMaterialId
     * @param string $categoryName
     * @param string $groupName
     * @return array
     */
    private function _addMaterialsMenuParams($materials, $activeMaterialId, $categoryName, $groupName)
    {
        $menu = array();
        if (!$materials) {
            return $menu;
        }
        foreach ($materials as $material) {
            $menu[] = array(
                'label' => $material->title,
                'url' => array(
                    '/material/view',
                    'category' => $categoryName,
                    'group' => $groupName,
                    'material' => $material->name,
                ),
                'linkOptions' => array('title' => $material->title),
                'active' => $activeMaterialId == $material->id,
            );
        }

        return $menu;
    }
}