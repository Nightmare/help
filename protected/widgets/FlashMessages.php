<?php
class FlashMessages extends CWidget
{
    const NOTICE_MESSAGE = 'notice';
    const WARNING_MESSAGE = 'warning';
    const ERROR_MESSAGE = 'error';

    const RECOVERY_MESSAGE = 'recovery';


    public $error = 'error';
    public $warning = 'warning';
    public $notice = 'notice';

    public $autoHide = false;
    public $autoHideSeconds = 2500;
    public $divId = 'flash';
    public $customJsCode;

    public function run()
    {
        if (count(Yii::app()->user->getFlashes(false))) {
            /**
             * @var $clientScript CClientScript
             */
            $clientScript = Yii::app()->clientScript;
            $clientScript->registerCoreScript('jquery.js');

            if ($this->autoHide) {
                $this->autoHideSeconds = (int)$this->autoHideSeconds;
                $this->error = CHtml::encode($this->error);
                $this->warning = CHtml::encode($this->warning);
                $this->notice = CHtml::encode($this->notice);

                $js = "$('#{$this->divId}').delay(5000).fadeOut({$this->autoHideSeconds});";
                $clientScript->registerScript(md5($this->id), $js, CClientScript::POS_END);
            } elseif ($this->customJsCode) {
                $clientScript->registerScript(md5($this->customJsCode), $this->customJsCode, CClientScript::POS_END);
            }

            $this->render('flashmessages');
        }
    }
}