<?php
class ItemWidget extends Widget
{
    /** @var Category */
    public $category;
    /** @var Group */
    public $group;
    /** @var Material */
    public $material;
    /** @var Item */
    public $item;
    /** @var Site */
    public $site;

    public function run()
    {
        $itemCriteria = new CDbCriteria();
        $itemCriteria->alias = 'i';
        $itemCriteria->scopes = array('orderByVies', 'orderByRate');
        $itemCriteria->with = array(
            'district' => array(
                'joinType' => 'INNER JOIN',
                'alias' => 'd',
                'together' => true,
            ),
            'topItem' => array(
                'alias' => 'tig',
                'together' => true,
                'on' => ':now > tig.expired_at OR tig.max_view_count = :infinity OR tig.max_view_count > tig.current_view_count',
            ),
            'itemFavorites' => array(
                'alias' => 'if',
//                    'scopes' => 'onlyFavorite', //todo: uncomment after CWebUsers realization
                'together' => true,
            ),
            'itemPhotos' => array(
                'alias' => 'ip',
                'scopes' => 'onlyMain',
                'together' => true,
                'with' => array(
                    'photo' => array(
                        'alias' => 'p',
                        'together' => true,
                    )
                )
            ),
        );

        $itemCriteria->params['now'] = date('Y-m-d');
        $itemCriteria->params['infinity'] = TopItem::SHOW_INFINITE;
        $itemCriteria->order = 'tig.item_id DESC';

        $addCriteria = new CDbCriteria();
        if ($this->material instanceof Material) {
            $addCriteria->with = array(
                'materials' => array(
                    'alias' => 'm',
                ),
            );

            $addCriteria->compare('m.id', $this->material->id);
        } elseif ($this->group instanceof Group) {
            $addCriteria->with = array(
                'group' => array(
                    'alias' => 'g',
                ),
                'materials' => array(
                    'alias' => 'm',
                    'with' => array(
                        'group' => array(
                            'alias' => 'mg',
                        ),
                    ),
                ),
            );

            $addCriteria
                ->compare('g.id', $this->group->id)
                ->compare('mg.id', $this->group->id, false, 'OR');
        } elseif ($this->category instanceof Category) {
            $addCriteria->with = array(
                'group' => array(
                    'alias' => 'g',
                    'with' => array(
                        'category' => array(
                            'alias' => 'c',
                        ),
                    ),
                ),
                'materials' => array(
                    'alias' => 'm',
                    'with' => array(
                        'group' => array(
                            'alias' => 'mg',
                            'with' => array(
                                'category' => array(
                                    'alias' => 'gc',
                                ),
                            ),
                        ),
                    ),
                ),
            );

            $addCriteria
                ->compare('c.id', $this->category->id)
                ->compare('gc.id', $this->category->id, false, 'OR');
        }

        $itemCriteria->mergeWith($addCriteria);
        $this->render('item', array('items' => Item::model()->findAll($itemCriteria)));
    }
}