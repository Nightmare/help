<?php
class CategoryWidget extends Widget
{
    public $site;
    public $category;

    public function run()
    {
        $this->render('category', array(
            'menuParams' => $this->_getMenuParams(),
        ));
    }

    protected function _getMenuParams()
    {
        $criteria = new CDbCriteria();
        $criteria->alias = 'c';
        $activeCategoryId = null;

        if (!$this->site instanceof Site) {
            $this->site = Site::getCurrentSite();
        }

        $criteria->compare('c.site_id', $this->site->id);

        if ($this->category instanceof Category) {
            $activeCategoryId = $this->category->id;
        }

        /** @var Category[] $categories */
        $categories = Category::model()->findAll($criteria);
        $menu = array();

        if (!$categories) {
            return $menu;
        }

        foreach ($categories as $category) {
            $menu[] = array(
                'label' => $category->title,
                'url' => array(
                    '/category/view',
                    'category' => $category->name,
                ),
                'linkOptions' => array('title' => $category->title),
                'active' => $activeCategoryId == $category->id,
            );
        }

        return $menu;
    }
}