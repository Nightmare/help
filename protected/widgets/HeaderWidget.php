<?php
Yii::import('application.helpers.YText');
/**
 * Class HeaderWidget
 * @property Controller $controller
 */
class HeaderWidget extends Widget
{
    public function run()
    {
        $this->render('header', array(
            'weather' => $this->_getWeather(),
            'site' => $this->controller->getSite(),
        ));
    }

    /**
     * @return Weather
     */
    protected function _getWeather()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'datetime DESC';

        return  Weather::model()->cache(7000)->find($criteria);
    }
} 