<?php
/** @var $model Filter */
?>
<div class="filter-form-row">
    <label><?php echo $model->title; ?></label>
    <div class="filter-checkbox-list">
        <?php
        /** @var FilterValue $value */
        foreach ($model->filterValues as $value) :
            ?>
            <div>
                <label>
                    <input type="checkbox" name="<?php echo YText::translit('дополнительно'); ?>[<?php echo $model->name; ?>][]" value="<?php echo $value->name; ?>" />
                    <?php echo $value->title; ?>
                </label>
            </div>
        <?php endforeach;?>
    </div>
</div>