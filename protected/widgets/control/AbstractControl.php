<?php
abstract class AbstractControl extends CWidget
{
    /** @var Filter */
    public $model;
    private $_params = array();

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    protected function _addParams($key, $value)
    {
        $this->_params[$key] = $value;
        return $this;
    }

    public function init()
    {
        parent::init();
        if (!$this->model instanceof Filter) {
            throw new CException('"model" parameter must be instance of Filter');
        }
    }

    public function run()
    {
        parent::run();
        $this->_addParams('model', $this->model);
        $this->render(strtolower(get_class($this)), $this->_params);
    }
}