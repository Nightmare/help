<?php
class HomeMapMenuWidget extends Widget
{
    protected $_sitesMapper = array(
        'stroy' => array(
            'coords' => array(54, 111, 9, 224, 187, 224, 233, 111),
        ),
        'avto' => array(
            'coords' => array(244, 111, 198, 224, 377, 224, 422, 111),
        ),
        'sport' => array(
            'coords' => array(434, 111, 389, 224, 567, 224, 613, 111),
        ),
        'otdyih' => array(
            'coords' => array(625, 111, 580, 224, 758, 224, 804, 111),
        ),
        'shoping' => array(
            'coords' => array(814, 111, 770, 224, 947, 224, 993, 111),
        ),
    );

    public function run()
    {
        $this->render('home-map-menu', array(
            'mapper' => $this->_sitesMapper,
        ));
    }
} 