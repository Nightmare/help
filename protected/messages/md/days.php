<?php
return array(
    'понедельник' => 'luni',
    'вторник' => 'marti',
    'среда' => 'miercuri',
    'четверг' => 'joi',
    'пятница' => 'vineri',
    'суббота' => 'simbata',
    'воскресение' => 'duminica',
);