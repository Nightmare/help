<?php
class GroupController extends Controller
{
    public function actionView($category, $group)
    {
        $criteria = new CDbCriteria();
        $criteria->alias = 'g';
        $criteria
            ->compare('g.name', $group)
            ->compare('c.name', $category);

        $criteria->with = array(
            'category' => array(
                'alias' => 'c',
                'together' => true,
                'joinType' => 'INNER JOIN',
            ),
        );

        $this->_group = Group::model()->find($criteria);
        $this->_category = $this->_group->category;

        $this->contentWidgets['widgets.filter.ItemFilter'] = array('model' => $this->_group);
        $this->widgets['widgets.GroupWidget'] = array(
            'category' => $this->_category,
            'group' => $this->_group,
        );

        $this->render('view', array('group' => $this->_group));
    }
}