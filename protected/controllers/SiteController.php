<?php
class SiteController extends Controller
{
    public function actionIndex()
    {
        if (!$this->getSite() instanceof Site) {
            $this->layout = '//layouts/home';
            return $this->render('home');
        }

        /** @var Category[] $categories */
        if ($categories = $this->getSite()->categories) {
            $this->redirect(array('category/view', 'category' => $categories[0]->name));
        }

        return $this->render('index');
    }
} 