<?php
class CategoryController extends Controller
{
    public function actionView($category)
    {
        $this->_category = Category::model()->findByAttributes(array('name' => $category));
        $this->widgets['widgets.GroupWidget'] = array(
            'category' => $this->_category,
        );
        $this->contentWidgets['widgets.filter.ItemFilter'] = array();

        $this->render('view', array('category' => $this->_category));
    }
}

