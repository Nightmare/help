<?php
class ErrorController extends Controller
{
    public function actionError()
    {
        if ($error = Yii::app()->getErrorHandler()->getError()) {
            if (Yii::app()->getRequest()->getIsAjaxRequest()) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }
} 