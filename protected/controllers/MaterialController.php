<?php
class MaterialController extends Controller
{
    public function actionView($category, $group, $material)
    {
        $criteria = new CDbCriteria();
        $criteria->alias = 'm';
        $criteria
            ->compare('m.name', $material)
            ->compare('g.name', $group)
            ->compare('c.name', $category);

        $criteria->with = array(
            'group' => array(
                'alias' => 'g',
                'together' => true,
                'joinType' => 'INNER JOIN',
                'with' => array(
                    'category' => array(
                        'alias' => 'c',
                        'together' => true,
                        'joinType' => 'INNER JOIN',
                    ),
                ),
            ),
        );

        $this->_material = Material::model()->find($criteria);
        $this->_group = $this->_material->group;
        $this->_category = $this->_group->category;

        $this->contentWidgets['widgets.filter.ItemFilter'] = array('model' => $this->_material);
        $this->widgets['widgets.GroupWidget'] = array(
            'category' => $this->_category,
            'group' => $this->_group,
            'material' => $this->_material,
        );

        $this->render('view', array('material' => $this->_material));
    }
}