<?php

class ItemController extends Controller
{
    public function actionView($category, $group, $material, $item)
    {
        /** @var ItemView $itemView */
        $itemView = $this->getHelper('itemView');
        $isMaterialExist = $material !== '';

        $criteria = new CDbCriteria();
        $criteria->alias = 'i';

        $criteria
            ->compare('g.name', $group)
            ->compare('c.name', $category)
            ->compare('i.name', $item);

        $groupTreeRelations = array(
            'group' => array(
                'alias' => 'g',
                'with' => array(
                    'category' => array(
                        'alias' => 'c',
                    ),
                ),
            ),
        );

        if ($isMaterialExist) {
            $criteria->with = array(
                'materials' => array(
                    'alias' => 'm',
                    'with' => $groupTreeRelations,
                ),
            );

            $criteria->compare('m.name', $material);
        } else {
            $criteria->with = $groupTreeRelations;
        }

        $this->_item = Item::model()->find($criteria);

        if ($isMaterialExist) {
            $this->_material = $this->_item->materials[0];
            $this->_group = $this->_material->group;
        } else {
            $this->_group = $this->_item->group;
        }

        $this->_category = $this->_group->category;
        $this->widgets['widgets.GroupWidget'] = array(
            'category' => $this->_category,
            'group' => $this->_group,
            'material' => $this->_material,
            'item' => $this->_item,
        );

        $itemView->increaseViewCount($this->_item);
        $this->render('view', array('item' => $this->_item));
    }

    /**
     * @param int $id
     */
    public function actionToggleFavorites($id)
    {
        $id = (int)$id;
        $type = 'plus';

        $itemFavorites = ItemFavorites::model()->findByAttributes(array(
            'user_id' => Yii::app()->user->id,
            'item_id' => $id,
        ));

        if (empty($itemFavorites)) {
            $itemFavorites = new ItemFavorites();
            $itemFavorites->item_id = $id;
            $itemFavorites->user_id = Yii::app()->user->id;
            $itemFavorites->save();
        } else {
            $itemFavorites->delete();
            $type = 'minus';
        }

        Yii::app()->ajax->success(array('type' => $type));
    }
} 