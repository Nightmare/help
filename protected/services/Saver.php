<?php
class SaverService
{
    /**
     * @Inject
     * @var Saver
     */
    protected $_saver;

    public function __construct()
    {
        $this->_saver = new Saver();
    }

    public function save(Item $item)
    {
        $userId = Yii::app()->user->id;
        if (!$this->_saver->findByAttributes(array('user_id' => $userId, 'item_id' => $item->id))) {
            $this->_saver->setAttributes(array(
                'user_id' => $userId,
                'item_id' => $item->id,
                'created_at' => new CDbExpression('NOW()'),
            ));
        }
    }

    /**
     * @param int $count
     * @return array|CActiveRecord[]
     */
    public function getLastViewed($count = 20)
    {
        if (($count = (int)$count) <= 0) {
            return array();
        }

        $criteria = new CDbCriteria();
        $criteria->distinct = true;
        $criteria->limit = $count;
        $criteria->order = 'created_at DESC';
        $criteria->compare('user_id', Yii::app()->user->id);

        return $this->_saver->findAll($criteria);
    }
}