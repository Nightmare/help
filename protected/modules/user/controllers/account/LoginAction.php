<?php
class LoginAction extends CAction
{
    public function run()
    {
        $form = new LoginForm();
        $post = Yii::app()->request->getPost('LoginForm');

        if (!empty($post)) {
            $form->setAttributes($post);

            /** @var UserModule $module */
            $module = Yii::app()->getModule('user');
            if (Yii::app()->getRequest()->getIsAjaxRequest()) {
                if (Validation::performAjaxValidation($form)) {
                    $this->_successFlashMessage();

                    Yii::app()->ajax->raw(array(
                        'success' => true,
                        'url' => $module->loginSuccess,
                    ));
                }
            } else {
                if ($form->validate()) {
                    $this->_successFlashMessage();

                    $redirect = $module->loginSuccess;

                    $this->controller->redirect(Yii::app()->user->getReturnUrl($redirect));
                    $this->controller->redirect('/admin/dashboard/index');
                }
            }
        }

        $this->controller->render('login', array('model' => $form));
    }

    private function _successFlashMessage()
    {
        Yii::app()->user->setFlash(FlashMessages::NOTICE_MESSAGE, Yii::t('UserModule.user', 'Вы успешно авторизовались!'));
    }
}