<?php
class ActivateAction extends CAction
{
    public function run($key)
    {
        /** @var $user User */
        $user = User::model()->notActivated()->findByAttributes(array('activate_key' => $key));
        // процедура активации
//        $module = Yii::app()->getModule('user');
        if (!$user instanceof User) {
            Yii::app()->user->setFlash(
                FlashMessages::ERROR_MESSAGE,
                Yii::t('UserModule.user', 'Ошибка активации! Возможно данный аккаунт уже активирован! Попробуете зарегистрироваться вновь?')
            );
//            $this->controller->redirect(array($module->accountActivationFailure));
            $this->controller->redirect(array('/site/index'));
        }

        if ($user->activate()) {
//            // отправить сообщение о активации аккаунта
//            $emailBody = $this->controller->renderPartial('accountActivatedEmail', array('model' => $user), true);
//            Yii::app()->mail->send($module->notifyEmailFrom, $user->email, Yii::t('UserModule.user', 'Аккаунт активирован!'), $emailBody);
            Yii::app()->user->setFlash(
                FlashMessages::NOTICE_MESSAGE,
                Yii::t('UserModule.user', 'Вы успешно активировали аккаунт! Теперь Вы можете войти!')
            );
//            $this->controller->redirect(array($module->accountActivationSuccess));
        } else {
            Yii::app()->user->setFlash(
                FlashMessages::ERROR_MESSAGE,
                Yii::t('UserModule.user', 'При активации аккаунта произошла ошибка! Попробуйте позже!')
            );
//            $this->controller->redirect(array($module->accountActivationFailure));
        }

        $this->controller->redirect(array('/site/index'));
    }
}