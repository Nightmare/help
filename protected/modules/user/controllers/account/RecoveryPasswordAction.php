<?php
class RecoveryPasswordAction extends CAction
{
    public function run($code)
    {
        if (Yii::app()->user->isAuthenticated()) {
            $this->controller->redirect(Yii::app()->user->returnUrl);
        }

        $RecoveryModel = RecoveryPassword::model()->with('user:active')->find('code = :code', array(':code' => $code));

        if (empty($RecoveryModel)) {
            Yii::log(
                Yii::t('UserModule.user', 'Код восстановления пароля {code} не найден!', array('{code}' => $code)),
                CLogger::LEVEL_ERROR, UserModule::$logCategory
            );
            Yii::app()->user->setFlash(
                FlashMessages::ERROR_MESSAGE,
                Yii::t('UserModule.user', 'Код восстановления пароля не найден! Попробуйте еще раз!')
            );

//            $this->controller->redirect(array('/user/account/recovery'));
            $this->controller->redirect(array('/site/index'));
        }

        $ChangePasswordForm = new ChangePasswordForm();
        $ChangePasswordPost = Yii::app()->request->getPost('ChangePasswordForm');

        if (!empty($ChangePasswordPost)) {
            $ChangePasswordForm->setAttributes($ChangePasswordPost);

            if (Yii::app()->request->isAjaxRequest) {
                $this->_ajaxValidation($ChangePasswordForm);
            } else {
                if ($ChangePasswordForm->validate()) {
                    $this->_removeAllRequests($ChangePasswordForm, $RecoveryModel);
                }
            }
        }

        $this->controller->render('changePassword', array('model' => $ChangePasswordForm));
    }

    private function _ajaxValidation(ChangePasswordForm $ChangePasswordForm)
    {
        $status = Validation::performAjaxValidation($ChangePasswordForm);

        if (!!$status) {

//            echo CJSON::encode($message);
            Yii::app()->end();
        }
    }

    /**
     * @param ChangePasswordForm $ChangePasswordForm
     * @param RecoveryPassword $RecoveryModel
     */
    private function _removeAllRequests(ChangePasswordForm $ChangePasswordForm, RecoveryPassword $RecoveryModel)
    {
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $RecoveryModel->user->password = User::model()->hashPassword($ChangePasswordForm->password, $RecoveryModel->user->salt);

            if ($RecoveryModel->user->save()) {
                if (RecoveryPassword::model()->deleteAll('user_id = :user_id', array(':user_id' => $RecoveryModel->user->id))) {
                    $transaction->commit();

                    Yii::app()->user->setFlash(
                        FlashMessages::NOTICE_MESSAGE,
                        Yii::t('UserModule.user', 'Пароль изменен!')
                    );
                    Yii::log(
                        Yii::t('UserModule.user', 'Успешная смена пароля для пользоателя {user}!', array('{user}' => $RecoveryModel->user->id)),
                        CLogger::LEVEL_INFO, UserModule::$logCategory
                    );

//                            $emailBody = $this->controller->renderPartial('passwordRecoverySuccessEmail', array('model' => $RecoveryModel->user), true);
//                            Yii::app()->mail->send(
//                                $module->notifyEmailFrom,
//                                $RecoveryModel->user->email,
//                                Yii::t('UserModule.user', 'Успешное восстановление пароля!'),
//                                $emailBody
//                            );
//                            $this->controller->redirect(array('/user/account/login'));
                    $this->controller->redirect(array('/site/index'));
                }
            }
        } catch (CDbException $e) {
            $transaction->rollback();

            Yii::app()->user->setFlash(
                FlashMessages::ERROR_MESSAGE,
                Yii::t('UserModule.user', 'Ошибка при смене пароля!')
            );
            Yii::log(
                Yii::t('Ошибка при смене пароля {error}!', array('{error}' => $e->getMessage())),
                CLogger::LEVEL_ERROR, UserModule::$logCategory
            );
//                    $this->controller->redirect(array('/user/account/recovery'));
            $this->controller->redirect(array('/site/index'));
        }
    }
}