<?php
class LogOutAction extends CAction
{
    public function run()
    {
        Yii::app()->user->logout();
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            Yii::app()->ajax->success(array(
                'success' => true,
            ));
        } else {
            $this->controller->redirect(array(/*Yii::app()->getModule('user')->logoutSuccess*/'/site/index'));
        }
    }
}