<?php
class RecoveryAction extends CAction
{
    public function run()
    {
        if (Yii::app()->user->isAuthenticated()) {
            $this->controller->redirect(Yii::app()->user->returnUrl);
        }

        $RecoveryForm = new RecoveryForm();
        $RecoveryPost = Yii::app()->request->getPost('RecoveryForm');

        if (!empty($RecoveryPost)) {
            $RecoveryForm->setAttributes($RecoveryPost);

            if (Yii::app()->request->isAjaxRequest) {
                $this->_ajaxValidation($RecoveryForm);
            } else {
                if ($RecoveryForm->validate()) {
                    $this->_validateRecoveryPassword($RecoveryForm);
                }
            }
        }
        $this->controller->render('recovery', array('model' => $RecoveryForm));
    }

    /**
     * @param RecoveryForm $RecoveryForm
     */
    private function _ajaxValidation(RecoveryForm $RecoveryForm)
    {
        $status = Validation::performAjaxValidation($RecoveryForm);

        if (!!$status) {
            if ($this->_prepareRecoveryModel($RecoveryForm)) {
                $message = array(
                    'url' => Yii::app()->createUrl('')
                );
                echo CJSON::encode($message);
                Yii::app()->end();
            } else {
                $this->_failureRecoveryMessage();
            }
        }
    }

    /**
     * @param RecoveryForm $RecoveryForm
     * @return bool
     */
    private function _prepareRecoveryModel(RecoveryForm $RecoveryForm)
    {
        $UserModel = $RecoveryForm->getUser();

        $RecoveryModel = new RecoveryPassword();
        $RecoveryModel->setAttributes(array(
            'user_id' => $UserModel->id,
            'code' => $RecoveryModel->generateRecoveryCode($UserModel->id),
        ));

        $RecoveryModel->onCreate = array($RecoveryModel, 'sendRecoveryEmail');
        $isSave = $RecoveryModel->save();
        if ($isSave) {
            $this->_successRecoveryMessage();
        }

        return $isSave;
    }

    private function _successRecoveryMessage()
    {
        Yii::app()->user->setFlash(
            FlashMessages::NOTICE_MESSAGE,
            Yii::t('UserModule.user', 'На указанный email отправлено письмо с инструкцией по восстановлению пароля!')
        );
    }

    private function _failureRecoveryMessage()
    {
        Yii::app()->user->setFlash(
            FlashMessages::ERROR_MESSAGE,
            Yii::t('UserModule.user', 'При восстановлении пароля произошла ошибка!')
        );
        Yii::log(
            Yii::t('UserModule.user', 'При восстановлении пароля произошла ошибка!'),
            CLogger::LEVEL_ERROR, UserModule::$logCategory
        );
    }

    private function _validateRecoveryPassword(RecoveryForm $RecoveryForm)
    {
        if ($this->_prepareRecoveryModel($RecoveryForm)) {
            Yii::log(
                Yii::t('UserModule.user', 'Заявка на восстановление пароля.'),
                CLogger::LEVEL_INFO, UserModule::$logCategory
            );
            $this->controller->redirect(array('/site/index'));
        } else {
            $this->_failureRecoveryMessage();
            $this->controller->redirect(array('/site/index'));
        }
    }
}