<?php
class RegistrationAction extends CAction
{
    public function run()
    {
        if (Yii::app()->user->isAuthenticated()) {
            $this->controller->redirect(Yii::app()->user->returnUrl);
        }

        $RegistrationForm = new RegistrationForm;
        $RegistrationPost = Yii::app()->request->getPost('RegistrationForm');
        $module = Yii::app()->getModule('user');
        $module->onBeginRegistration(new CModelEvent($RegistrationForm));

        if (!empty($RegistrationPost)) {
            $RegistrationForm->setAttributes($RegistrationPost);

            if (Yii::app()->request->isAjaxRequest) {
                $this->_ajaxValidation($RegistrationForm);
            } else {
                if ($RegistrationForm->validate()) {
                    $this->_validateUserModel($RegistrationForm);
                }
            }
        }

        $this->controller->render('registration', array('model' => $RegistrationForm, 'module' => $module));
    }

    /**
     * @param RegistrationForm $RegistrationForm
     */
    private function _ajaxValidation(RegistrationForm $RegistrationForm)
    {
        $status = Validation::performAjaxValidation($RegistrationForm);

        if (!!$status) {
            $success = $this->_validateUserModel($RegistrationForm);
            $message = array(
                'url' => Yii::app()->createAbsoluteUrl('/')
            );

            if (!$success) {
                $message = $RegistrationForm->getErrors();
            } else {
                $this->_successRegistrationMessage();
            }

            echo CJSON::encode($message);
            Yii::app()->end();
        }
    }

    /**
     * @param RegistrationForm $RegistrationForm
     * @return bool
     */
    private function _validateUserModel(RegistrationForm $RegistrationForm)
    {
        $UserModel = new User();
        $data = $RegistrationForm->getAttributes();
        unset($data['cPassword']);

        $UserModel->setAttributes($RegistrationForm->getAttributes());
        $salt = $UserModel->generateRandomPassword();
        $UserModel->setAttributes(array(
            'salt' => $salt,
            'password' => $UserModel->hashPassword($RegistrationForm->password, $salt),
        ));

        $errors = Validation::performAjaxValidation($UserModel, true);

        $success = true;
        if (true === $errors) {
            //@TODO: check if need email
            $UserModel->onCreate = array($UserModel, 'sendActivationMail');
            $UserModel->save(false);
            $this->_successRegistrationMessage();
        } else {
            $RegistrationForm->addErrors($UserModel->getErrors());
            $success = false;
        }

        return $success;
    }

    private function _successRegistrationMessage()
    {
        Yii::app()->user->setFlash(
            FlashMessages::NOTICE_MESSAGE,
            Yii::t('UserModule.user', 'Учетная запись создана! Проверьте Вашу почту!')
        );
    }
}