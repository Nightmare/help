<?php
class WebUser extends CWebUser
{
    private $_profile;
    private $_model;

    /**
     * Метод проверки пользователя на принадлежность к админам:
     *
     * @return bool is super user
     **/
    /*public function isSuperUser()
    {
        if (!$this->isAuthenticated())
            return false;

        $loginAdmTime = Yii::app()->user->getState('loginAdmTime');
        $isAdmin = Yii::app()->user->getState('isAdmin');

        if ($isAdmin == User::ACCESS_LEVEL_ADMIN && $loginAdmTime)
            return true;
        return false;
    }*/

    /**
     * Метод который проверяет, авторизирован ли пользователь:
     *
     * @return bool авторизирован ли пользователь
     **/
    public function isAuthenticated()
    {
        if (Yii::app()->user->isGuest) {
            return false;
        }

        $authData = $this->getAuthData();

        return $authData['nick_name'] && isset($authData['role']) && $authData['id'];
    }

    /**
     * Возвращаем данные по авторизации:
     *
     * @return mixed authdata
     **/
    protected function getAuthData()
    {
        return array(
            'nick_name' => Yii::app()->user->getState('nick_name'),
            'role' => (int)Yii::app()->user->getState('role'),
            'id' => (int)Yii::app()->user->getState('id'),
        );
    }

    /**
     * Метод возвращающий профайл пользователя:
     *
     * @param string $moduleName - идентификатор модуля
     *
     * @todo: Реализовать выборку любого профиля
     *
     * @return null || user profile
     **/
    public function getProfile($moduleName = null)
    {
        if (!$moduleName) {
            if ($this->_profile === null) {
                $this->_profile = User::model()->findByPk($this->id);
            }

            return $this->_profile;
        }
        return null;
    }

    public function getRole()
    {
        if ($user = $this->getModel()) {
            return $user->role;
        }
    }

    /**
     * @return CActiveRecord
     */
    private function getModel()
    {
        if (!$this->isGuest && null === $this->_model) {
            $this->_model = User::model()->findByPk($this->id, array('select' => 'role'));
        }
        return $this->_model;
    }
}