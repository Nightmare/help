<?php
class UserIdentity extends CUserIdentity
{
    const ERROR_USER_IS_NOT_ADMIN = 11;
    private $_id;

    /**
     * @return bool is user authenticated
     **/
    public function authenticate()
    {
        /** @var User $user */
        $user = User::model()->active()->findByAttributes(array('nick_name' => $this->username));
        /** @var @var UserModule $module $UserModule */
        $userModule = Yii::app()->getModule('user');

        if ($user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif (!$user->validatePassword($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } elseif ($userModule->isAdmin && $user->role == User::ROLE_USER) {
            $this->errorCode = self::ERROR_USER_IS_NOT_ADMIN;
        } else {
            $this->_id = $user->id;

//            $this->username = $UserModel->nick_name;
//            Yii::app()->user->setState('id', $UserModel->id);
//            Yii::app()->user->setState('access_level', $UserModel->access_level);
//            Yii::app()->user->setState('nick_name', $UserModel->nick_name);
//            Yii::app()->user->setState('email', $UserModel->email);
//            Yii::app()->user->setState('loginTime', time());

            // для админа в сессию запишем еще несколько значений
//            if ($UserModel->access_level == User::ACCESS_LEVEL_ADMIN) {
//                Yii::app()->user->setState('loginAdmTime', time());
//                Yii::app()->user->setState('isAdmin', $UserModel->access_level);
//
//                /* Получаем настройки по всем модулям для данного пользователя: */
//                $settings = Settings::model()->fetchUserModuleSettings(Yii::app()->user->id);
//                $sessionSettings = array();
//
//                // Если передан не пустой массив, проходим по нему: */
//                if (!empty($settings) && is_array($settings)) {
//                    foreach ($settings as $sets) {
//                        /* Если есть атрибуты - продолжаем: */
//                        if (isset($sets->attributes)) {
//                            /* Наполняем нашу сессию: */
//                            if (!isset($sessionSettings[$sets->module_id]))
//                                $sessionSettings[$sets->module_id] = array();
//                            $sessionSettings[$sets->module_id][$sets->param_name] = $sets->param_value;
//                        }
//                    }
//                }
//                Yii::app()->session['modSettings'] = $sessionSettings;
//            }

            // зафиксируем время входа
//            $UserModel->last_visit = new CDbExpression('NOW()');
//            $UserModel->update(array('last_visit'));

            $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode == self::ERROR_NONE;
    }

    /**
     * Get user ID
     *
     * @return int userID
     **/
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return string
     */
    public function getErrorText()
    {
        $message = 'Undefined error';
        switch ($this->errorCode) {
            case self::ERROR_USERNAME_INVALID:
                $message = Yii::t('UserModule.user', 'Пользователя не найдено.');
                break;
            case self::ERROR_PASSWORD_INVALID:
                $message = Yii::t('UserModule.user', 'Пароль введен неверно.');
                break;
            case self::ERROR_USER_IS_NOT_ADMIN:
                $message = Yii::t('UserModule.user', 'Пользователь не является админом.');
                break;
        }

        return $message;
    }
}