<?php

Yii::import('application.modules.user.models._base.BaseUser');

/**
 * Class User
 *
 * @method User active
 * @method User notActivated
 * @method User admin
 * @method User notUser
 * @method User user
 * @method User moderator
 */
class User extends BaseUser
{
    const GENDER_THING = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';
    const ROLE_MODERATOR = 'moderator';

    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return array(
            'active' => array(
                'condition' => 'activated_at IS NOT NULL',
            ),
            'notActivated' => array(
                'condition' => 'activated_at IS NULL',
            ),
            'admin' => array(
                'condition' => 'role = :role',
                'params' => array(':role' => self::ROLE_ADMIN),
            ),
            'notUser' => array(
                'condition' => 'role != :role',
                'params' => array(':role' => self::ROLE_USER),
            ),
            'user' => array(
                'condition' => 'role = :role',
                'params' => array(':role' => self::ROLE_USER),
            ),
            'moderator' => array(
                'condition' => 'role = :role',
                'params' => array(':role' => self::ROLE_MODERATOR),
            ),
        );
    }

    public function afterSave()
    {
        $this->onCreate(new CModelEvent($this));
        parent::afterSave();
    }

    public function onCreate($event)
    {
        $this->raiseEvent('onCreate', $event);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->getIsNewRecord()) {
            $this->created_at = new CDbExpression('NOW()');
            $this->activate_key = $this->generateActivationKey();
        }
        return parent::beforeValidate();
    }

    /**
     * @return string
     */
    public function generateActivationKey()
    {
        return md5(time() . $this->email . uniqid());
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return $this->password === $this->hashPassword($password, $this->salt);
    }

    /**
     * @param $password
     * @param $salt
     * @return string
     */
    public function hashPassword($password, $salt)
    {
        return md5($salt . $password);
    }

    /**
     * @param $nickName
     * @param $email
     * @param $password
     */
    public function createAccount($nickName, $email, $password)
    {
        $salt = $this->generateSalt();

        $this->setAttributes(array(
            'nick_name' => $nickName,
            'email' => $email,
            'created_at' => new CDbException('NOW()'),
            'password' => $this->hashPassword($password, $salt),
            'activate_key' => $this->generateActivationKey(),
            'salt' => $salt,
        ));

        $this->save(false);
    }

    /**
     * @return string
     */
    public function generateSalt()
    {
        return md5(uniqid('', true) . time());
    }

    /**
     * @param null $length
     * @return string
     */
    public function generateRandomPassword($length = null)
    {
        return substr(md5(uniqid(mt_rand(), true) . time()), 0, $length ? $length : 32);
    }

    /**
     * @return bool
     */
    public function sendActivationMail()
    {
        Yii::app()->mail->viewPath = 'webroot.themes.' . Yii::app()->theme->name . '.views.user.account';

        $message = new YiiMailMessage();
        $message->view = 'needAccountActivationEmail';

        //userModel is passed to the view
        $message->setBody(array(
            'model' => $this
        ), 'text/html');

        $message->addTo($this->email);
        $message->setSubject('Account Activation');
        $message->from = 'do-not-reply@' . Yii::app()->params['site'];
        Yii::app()->mail->send($message);

        return true;
    }

    /**
     * @return bool
     */
    public function activate()
    {
        $this->activated_at = new CDbException('NOW()');
        return $this->save();
    }

    public function getOnlineStatus()
    {
        return
            Yii::app()->db->createCommand()
                ->select('COUNT(*)')
                ->from('session')
                ->where('user_id = :user_id', array(':user_id' => $this->id))
                ->queryScalar();
    }


    public function getOnlineUsers()
    {
        return
            Yii::app()->db->createCommand()
                ->select('COUNT(*)')
                ->from('session')
                ->where('user_id = :user_id', array(':user_id' => $this->id))
                ->queryScalar();
    }
}