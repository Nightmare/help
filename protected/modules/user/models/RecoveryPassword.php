<?php

Yii::import('application.modules.user.models._base.BaseRecoveryPassword');

class RecoveryPassword extends BaseRecoveryPassword
{
    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param $userId
     * @return string
     */
    public function generateRecoveryCode($userId)
    {
        return md5(time() . $userId . uniqid());
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->created_at = new CDbExpression('NOW()');
        }

        return parent::beforeValidate();
    }

    /**
     *
     */
    public function afterSave()
    {
        $this->onCreate(new CModelEvent($this));
        return parent::afterSave();
    }

    /**
     * @param $event
     */
    public function onCreate($event)
    {
        $this->raiseEvent('onCreate', $event);
    }

    public function sendRecoveryEmail()
    {
        Yii::app()->mail->viewPath = 'webroot.themes.' . Yii::app()->theme->name . '.views.user.account';

        $message = new YiiMailMessage();
        $message->view = 'passwordRecoveryEmail';

        //userModel is passed to the view
        $message->setBody(array(
            'model' => $this
        ), 'text/html');

        $message->addTo($this->user->email);
        $message->setSubject('Recovery Password');
        $message->from = 'do-not-reply@' . Yii::app()->params['site'];
        Yii::app()->mail->send($message);

        return true;
    }
}