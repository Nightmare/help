<?php

class UserModule extends CWebModule
{
    public static $logCategory = 'application.modules.user';
    public $attachedProfileEvents = array();
    public $minPasswordLength = 3;
    public $logoutSuccess = '/site/index';
    public $loginSuccess;
    public $isAdmin = false;
    public $adminAuthLayoutControllers = array(
        'login', 'registration'
    );

    public function init()
    {
        $s = 1;
//        $subDomain = Url::getSubDomain();
//        if (empty($subDomain)) {
//            Yii::app()->theme = 'admin';
//            $this->loginSuccess = '/admin';
//            $this->isAdmin = true;
//        } else {
//            Yii::app()->theme = 'main';
//            $this->loginSuccess = '/site/index';
//        }
//
//        if (is_array($this->attachedProfileEvents)) {
//            foreach ($this->attachedProfileEvents as $event) {
//                $this->attachEventHandler("onBeginRegistration", array($event, "onBeginRegistration"));
//                $this->attachEventHandler("onBeginProfile", array($event, "onBeginProfile"));
//            }
//        }
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
//            if (Yii::app()->theme->getName() == 'admin' && in_array(strtolower($action->getId()), $this->adminAuthLayoutControllers)) {
////                $this->layout = '//layouts/auth';
//                $this->layout = 'auth';
//            }
            return true;
        } else
            return false;
    }

    public function onBeginRegistration($event)
    {
        $this->raiseEvent('onBeginRegistration', $event);
    }

    public function onBeginProfile($event)
    {
        $this->raiseEvent('onBeginProfile', $event);
    }
}
