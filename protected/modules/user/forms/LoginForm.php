<?php
class LoginForm extends CFormModel
{
    public $nick_name;
    public $password;
    public $rememberMe = false;
    private $_identity;

    public function rules()
    {
        return array(
            array('nick_name, password', 'required'),
            array('rememberMe', 'boolean'),
            array('password', 'authenticate'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'nick_name' => Yii::t('UserModule.user', 'Никнейм'),
            'password' => Yii::t('UserModule.user', 'Пароль'),
            'rememberMe' => Yii::t('UserModule.user', 'Remember me next time'),
        );
    }

    public function attributeDescriptions()
    {
        return array(
            'nick_name' => Yii::t('UserModule.user', 'Никнейм'),
            'password' => Yii::t('UserModule.user', 'Пароль'),
            'rememberMe' => Yii::t('UserModule.user', 'Remember me next time'),
        );
    }

    public function authenticate()
    {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->nick_name, $this->password);

            if (!$this->_identity->authenticate()) {
                $this->addError('password', $this->_identity->getErrorText());
            } else {
                Yii::app()->user->login($this->_identity, $this->rememberMe ? 3600 * 24 * 1000 : 0);// 1000 days
            }
        }
    }
}