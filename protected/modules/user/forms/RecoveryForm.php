<?php
class RecoveryForm extends CFormModel
{
    public $email;
    private $_user;

    public function rules()
    {
        return array(
            array('email', 'required'),
            array('email', 'email'),
            array('email', 'checkEmail'),
        );
    }

    public function checkEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->_user = User::model()->active()->find($attribute . ' = :email', array(':email' => $this->email));
            if (!$this->_user) {
                $this->addError($attribute, Yii::t('UserModule.user', 'Email "{email}" не найден или пользователь заблокирован !', array('{email}' => $this->email)));
            }
        }
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->_user;
    }
}