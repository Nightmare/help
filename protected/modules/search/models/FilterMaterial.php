<?php

Yii::import('application.modules.search.models._base.BaseFilterMaterial');
/**
 * Class FilterMaterial
 * @property Material $material
 * @property Filter $filter
 */
class FilterMaterial extends BaseFilterMaterial
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return CMap::mergeArray(
            parent::relations(),
            array(
                'material' => array(self::BELONGS_TO, 'Material', array('material_id' => 'id')),
                'filter' => array(self::BELONGS_TO, 'Filter', array('filter_id' => 'id')),
            )
        );
    }
}