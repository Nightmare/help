<?php

Yii::import('application.modules.search.models._base.BaseItemFilterValue');

/**
 * Class ItemFilterValue
 *
 * @property FilterValue $filterValue
 */
class ItemFilterValue extends BaseItemFilterValue
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return CMap::mergeArray(
            parent::relations(),
            array(
                'filterValue' => array(self::BELONGS_TO, 'FilterValue', 'filter_value_id'),
                'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
            )
        );
    }
}