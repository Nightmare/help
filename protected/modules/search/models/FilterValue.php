<?php

Yii::import('application.modules.search.models._base.BaseFilterValue');
/**
 * Class FilterValue
 * @method Item[] items($params = array())
 */
class FilterValue extends BaseFilterValue
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}