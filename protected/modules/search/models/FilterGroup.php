<?php
Yii::import('application.modules.search.models._base.BaseFilterGroup');
/**
 * Class FilterGroup
 * @property Group $group
 * @property Filter $filter
 */
class FilterGroup extends BaseFilterGroup
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return CMap::mergeArray(
            parent::relations(),
            array(
                'group' => array(self::BELONGS_TO, 'Group', array('group_id' => 'id')),
                'filter' => array(self::BELONGS_TO, 'Filter', array('filter_id' => 'id')),
            )
        );
    }
}