<?php

Yii::import('application.modules.search.models._base.BaseFilter');

class Filter extends BaseFilter
{
    /**
     * @param string $className
     * @return $this
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'filterValues' => array(self::HAS_MANY, 'FilterValue', 'filter_id'),
            'filterMaterials' => array(self::HAS_MANY, 'FilterMaterial', 'filter_id'),
            'materials' => array(self::HAS_MANY, 'Material', array('material_id' => 'id'), 'through' => 'filterMaterials'),
            'filterGroups' => array(self::HAS_MANY, 'FilterGroup', 'filter_id'),
            'groups' => array(self::HAS_MANY, 'Group', array('group_id' => 'id'), 'through' => 'filterGroups'),
        );
    }
}