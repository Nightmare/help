<?php
class DefaultController extends Controller
{
    public function actionIndex()
    {
        $items = array();
        parse_str(Yii::app()->getRequest()->getQueryString(), $params);
        ksort($params);

        if ($params) {
            $search = new Search($params);
            /** @var Item[] $items */
            $items = $search->getItems();
        }

//        $this->render('//material/view', array('model' => $ItemModels));
        $this->render('index', array('items' => $items));
    }
}