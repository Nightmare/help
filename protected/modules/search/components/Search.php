<?php
Yii::import('search.components.filters.*', true);
/**
 * Class Search
 *
 * @property DistrictFilter $DistrictFilter
 * @property RateFilter $RateFilter
 * @property ViewsFilter $ViewsFilter
 * @property SortFilter $SortFilter
 * @property AdvancedFilter $AdvancedFilter
 */
class Search implements Countable, Iterator
{
    const POSTFIX_CLASS = 'Filter';

    const ITEM_ALIAS = 'i';
    const SITE_ALIAS = 's';
    const CATEGORY_ALIAS = 'c';
    const GROUP_ALIAS = 'g';
    const MATERIAL_ALIAS = 'm';
    const DISTRICT_ALIAS = 'd';
    const LOG_CATEGORY = 'application.modules.search.components.Search';

    const TYPE_DISTRICT = 'rayon';
    const TYPE_RATE = 'reyting';
    const TYPE_SORT = 'sortirovka';
    const TYPE_ADVANCED = 'dopolnitelno';

    protected $_params;
    /**
     * @var Filter[]
     */
    protected $_filters = array();
    /**
     * @var CDbCriteria
     */
    protected $_criteria;

    protected function _getFilterTypes()
    {
        return array(
            self::TYPE_DISTRICT => 'district',
            self::TYPE_RATE => 'rate',
            self::TYPE_SORT => 'sort',
            self::TYPE_ADVANCED => 'advanced',
        );
    }

    public function __construct(array $params)
    {
        if (empty($params)) {
            throw new Exception('Invalid search params');
        }

        $this->_params = $params;
        $this->_criteria = new CDbCriteria();
        $this->_criteria->alias = self::ITEM_ALIAS;

        $this->_addDomainCriteria();

        $filterTypes = $this->_getFilterTypes();
        foreach ($params as $filter => $param) {
            if (!isset($filterTypes[$filter]) || $param == '') {
                continue;
            }

            //@todo: refactor '_id'
            $filter = ucfirst($filterTypes[$filter]) . self::POSTFIX_CLASS;
            if (!$this->_classExist($filter)) {
                Yii::log(sprintf('Class %s not found', $filter), CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
                continue;
            }
            $this->_filters[$filter] = new $filter($this->_criteria, $param);
        }

        $this->rewind();
    }

    protected function _addDomainCriteria()
    {
        $criteria = new CDbCriteria();
        $criteria->with = array(
            'topItem' => array(
                'alias' => 'tig',
                'together' => true,
                'on' => ':now > tig.expired_at OR tig.max_view_count = :infinity OR tig.max_view_count > tig.current_view_count',
            ),
        );

        $criteria->params['now'] = date('Y-m-d');
        $criteria->params['infinity'] = TopItem::SHOW_INFINITE;
        $criteria->order = 'tig.item_id DESC';
//        $criteria->with = array(
//            'materials' => array(
//                'alias' => self::MATERIAL_ALIAS,
//                'select' => false,
//                'together' => true,
//                'with' => array(
//                    'group' => array(
//                        'alias' => self::GROUP_ALIAS,
//                        'select' => false,
//                        'together' => true,
//                        'with' => array(
//                            'category' => array(
//                                'alias' => self::CATEGORY_ALIAS,
//                                'select' => false,
//                                'together' => true,
//                                'with' => array(
//                                    'site' => array(
//                                        'alias' => self::SITE_ALIAS,
//                                        'select' => false,
//                                        'together' => true,
//                                        'condition' => self::SITE_ALIAS. '.url = :url',
//                                        'params' => array(
//                                            ':url' => Url::getSubDomain()
//                                        )
//                                    )
//                                )
//                            )
//                        )
//                    )
//                )
//            ),
////            'itemFavorites' => array(
////                'alias' => 'if',
////                'joinType' => 'LEFT JOIN',
////                'scopes' => 'isFavorite',
////                'together' => true,
////            ),
////            'itemPhotos' => array(
////                'alias' => 'ip',
////                'joinType' => 'LEFT JOIN',
////                'scopes' => 'isMain',
////                'together' => true,
////                'with' => array(
////                    'photo' => array(
////                        'alias' => 'p',
////                        'together' => true,
////                    )
////                )
////            ),
//        );
        $this->_criteria->mergeWith($criteria);
    }

    /**
     * @param $className
     * @return bool
     */
    private function _classExist($className)
    {
        return file_exists(Yii::getPathOfAlias('search.components.filters') . DIRECTORY_SEPARATOR . $className . '.php');
    }

    public function rewind()
    {
        reset($this->_filters);
    }

    public function count()
    {
        return count($this->_filters);
    }

    public function key()
    {
        return key($this->_filters);
    }

    public function valid()
    {
        return ($this->current() !== false);
    }

    public function current()
    {
        return current($this->_filters);
    }

    public function next()
    {
        next($this->_filters);
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * @return CDbCriteria
     */
    public function getCriteria()
    {
        return $this->_criteria;
    }

    /**
     * @return CActiveRecord[]
     */
    public function getItems()
    {
        return Item::model()->findAll($this->_criteria);
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->_filters)) {
            return $this->_filters[$name];
        }

        return null;
    }
}