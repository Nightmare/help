<?php
class SortFilter extends AbstractFilter
{
    public function _setCriteria()
    {
        $this->_criteria->order = Search::ITEM_ALIAS . '.' . $this->getValue() . ' DESC';
    }
}