<?php
class ViewsFilter extends AbstractFilter
{
    public function _setCriteria()
    {
        $this->_criteria->compare(Search::ITEM_ALIAS . '.view_count', (int)$this->getValue());
    }
}