<?php
abstract class AbstractFilter
{
    protected $_value;
    protected $_criteria;

    /**
     * @param CDbCriteria $criteria
     * @param $value
     */
    public function __construct(CDbCriteria $criteria, $value)
    {
        $this->_value = $value;
        $this->_criteria = new CDbCriteria();
        $this->_setCriteria();

        $criteria->mergeWith($this->_criteria);
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * @return CDbCriteria
     */
    public function getCriteria()
    {
        return $this->_criteria;
    }

    abstract protected function _setCriteria();
}