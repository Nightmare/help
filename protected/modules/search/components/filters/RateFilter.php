<?php
class RateFilter extends AbstractFilter
{
    public function _setCriteria()
    {
        $this->_criteria->compare(Search::ITEM_ALIAS . '.rate', (int)$this->getValue());
    }
}