<?php
class AdvancedFilter extends AbstractFilter
{
    public function _setCriteria()
    {
        $values = $this->getValue();
        if (!$values || !is_array($values)) {
            return;
        }

        foreach ($values as $filterName => $filterValues) {
            if (!$filterValues) {
                continue;
            }

            $criteria = new CDbCriteria();
            $criteria->alias = 'ifv';
            $criteria->with = array(
                'filterValue' => array(
                    'alias' => 'fv',
                    'joinType' => 'INNER JOIN',
                    'with' => array(
                        'filter' => array(
                            'alias' => 'f',
                            'joinType' => 'INNER JOIN',
                        ),
                    ),
                ),
            );

            $criteria->compare('f.name', $filterName);
            $criteria->addInCondition('fv.name', $filterValues);
            if ($itemFilterValues = ItemFilterValue::model()->findAll($criteria)) {
                $this->_criteria->addInCondition(Search::ITEM_ALIAS . '.id', array_map(function($ifv) { return $ifv->item_id; }, $itemFilterValues));
            }
        }

        $this->_criteria->distinct = true;
    }
}