<?php
class DistrictFilter extends AbstractFilter
{
    public function _setCriteria()
    {
        $this->_criteria->with = array(
            'district' => array(
                'alias' => Search::DISTRICT_ALIAS,
                'together' => true,
                'joinType' => 'INNER JOIN',
            ),
        );

        $this->_criteria->compare(Search::DISTRICT_ALIAS . '.name', $this->getValue());
    }
}