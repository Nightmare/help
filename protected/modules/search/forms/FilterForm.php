<?php
class FilterForm extends CFormModel
{
    public $rate;
    public $views;
    public $district_id;
    public $sort;
    public $advanced;

    public function rules()
    {
        return array(
            array('district_id, rate, views', 'numerical', 'integerOnly' => true),
            array('district_id, description, views, sort, advanced', 'default', 'setOnEmpty' => true, 'value' => null)
        );
    }

//    public function relations()
//    {
//        return array(
//            'district' => array(self::BELONGS_TO, 'District', 'district_id'),
//        );
//    }

    public function attributeLabels()
    {
        return array(
            'rate' => Yii::t('user', 'Рейтинг'),
            'views' => Yii::t('user', 'К-во просмотров'),
            'district_id' => Yii::t('user', 'Район'),
            'advanced' => '',
            'sort' => 'Сортировать'
        );
    }

    public static function getSortStates()
    {
        return array(
            'rate' => 'Рейтингу',
            'view_count' => 'К-ву просмотров'
        );
    }
}