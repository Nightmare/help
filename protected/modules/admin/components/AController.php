<?php
Yii::import('application.helpers.YText');
Yii::import('application.helpers.YFile');
class AController extends CController
{
    public $layout = '//layouts/main';
//    public $layout = '//layouts/extJs';
    private $_request,
        $_criteria,
        $_limit,
        $_page,
        $_start;

    /**
     * @var BackendSettings
     */
    public $settingsModel;

    public function filters()
    {
        return array(
//            'ajaxOnly + read, save, delete',
            'ajaxOnly + read, delete',
//            'accessControl'
        );
    }

    public function accessRules()
    {
        return array(
//            array('allow',
//                'roles' => array('admin'),
//            ),
//            array('allow',
//                'actions' => array('read', 'save', 'upload'),
//                'roles' => array('moderator'),
//            ),
//            array('allow',
//                'controllers' => array('default'),
//                'actions' => array('index'),
//                'users' => array('*'),
//            ),
//            array('deny',
//                'users' => array('*'),
//            ),
        );
    }

    public function init()
    {
        parent::init();
        $this->_request = Yii::app()->getRequest();
        $this->_criteria = new CDbCriteria();

        $this->_limit = $this->_getRequest()->getParam('limit');
        $this->_page = $this->_getRequest()->getParam('page');
        $this->_start = $this->_getRequest()->getParam('start');
//        $this->settingsModel = BackendSettings::model()->findByAttributes(array('user_id' => Yii::app()->user->id));
        $this->settingsModel = BackendSettings::model()->findByPk(1);
        if (empty($this->settingsModel)) {
            $this->settingsModel = new BackendSettings();
            $this->settingsModel->user_id = Yii::app()->user->id;
            $this->settingsModel->save();
        }
    }

    /**
     * @return CHttpRequest
     */
    protected function _getRequest()
    {
        if (!$this->_request) {
            $this->_request = Yii::app()->getRequest();
        }

        return $this->_request;
    }

    protected function _getPage()
    {
        return $this->_page;
    }

    protected function _getPagerCriteria()
    {
        $criteria = clone $this->_getCriteria();
        $criteria->limit = $this->_getLimit();
        $criteria->offset = $this->_getStart();

        return $criteria;
    }

    protected function _getCriteria()
    {
        if (!$this->_criteria) {
            $this->_criteria = new CDbCriteria();
        }

        return $this->_criteria;
    }

    protected function _getLimit()
    {
        return $this->_limit;
    }

    protected function _getStart()
    {
        return $this->_start;
    }
}