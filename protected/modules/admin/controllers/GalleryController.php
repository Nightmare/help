<?php
Yii::import('application.components.ImageUploader');
class GalleryController extends AController
{
    public function actionSave()
    {
        $id = (int)$this->_getRequest()->getParam('id');
        $title = $this->_getRequest()->getParam('title');
        $photo = Photo::model()->findByPk($id);
        if (!$photo) {
            Yii::app()->ajax->extFailure('Фотография не найдена');
        }

        $photo->title = $title;
        if ($photo->save()) {
            Yii::app()->ajax->extSuccess('all cool');
        } else {
            Yii::app()->ajax->extFailure($photo->getErrors());
        }
    }

    public function actionRead()
    {
        $filter = $this->_getRequest()->getParam('q');
        $itemId = (int)$this->_getRequest()->getParam('item_id');

        if (isset($filter)) {
            $this->_getCriteria()->addSearchCondition('title', $filter);
        }

        $ids = array();

        if ($itemId && $this->_getPage() == 1) {
            $criteria = clone $this->_getCriteria();
            $criteria->select = 'id';
            $criteria->with = array(
                'items' => array(
                    'alias' => 'i',
                    'select' => false,
                    'together' => true,
                    'joinType' => 'INNER JOIN',
                    'condition' => 'i.id = :item_id',
                    'params' => array(
                        ':item_id' => $itemId
                    )
                )
            );

            $photos = Photo::model()->findAll($criteria);
            $ids = array_map(function ($photo) {
                return $photo->id;
            }, $photos);
            unset ($criteria, $photos);
        }

        if (!empty($ids)) {
            $this->_getPagerCriteria()->order = sprintf('id IN (%s) DESC, id', implode(',', $ids));
        }

        /** @var Photo[] $photos */
        $photos = Photo::model()->findAll($this->_getPagerCriteria());

        $result = array();
        foreach ($photos as $photo) {
            $_photo = array(
                'id' => $photo->id,
                'title' => $photo->title,
                'path' => $photo->getThumbSmall(),
                'is_main' => $photo->id,
                'selected' => false
            );

            if (in_array($photo->id, $ids)) {
                $_photo['selected'] = true;
            }

            $result[] = $_photo;
        }

        Yii::app()->ajax->raw(
            array(
                'success' => true,
                'resultTotal' => Photo::model()->count($this->_getCriteria()),
                'data' => $result
            )
        );
    }

    public function actionUpload()
    {
        $images = CUploadedFile::getInstancesByName('photo');
        $img = array();
        if ($this->_getRequest()->getIsPostRequest() && !empty($images)) {
            $tr = Yii::app()->db->beginTransaction();
            $uploader = new ImageUploader();
            foreach ($images as $image) {
                $photo = new Photo();
                $photo->setScenario('upload');
                $photo->title = pathinfo($image->getName(), PATHINFO_FILENAME);
                $photo->file_name = $image;
                $photo->owner_id = null;//Yii::app()->user->id;
                if ($success = $photo->save()) {
                    if ($uploader->upload($photo->getImagePath(), pathinfo($image->getName(), PATHINFO_BASENAME), $image)) {
                        $img[] = $photo->getThumbSmall();
                    }
                } else {
                    unset($photo);
                }
            }
            $tr->commit();
        }

        echo CJSON::encode(array('success' => true, 'images' => $img));
    }

    public function actionDelete()
    {
        $tr = Photo::model()->dbConnection->beginTransaction();
        try {
            $ids = $this->_getRequest()->getPost('ids');
            foreach ($ids as $id) {
                if (!Photo::model()->findByPk($id)->delete()) {
                    Yii::app()->ajax->extFailure('Невозможно удалить фотографии');
                    $tr->rollback();
                }
            }
            $tr->commit();
            Yii::app()->ajax->extSuccess(Yii::t('app', 'Фотографии успешно удалены'));
        } catch (Exception $e) {
            $tr->rollback();
            Yii::app()->ajax->extFailure($e->getMessage());
        }
    }
}