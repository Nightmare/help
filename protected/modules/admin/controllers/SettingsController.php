<?php
class SettingsController extends AController
{
    public function actionSave()
    {
    }

    public function actionSaveWallpaper()
    {
        $wallpaper = $this->_getRequest()->getParam('wallpaper');
        $wallpaperStretch = (int)$this->_getRequest()->getParam('wallpaper_stretch');
        $this->settingsModel->wallpaper = $wallpaper;
        $this->settingsModel->wallpaper_stretch = $wallpaperStretch;
        if ($this->settingsModel->save()) {
            Yii::app()->ajax->extSuccess(Yii::t('app', 'Обои сохранены'));
        } else {
            Yii::app()->ajax->extFailure($this->settingsModel->getErrors());
        }
    }
}