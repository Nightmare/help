<?php
class CategoryController extends AController
{
    public function actionSave()
    {
        $title = $this->_getRequest()->getPost('title');
        $siteId = (int)$this->_getRequest()->getPost('site_id');
        $id = (int)$this->_getRequest()->getPost('id');

        if (!empty($siteId) && !empty($title)) {
            $category = $id ? Category::model()->findByPk($id) : new Category();

            if (!$category) {
                Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Категория не найдена, попробуйте еще раз.'));
            }

            $category->site_id = $siteId;
            $category->title = $title;
            $category->name = YText::translit($title);

            if ($category->save()) {
                Yii::app()->ajax->extSuccess('all cool');
            } else {
                Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Невозможно сохранить категорию'));
            }
        } else {
            Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Нет данных для сохранения категории'));
        }
    }

    public function actionRead()
    {
        /** @var Category[] $categories */
        $categories = Category::model()->bySequence()->findAll();
        $result = array();
        foreach ($categories as $category) {
            $result[] = array(
                'id' => $category->id,
                'title' => $category->title,
                'site_id' => $category->site->name,
                'sequence' => $category->sequence,
            );
        }

        Yii::app()->ajax->extSuccess($result);
    }

    public function actionDelete()
    {
        $tr = Category::model()->dbConnection->beginTransaction();
        try {
            $ids = $this->_getRequest()->getPost('ids');
            foreach ($ids as $id) {
                if (!Category::model()->deleteByPk($id)) {
                    Yii::app()->ajax->extFailure('Невозможно удалить категории');
                    $tr->rollback();
                }
            }
            $tr->commit();
            Yii::app()->ajax->extSuccess(Yii::t('app', 'Категории успешно удалены'));
        } catch (Exception $e) {
            $tr->rollback();
            Yii::app()->ajax->extFailure($e->getMessage());
        }
    }

    public function actionRecognize()
    {
        $categories = (array)json_decode($this->_getRequest()->getPost('value'), true);
        foreach ($categories as $id => $sequence) {
            $category = Category::model()->find($id);
            if (!$category) {
                continue;
            }

            $category->sequence = $sequence;
            $category->save(false);
        }

        Yii::app()->ajax->extSuccess(Yii::t('app', 'Категории успешно обвновлены'));
    }
}