<?php
Yii::import('application.components.CImageHandler');
class ItemController extends AController
{
    public function actionSave()
    {
        $itemPost = $this->_getRequest()->getPost('Item');
        $contactsPost = $this->_getRequest()->getParam('Contacts');
        $photosPost = $this->_getRequest()->getParam('Photos');
        $mainPhotoId = $this->_getRequest()->getParam('main_photo');

        $this->layout = false;

        if (!empty($itemPost)) {
            /** @var Item $item */
            $item = !empty($itemPost['id']) ? Item::model()->findByPk($itemPost['id']) : new Item();
            if (!$item) {
                Yii::app()->ajax->extFailure('Не возможно найти объект.');
            }

            $filterValues = array();
            if (isset($itemPost['filter_values'])) {
                try {
                    $filterValues = CJSON::decode($itemPost['filter_values']);
                    unset($itemPost['filter_values']);
                } catch (Exception $e) {
                    Yii::log('Cannot parse filter values', CLogger::LEVEL_ERROR);
                }
            }

            $top = array();
            if (isset($itemPost['top'])) {
                $top = $itemPost['top'];
                unset($itemPost['top']);
            }

            $marker = CUploadedFile::getInstance($item, 'logo');
            $isEmptyMarker = empty($marker);

            if ($isEmptyMarker) {
                unset($itemPost['logo']);
            } else {
                $item->setScenario('create');
            }

            if (isset($itemPost['template_id'])) {
                $itemPost['template_id'] = is_numeric($itemPost['template_id']) ? (int)$itemPost['template_id'] : null;
            }

            $item->setAttributes($itemPost);

            if (!empty($item->template_id)) {
                $type = Item::TYPE_MATERIAL;
                /** @var Template $template */
                if (($template = Template::model()->findByPk($item->template_id))
                    && (null != $template->group_id)
                ) {
                    $type = Item::TYPE_GROUP;
                }

                $item->setAttribute('type', $type);
            }

            if ($title = $item->getAttribute('title')) {
                $item->setAttribute('name', YText::translit($title));
            }

            if (!$isEmptyMarker) {
                $item->marker = $marker;
            }

            if ($item->save()) {
                $topItem = $item->topItem;
                if ($top == 'delete') {
                    if ($topItem) {
                        $item->topItem->delete();
                    }
                } else {
                    try {
                        $top = CJSON::decode($top);

                        if ($top['current_view_count'] == '') {
                            unset($top['current_view_count']);
                        }
                        if ($top['paid_at'] == '') {
                            unset($top['paid_at']);
                        }

                        $top['item_id'] = $item->id;
                        $topItem = $topItem ? : new TopItem();
                        $topItem->setAttributes($top);
                        $topItem->save();
                    } catch (Exception $e) {
                        Yii::log('Cannot parse filter values', CLogger::LEVEL_ERROR);
                    }
                }

                if (!$isEmptyMarker) {
                    $dirName = $item->getMarkerPath();
                    if (!is_dir($dirName)) {
                        mkdir($dirName, 1755, true);
                    }

                    $filename = $item->getMarker(true);
                    $item->marker->saveAs($filename);

                    $ih = new CImageHandler();
                    $ih
                        ->load($filename)
                        ->thumb(100, false)
                        ->save($item->getThumbMarker(true));
                }

                if (!empty($contactsPost)) {
                    ItemContact::model()->deleteAllByAttributes(array('item_id' => $item->id));
                    //@TODO: delete contact too
                    foreach ($contactsPost as $contactPost) {
                        $contact = new Contact();
                        $contact->type = $contactPost['type'];
                        $contact->value = $contactPost['value'];
                        $contact->save();
                        $this->_saveItemContact($item->id, $contact->id);
                    }
                }

                if (!empty($photosPost)) {
                    ItemPhoto::model()->deleteAllByAttributes(array('item_id' => $item->id));
                    foreach ($photosPost as $photo) {
                        $itemPhoto = new ItemPhoto();
                        $itemPhoto->setAttributes(array(
                            'item_id' => $item->id,
                            'photo_id' => (int)$photo,
                            'is_main' => (int)($mainPhotoId == $photo),
                        ));
                        $itemPhoto->save();
                    }
                }

                if ($filterValues) {
                    ItemFilterValue::model()->deleteAllByAttributes(array('item_id' => $item->id));
                    foreach ($filterValues as $values) {
                        $values = (array)$values;
                        foreach ($values as $value) {
                            $itemFilterValue = new ItemFilterValue();
                            $itemFilterValue->setAttributes(array(
                                'item_id' => $item->id,
                                'filter_value_id' => $value,
                            ));

                            $itemFilterValue->save();
                        }
                    }
                }

                Yii::app()->ajax->extSuccess($item->attributes);
            } else {
                Yii::app()->ajax->extFailure($item->getErrors());
            }
        } else {
            Yii::app()->ajax->extFailure('Не заполненые данные.');
        }
    }

    /**
     * @param $itemId
     * @param $contactId
     */
    private function _saveItemContact($itemId, $contactId)
    {
        $itemContact = new ItemContact();
        $itemContact->item_id = $itemId;
        $itemContact->contact_id = $contactId;
        $itemContact->save();
    }

    public function actionRead()
    {
        $filter = $this->_getRequest()->getParam('q');

        if (isset($filter)) {
            $this->_getCriteria()
                ->addSearchCondition('title', $filter)
                ->addSearchCondition('description', $filter, true, 'OR')
                ->addSearchCondition('address', $filter, true, 'OR');
        }

        /** @var Item[] $items */
        $items = Item::model()->findAll($this->_getPagerCriteria());

        $result = array();
        foreach ($items as $item) {
            $contacts = array();
            foreach ($item->contacts as $contact) {
                $contacts[] = array(
                    'id' => $contact->id,
                    'type' => $contact->type,
                    'value' => $contact->value,
                );
            }

            $result[] = array(
                'id' => $item->id,
                'title' => $item->title,
                'description' => $item->description,
                'rate' => $item->rate,
                'address' => $item->address,
                'view_count' => $item->view_count,
                'lat' => $item->lat,
                'lng' => $item->lng,
                'logo' => pathinfo($item->getMarker(), PATHINFO_BASENAME),
                'Contact' => $contacts,
                'photos_count' => count($item->photos),
                'district_id' => $item->district_id,
                'template_id' => $item->template_id,
                'value_count' => count($item->itemFilterValues),
            );
        }

        Yii::app()->ajax->raw(
            array(
                'success' => true,
                'resultTotal' => Item::model()->count($this->_getCriteria()),
                'data' => $result
            )
        );
    }

    public function actionDelete()
    {
        $tr = Item::model()->dbConnection->beginTransaction();
        try {
            $ids = $this->_getRequest()->getPost('ids');
            foreach ($ids as $id) {
                if (!Item::model()->deleteByPk($id)) {
                    Yii::app()->ajax->extFailure('Невозможно удалить объекты');
                    $tr->rollback();
                }
            }
            $tr->commit();
            Yii::app()->ajax->extSuccess(Yii::t('app', 'Объекты успешно удалены'));
        } catch (Exception $e) {
            $tr->rollback();
            Yii::app()->ajax->extFailure($e->getMessage());
        }
    }
}