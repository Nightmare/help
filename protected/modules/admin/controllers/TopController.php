<?php
class TopController extends AController
{
    public function actionReadItem()
    {
        /** @var TopItem[] $topItems */
        if ($itemId = $this->_getRequest()->getParam('item_id')) {
            $this->_getCriteria()->compare('item_id', $itemId);
            $topItems = TopItem::model()->findAll($this->_getCriteria());
        } else {
            $topItems = TopItem::model()->findAll($this->_getPagerCriteria());
        }

        $ar = array();
        foreach ($topItems as $topItem) {
            $ar[] = array(
                'item_id' => $topItem->item_id,
                'owner_id' => $topItem->owner_id,
                'max_view_count' => $topItem->max_view_count,
                'current_view_count' => $topItem->current_view_count,
                'paid_at' => $topItem->paid_at,
                'expired_at' => $topItem->expired_at,
            );
        }

        Yii::app()->ajax->raw(
            array(
                'success' => true,
                'resultTotal' => TopItem::model()->count($this->_getCriteria()),
                'data' => $ar
            )
        );
    }

    public function actionSaveItem()
    {
        $topItemPost = $this->_getRequest()->getPost('TopItem');
//        $topItemPost['item_id'] = $topItemPost['id'];
        unset($topItemPost['id']);

        if (!empty($topItemPost)) {
            $topItem = !empty($topItemPost['paid_at'])
                ? TopItem::model()->findByAttributes(array('item_id' => $topItemPost['item_id'], 'owner_id' => $topItemPost['owner_id']))
                : new TopItem();
            $topItem->setAttributes($topItemPost);

            if ($topItem->save()) {
                Yii::app()->ajax->extSuccess(Yii::t('app', 'all cool'));
            } else {
                Yii::app()->ajax->extFailure($topItem->getErrors());
            }
        } else {
            Yii::app()->ajax->extFailure(Yii::t('', 'Нет данных для сохранения топа объектов для группы'));
        }
    }

    public function actionDeleteItem()
    {
        $ids = (array) $this->_getRequest()->getPost('ids');

        if (!empty($ids)) {
            $criteria = new CDbCriteria();
            $criteria->addInCondition('item_id', $ids);
            if (TopItem::model()->deleteAll($criteria)) {
                Yii::app()->ajax->extSuccess(Yii::t('app', 'all cool'));
            } else {
                Yii::app()->ajax->extFailure('Невозможно удалить топ объектов для группы');
            }
        } else {
            Yii::app()->ajax->extFailure(Yii::t('', 'Нет данных для удаления топа объектов для группы'));
        }
    }
}