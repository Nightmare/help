<?php
class GroupController extends AController
{
    public function actionSave()
    {
        $title = $this->_getRequest()->getPost('title');
        $categoryId = (int)$this->_getRequest()->getPost('category_id');
        $id = (int)$this->_getRequest()->getPost('id');
        /** @var StdClass[] $filters */
        $filters = CJSON::decode($this->_getRequest()->getPost('filters'));

        if (!empty($categoryId) && !empty($title)) {
            $filterIds = array_map(function($filter) { return $filter['id']; }, $filters);

            if ($id) {
                $group = Group::model()->findByPk($id);

                // delete from checked filter value
                $criteria = new CDbCriteria();
                $criteria->with = array(
                    'filterValue' => array(
                        'joinType' => 'INNER JOIN',
                        'together' => true,
                        'with' => array(
                            'filter' => array(
                                'joinType' => 'INNER JOIN',
                                'with' => array(
                                    'filterGroups' => array(
                                        'joinType' => 'INNER JOIN',
                                        'alias' => 'fg',
                                    ),
                                ),
                            ),
                        ),
                    ),
                );
                $criteria->compare('fg.group_id', $group->id);
                foreach (ItemFilterValue::model()->findAll($criteria) as $itemFilterValue) {
                    /** @var ItemFilterValue $itemFilterValue */
                    if (!in_array($itemFilterValue->filterValue->filter_id, $filterIds)) {
                        $itemFilterValue->delete();
                    }
                }

                FilterGroup::model()->deleteAllByAttributes(array('group_id' => $id));
            } else {
                $group = new Group();
            }

            $group->setAttributes(array(
                'category_id' => $categoryId,
                'title' => $title,
                'name' => YText::translit($title),
            ));

            if ($group->save()) {
                foreach ($filters as $filter) {
                    $filterGroup = new FilterGroup();
                    $filterGroup->filter_id = $filter['id'];
                    $filterGroup->group_id = $group->id;
                    $filterGroup->save();
                }

                if (!empty($filterIds)) {
                    /** @var Material[] $material */
                    if ($material = Material::model()->findAllByAttributes(array('group_id' => $group->id))) {
                        $materialIds = array_map(function($material) { return (int)$material->id; }, $material);

                        $criteria = new CDbCriteria();
                        $criteria
                            ->addInCondition('material_id', $materialIds)
                            ->addInCondition('filter_id', $filterIds);

                        FilterMaterial::model()->deleteAll($criteria);
                    }
                }

                Yii::app()->ajax->extSuccess('all cool');
            } else {
                Yii::app()->ajax->extFailure($group->getErrors());
            }
        } else {
            Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Нет данных для сохранения группы'));
        }
    }

    public function actionRead()
    {
        $activeId = 0;
        $filter = $this->_getRequest()->getParam('q');
        $this->_getCriteria()->alias = 'g';
        if ($excludeTemplateId = (int)$this->_getRequest()->getParam('excludeTemplateId')) {
            /** @var Template $template */
            $template = Template::model()->findByPk($excludeTemplateId);
            $activeId = $template->group_id;
        }

        if (isset($filter)) {
            $this->_getCriteria()->addSearchCondition('title', $filter);
        }

        /** @var Group[] $groups */
        $groups = Group::model()->findAll($this->_getPagerCriteria());

        $ar = array();
        foreach ($groups as $group) {
            $ar[] = array(
                'id' => $group->id,
                'title' => $group->title,
                'category_id' => (int)$group->category_id,
                'category_title' => $group->category->title,
                'active' => $activeId == $group->id,
            );
        }

        if ($activeId && $ar) {
            usort($ar, function($a, $b) { return $b['active'] > $a['active']; });
        }

        Yii::app()->ajax->raw(
            array(
                'success' => true,
                'resultTotal' => Group::model()->count($this->_getCriteria()),
                'data' => $ar,
            )
        );
    }

    public function actionDelete()
    {
        $tr = Group::model()->dbConnection->beginTransaction();
        try {
            $ids = $this->_getRequest()->getPost('ids');
            foreach ($ids as $id) {
                if (!Group::model()->deleteByPk($id)) {
                    Yii::app()->ajax->extFailure('Невозможно удалить группы');
                    $tr->rollback();
                }
            }
            $tr->commit();
            Yii::app()->ajax->extSuccess(Yii::t('app', 'Группы успешно удалены'));
        } catch (Exception $e) {
            $tr->rollback();
            Yii::app()->ajax->extFailure($e->getMessage());
        }
    }
}