<?php
class CommentController extends AController
{
    public function actionRead()
    {
        $filter = $this->_getRequest()->getParam('q');
        $type = $this->_getRequest()->getParam('type');

        if (isset($filter)) {
            $this->_getCriteria()->addSearchCondition('text', $filter);
        }

//        $alias = strtolower($type) . 's';
        $relationName = sprintf('comment%ss', ucfirst($type));
        $this->_getCriteria()->order = 't.created_at DESC';
        $this->_getCriteria()->with = array(
            $relationName => array(
                'alias' => substr($type, -1),
                'select' => false,
                'together' => true,
            )
        );

        $comments = Comment::model()->findAll($this->_getPagerCriteria());

        $result = array();
        foreach ($comments as $comment) {
            $result[] = array(
                'id' => $comment->id,
                'text' => $comment->text,
                'user' => $comment->user ? $comment->user->fullname : 'Гость',
                'created_at' => $comment->created_at,
                'item_id' => $comment->{$relationName}[0]->item_id,
                'is_approved' => (bool)$comment->is_approved,
            );
        }

        Yii::app()->ajax->raw(
            array(
                'success' => true,
                'resultTotal' => Comment::model()->count($this->_getCriteria()),
                'data' => $result,
            )
        );
    }

    public function actionSave()
    {
        $id = (int)$this->_getRequest()->getParam('id');
        $isApproved = (int)$this->_getRequest()->getParam('is_approved');
        if ($id) {
            /** @var Comment $comment */
            $comment = Comment::model()->findByPk($id);
            $comment->is_approved = (int)$isApproved;
            if ($comment->save()) {
                Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'all cool'));
            } else {
                Yii::app()->ajax->extFailure($comment->getErrors());
            }
        } else {
            Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Нет данных для сохранения комментариев'));
        }
    }
}