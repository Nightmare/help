<?php
class FilterController extends AController
{
    public function actionSave()
    {
        $id = (int)$this->_getRequest()->getParam('id');
        $title = $this->_getRequest()->getParam('title');
        $type = $this->_getRequest()->getParam('type');
        /** @var stdClass[] $values */
        $values = CJSON::decode($this->_getRequest()->getParam('filter_values'), false);

        if (!empty($title) && !empty($type)) {
            if ($id) {
                FilterValue::model()->deleteAllByAttributes(array('filter_id' => $id));
                $filter = Filter::model()->findByPk($id);
            } else {
                $filter = new Filter();
            }

            $filter->title = $title;
            $filter->name = YText::translit($title);
            $filter->type = $type;

            if ($filter->save()) {
                foreach ($values as $value) {
                    $filterValue = new FilterValue();
                    $filterValue->name = YText::translit($value->value);
                    $filterValue->title = $value->value;
                    $filterValue->filter_id = $filter->id;
                    $filterValue->save();
                }
                Yii::app()->ajax->extSuccess('all cool');
            } else {
                Yii::app()->ajax->extFailure($filter->getErrors());
            }
        } else {
            Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Нет данных для сохранения фильтра.'));
        }
    }

    public function actionRead()
    {
        $filterTitle = $this->_getRequest()->getParam('q');
        $groupId = (int)$this->_getRequest()->getParam('groupId');
        $materialId = (int)$this->_getRequest()->getParam('materialId');
        $templateId = (int)$this->_getRequest()->getParam('templateId');

        $excludeGroupId = (int)$this->_getRequest()->getParam('excludeGroupId');
        $excludeMaterialId = (int)$this->_getRequest()->getParam('excludeMaterialId');

        $excludeFilterIds = $this->_getRequest()->getParam('excludeFilterIds');

        $this->_getCriteria()->alias = 'f';
        if (isset($filterTitle)) {
            $this->_getCriteria()->addSearchCondition('f.title', $filterTitle);
        }

        if ($template = Template::model()->findByPk($templateId)) {
            /** @var callable $getFilterIdsByGroupsIds */
            $getFilterIdsByGroupsIds = function(array $groupIds) {
                $criteria = new CDbCriteria();
                $criteria->addInCondition('group_id', $groupIds);
                return array_map(function($filterGroup) {
                    return (int)$filterGroup->filter_id;
                }, FilterGroup::model()->findAll($criteria));
            };

            $filterIds = $groupIds = array();

            if (!$template->group_id) {
                foreach ($template->templateFilterMaterials as $material) {
                    $filterIds[] = $material->filter_id;
                }

                foreach ($template->materials as $material) {
                    $groupIds[] = $material->group_id;
                }
            } else {
                foreach ($template->templateFilterGroups as $group) {
                    $filterIds[] = $group->filter_id;
                    $groupIds[] = $group->group_id;
                }
            }

            if ($groupIds = array_unique($groupIds)) {
                $filterIds = array_merge($filterIds, $getFilterIdsByGroupsIds($groupIds));
            }

            if ($filterIds = array_unique($filterIds)) {
                $this->_getCriteria()->addInCondition('f.id', $filterIds);
            }
        }

        if (is_array($excludeFilterIds) && count($excludeFilterIds)) {
            $this->_getCriteria()->addNotInCondition('f.id', $excludeFilterIds);
        }

        if (!empty($groupId)) {
            $this->_getCriteria()->with['groups'] = array(
                'select' => false,
                'alias' => 'g',
                'together' => true,
                'condition' => 'g.id = :groupId',
                'params' => array(
                    ':groupId' => $groupId,
                ),
            );
        }

        if (!empty($materialId)) {
            $this->_getCriteria()->with['materials'] = array(
                'select' => false,
                'alias' => 'm',
                'together' => true,
                'jointType' => 'INNER JOIN',
                'condition' => 'm.id = :materialId',
                'params' => array(
                    ':materialId' => $materialId
                ),
            );
        }

        if (!empty($excludeGroupId)) {
            $this->_getCriteria()
                ->addCondition('f.id NOT IN (SELECT DISTINCT fg.filter_id FROM filter_group fg WHERE fg.group_id = :excludeGroupId)')
                ->params[':excludeGroupId'] = $excludeGroupId;
        }

        if (!empty($excludeMaterialId)) {
            $this->_getCriteria()
                ->addCondition('f.id NOT IN (SELECT DISTINCT fm.filter_id FROM filter_material fm WHERE fm.material_id = :excludeMaterialId)')
                ->params[':excludeMaterialId'] = $excludeMaterialId;
        }

        /** @var Filter[] $filters */
        $filters = Filter::model()->findAll($this->_getPagerCriteria());

        $result = array();
        foreach ($filters as $filter) {
            $result[] = array(
                'id' => $filter->id,
                'title' => $filter->title,
                'type' => $filter->type,
                'name' => $filter->name,
            );
        }

        Yii::app()->ajax->raw(
            array(
                'success' => true,
                'resultTotal' => Filter::model()->count($this->_getCriteria()),
                'data' => $result
            )
        );
    }

    public function actionReadValue()
    {
        $filterId = (int)$this->_getRequest()->getParam('filterId');
        $itemId = (int)$this->_getRequest()->getParam('itemId');

        $criteria = new CDbCriteria();
        $criteria->alias = 'fv';
        $criteria->compare('fv.filter_id', $filterId);

        /** @var FilterValue[] $filterValues */
        $filterValues = FilterValue::model()->findAll($criteria);
        $result = array();
        foreach ($filterValues as $filterValue) {
            $items = $itemId
                ? $filterValue->items(array(
                    'joinType' => 'INNER JOIN',
                    'condition' => 'id = :itemsId',
                    'params' => array(':itemsId' => $itemId),
                  ))
                : array();

            $result[] = array(
                'id' => $filterValue->id,
                'filter_id' => $filterId,
                'value' => $filterValue->title,
                'active' => !empty($items),
            );
        }

        Yii::app()->ajax->raw(
            array(
                'success' => true,
                'resultTotal' => count($result),
                'data' => $result,
            )
        );
    }

    public function actionReadFilterValues()
    {
        $itemId = (int)$this->_getRequest()->getParam('itemId');
        $filterIds = (array)$this->_getRequest()->getParam('filterIds');

        $criteria = new CDbCriteria();
        $criteria->alias = 'f';
        $criteria->with = array(
            'filterValues' => array(
                'alias' => 'fv',
                'together' => true,
                'with' => array(
                    'items' => array(
                        'alias' => 'i',
                        'select' => 'id',
                        'on' => 'i.id = :itemId',
                        'together' => true,
                        'params' => array(
                            ':itemId' => $itemId,
                        ),
                    ),
                ),
            ),
        );
        $criteria->addInCondition('f.id', $filterIds);

        $result = array();
        $filters = Filter::model()->findAll($criteria);
        /** @var Filter $filter */
        foreach ($filters as $filter) {
            $item = array(
                'id' => $filter->id,
                'title' => $filter->title,
                'type' => $filter->type,
                'name' => $filter->name,
            );

            $values = array();
            foreach ($filter->filterValues as $filterValue) {
                $values[] = array(
                    'id' => $filterValue->id,
                    'filter_id' => $filterValue->filter_id,
                    'value' => $filterValue->title,
                    'active' => !empty($filterValue->items),
                );
            }

            $item['values'] = $values;
            $result[] = $item;
        }

        Yii::app()->ajax->raw(
            array(
                'success' => true,
                'resultTotal' => count($result),
                'data' => $result,
            )
        );
    }

    public function actionDelete()
    {
        $tr = Filter::model()->dbConnection->beginTransaction();
        try {
            $ids = $this->_getRequest()->getPost('ids');
            foreach ($ids as $id) {
                if (!Filter::model()->deleteByPk($id)) {
                    Yii::app()->ajax->extFailure('Невозможно удалить фильтры');
                    $tr->rollback();
                }
            }
            $tr->commit();
            Yii::app()->ajax->extSuccess(Yii::t('app', 'Фильтры успешно удалены'));
        } catch (Exception $e) {
            $tr->rollback();
            Yii::app()->ajax->extFailure($e->getMessage());
        }
    }

    public function actionDeleteValues()
    {
        if ($ids = (array)$this->_getRequest()->getPost('ids')) {
            $criteria = new CDbCriteria();
            $criteria->addInCondition('id', $ids);
            FilterValue::model()->deleteAll($criteria);
        }

        Yii::app()->ajax->extSuccess(Yii::t('app', 'Значения фильтра успешно удалены'));
    }
}