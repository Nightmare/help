<?php
class DistrictController extends AController
{
    public function actionSave()
    {
        $id = (int)$this->_getRequest()->getParam('id');
        $title = $this->_getRequest()->getParam('title');

        if (!empty($title)) {
            $district = $id ? District::model()->findByPk($id) : new District();

            if (!$district) {
                Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Район не найден, попробуйте еще раз.'));
            }

            $district->title = $title;
            $district->name = YText::translit($title);

            if ($district->save()) {
                Yii::app()->ajax->extSuccess('all cool');
            } else {
                Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Невозможно сохранить район'));
            }
        } else {
            Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Нет данных для сохранения района'));
        }
    }

    public function actionRead()
    {
        /** @var District[] $districts */
        $districts = District::model()->findAll();
        $result = array();

        foreach ($districts as $district) {
            $photo = array(
                'id' => $district->id,
                'name' => $district->name,
                'title' => $district->title,
            );

            $result[] = $photo;
        }

        Yii::app()->ajax->extSuccess($result);
    }

    public function actionDelete()
    {
        $tr = District::model()->dbConnection->beginTransaction();
        try {
            $ids = $this->_getRequest()->getPost('ids');
            foreach ($ids as $id) {
                if (!District::model()->deleteByPk($id)) {
                    Yii::app()->ajax->extFailure('Невозможно удалить район(ы)');
                    $tr->rollback();
                }
            }
            $tr->commit();
            Yii::app()->ajax->extSuccess(Yii::t('app', 'Район(ы) успешно удалены'));
        } catch (Exception $e) {
            $tr->rollback();
            Yii::app()->ajax->extFailure($e->getMessage());
        }
    }
}
