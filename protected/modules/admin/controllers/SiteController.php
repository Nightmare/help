<?php
class SiteController extends AController
{
    public function actionSave()
    {
        $title = $this->_getRequest()->getPost('title');
        $id = (int)$this->_getRequest()->getPost('id');
        $weatherType = (int)$this->_getRequest()->getPost('weather_type');
        $typeOfDay = (int)$this->_getRequest()->getPost('time_of_day');

        if (!empty($title)) {
            $site = $id ? Site::model()->findByPk($id) : new Site();

            if (!$site) {
                Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Сайт не найден, попробуйте еще раз.'));
            }

            $site->title = $title;
            $site->name = YText::translit($title);
            $site->weather_type = $weatherType;
            $site->time_of_day = $typeOfDay;

            if ($site->save()) {
                Yii::app()->ajax->extSuccess('all cool');
            } else {
                Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Невозможно сохранить сайт'));
            }
        } else {
            Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Нет данных для сохранения сайта'));
        }
    }

    public function actionRead()
    {
        $sites = Site::model()->findAll();
        Yii::app()->ajax->extSuccess($this->object_2_array($sites));
    }

    function object_2_array($data)
    {
        if (is_array($data) || is_object($data)) {
            $result = array();
            foreach ($data as $key => $value) {
                $result[$key] = $this->object_2_array($value);
            }
            return $result;
        }
        return $data;
    }

    public function actionDelete()
    {
        $tr = Site::model()->dbConnection->beginTransaction();
        try {
            $ids = $this->_getRequest()->getPost('ids');
            foreach ($ids as $id) {
                if (!Site::model()->deleteByPk($id)) {
                    Yii::app()->ajax->extFailure('Невозможно удалить сайт(ы)');
                    $tr->rollback();
                }
            }
            $tr->commit();
            Yii::app()->ajax->extSuccess(Yii::t('app', 'Сыйт(ы) успешно удалены'));
        } catch (Exception $e) {
            $tr->rollback();
            Yii::app()->ajax->extFailure($e->getMessage());
        }
    }
}