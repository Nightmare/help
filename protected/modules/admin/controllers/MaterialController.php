<?php
class MaterialController extends AController
{
    public function actionSave()
    {
        $title = $this->_getRequest()->getPost('title');
        $groupId = (int)$this->_getRequest()->getPost('group_id');
        $id = (int)$this->_getRequest()->getPost('id');
        /** @var array[] $filters */
        $filters = CJSON::decode($this->_getRequest()->getPost('filters'));

        if (!empty($groupId) && !empty($title)) {
            $filterIds = array_map(function($filter) { return $filter['id']; }, $filters);

            if ($id) {
                $material = Material::model()->findByPk($id);

                $criteria = new CDbCriteria();
                $criteria->with = array(
                    'filterValue' => array(
                        'joinType' => 'INNER JOIN',
                        'together' => true,
                        'with' => array(
                            'filter' => array(
                                'joinType' => 'INNER JOIN',
                                'with' => array(
                                    'filterMaterials' => array(
                                        'joinType' => 'INNER JOIN',
                                        'alias' => 'fm',
                                    ),
                                ),
                            ),
                        ),
                    ),
                );
                $criteria->compare('fm.material_id', $id);
                foreach (ItemFilterValue::model()->findAll($criteria) as $itemFilterValue) {
                    /** @var ItemFilterValue $itemFilterValue */
                    if (!in_array($itemFilterValue->filterValue->filter_id, $filterIds)) {
                        $itemFilterValue->delete();
                    }
                }

                FilterMaterial::model()->deleteAllByAttributes(array('material_id' => $id));
            } else {
                $material = new Material();
            }

            $material->setAttributes(array(
                'group_id' => $groupId,
                'title' => $title,
                'name' => YText::translit($title),
            ));

            if ($material->save()) {
                foreach ($filters as $filter) {
                    $filterMaterial = new FilterMaterial();
                    $filterMaterial->filter_id = $filter['id'];
                    $filterMaterial->material_id = $material->id;
                    $filterMaterial->save();
                }

                Yii::app()->ajax->extSuccess('all cool');
            } else {
                Yii::app()->ajax->extFailure($material->getErrors());
            }
        } else {
            Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Нет данных для сохранения материала'));
        }
    }

    public function actionRead()
    {
        $filter = $this->_getRequest()->getParam('q');
        $excludeTemplateId = (int)$this->_getRequest()->getParam('excludeTemplateId');

        $this->_getCriteria()->alias = 'm';
        $activeIds = array();

        if ($excludeTemplateId) {
            /** @var TemplateMaterial $templateMaterial */
            $templateMaterials = TemplateMaterial::model()->findAllByAttributes(array('template_id' => $excludeTemplateId));
            foreach ($templateMaterials as $templateMaterial) {
                $activeIds[] = $templateMaterial->material_id;
            }
        }

        if (isset($filter)) {
            $this->_getCriteria()->addSearchCondition('title', $filter);
        }

        /** @var Material[] $materials */
        $materials = Material::model()->findAll($this->_getPagerCriteria());

        $result = array();
        foreach ($materials as $material) {
            $result[] = array(
                'id' => $material->id,
                'title' => $material->title,
                'group_id' => (int)$material->group_id,
                'group_title' => $material->group->title,
                'active' => in_array($material->id, $activeIds),
            );
        }

        if ($activeIds && $result) {
            usort($result, function($a, $b) { return $b['active'] > $a['active']; });
        }

        Yii::app()->ajax->raw(
            array(
                'success' => true,
                'resultTotal' => Material::model()->count($this->_getCriteria()),
                'data' => $result,
            )
        );
    }

    public function actionDelete()
    {
        $tr = Material::model()->getDbConnection()->beginTransaction();
        try {
            $ids = $this->_getRequest()->getPost('ids');
            foreach ($ids as $id) {
                if (!Material::model()->deleteByPk($id)) {
                    Yii::app()->ajax->extFailure('Невозможно удалить материалы');
                    $tr->rollback();
                    break;
                }
            }
            $tr->commit();
            Yii::app()->ajax->extSuccess(Yii::t('app', 'Материалы успешно удалены'));
        } catch (Exception $e) {
            $tr->rollback();
            Yii::app()->ajax->extFailure($e->getMessage());
        }
    }
}