<?php
class UserController extends AController
{
    public function actionSave()
    {
        $UserPost = $this->_getRequest()->getPost('User');

        if (!empty($UserPost)) {
            $UserModel = $UserPost['id'] ? User::model()->findByPk($UserPost['id']) : new User();

            if (!$UserModel) {
                Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Пользователь не найден, попробуйте еще раз.'));
            }

            $UserModel->setAttributes($UserPost);
            if ($UserModel->save()) {
                Yii::app()->ajax->extSuccess('all cool');
            } else {
                Yii::app()->ajax->extFailure($UserModel->getErrors());
            }
        } else {
            Yii::app()->ajax->extFailure(Yii::t('BlogModule.blog', 'Нет данных для сохранения пользователя.'));
        }
    }

    public function actionRead()
    {
        $filter = $this->_getRequest()->getParam('q');

        if (isset($filter)) {
            $this->_getCriteria()
                ->addSearchCondition('nick_name', $filter)
                ->addSearchCondition('email', $filter, true, 'OR')
                ->addSearchCondition('first_name', $filter, true, 'OR')
                ->addSearchCondition('last_name', $filter, true, 'OR');
        }

        $UserModels = User::model()->findAll($this->_getPagerCriteria());

        Yii::app()->ajax->raw(
            array(
                'success' => true,
                'resultTotal' => User::model()->count($this->_getCriteria()),
                'data' => $this->object_2_array($UserModels)
            )
        );
    }

    function object_2_array($data)
    {
        if (is_array($data) || is_object($data)) {
            $result = array();
            foreach ($data as $key => $value) {
                $result[$key] = $this->object_2_array($value);
            }
            return $result;
        }
        return $data;
    }
}