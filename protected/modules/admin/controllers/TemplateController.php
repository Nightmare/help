<?php
class TemplateController extends AController
{
    public function actionRead()
    {
        $filterTitle = $this->_getRequest()->getParam('q');
        if (isset($filterTitle)) {
            $this->_getCriteria()->addSearchCondition('title', $filterTitle);
        }

        /** @var Template[] $templates */
        $templates = Template::model()->findAll($this->_getPagerCriteria());
        $result = array();
        foreach ($templates as $template) {
            $result[] = array(
                'id' => $template->id,
                'title' => $template->title,
                'info' => $template->group_id ? '1 группа' : count($template->materials) . ' материалов',
            );
        }

        Yii::app()->ajax->raw(
            array(
                'success' => true,
                'resultTotal' => Template::model()->count($this->_getCriteria()),
                'data' => $result,
            )
        );
    }

    public function actionSave()
    {
        $templateTitle = $this->_getRequest()->getParam('title');
        $templateId = (int)$this->_getRequest()->getParam('id');

        $ids = explode(',', $this->_getRequest()->getParam('ids', ''));
        $type = $this->_getRequest()->getParam('type');

        /** @var Template $template */
        if (empty($templateId)) {
            $template = new Template();
        } else {
            $template = Template::model()->findByPk($templateId);

            TemplateMaterial::model()->deleteAllByAttributes(array('template_id' => $templateId));
            $template->group_id = null;
        }

        $template->title = $templateTitle;
        $template->save();

        $typeId = $type == 'group' ? Item::TYPE_GROUP : Item::TYPE_MATERIAL;
        /** @var Item $item */
        foreach ($template->items as $item) {
            $item->type = $typeId;
            $item->save();
        }

        if ($ids) {
            if ($type == 'group') {
                $template->group_id = (int)reset($ids);
                $template->save();
            } else {
                foreach ($ids as $id) {
                    $templateMaterial = new TemplateMaterial();
                    $templateMaterial->setAttributes(array('template_id' => $template->id, 'material_id' => (int)$id));
                    $templateMaterial->save();
                }
            }

            $criteria = new CDbCriteria();
            $modelType = sprintf('filter%ss', ucfirst($type));
            $alias = 'f' . $type[0];
            $criteria->with = array(
                'filterValue' => array(
                    'joinType' => 'INNER JOIN',
                    'with' => array(
                        'filter' => array(
                            'joinType' => 'INNER JOIN',
                            'with' => array(
                                $modelType => array(
                                    'joinType' => 'INNER JOIN',
                                    'alias' => $alias,
                                ),
                            ),
                        ),
                    ),
                ),
            );
            $criteria->addNotInCondition($alias . '.' . $type . '_id', $ids);
            foreach (ItemFilterValue::model()->findAll($criteria) as $itemFilterValue) {
                $itemFilterValue->delete();
            }
        }

        Yii::app()->ajax->extSuccess('Шаблон успешно сохранен.');
    }

    public function actionDelete()
    {
        if ($ids = (array)$this->_getRequest()->getPost('ids')) {
            $criteria = new CDbCriteria();
            $criteria->addInCondition('id', $ids);
            Template::model()->deleteAll($criteria);
        }
        Yii::app()->ajax->extSuccess(Yii::t('app', 'Шаблон(ы) успешно удалены.'));
    }

    public function actionGroupMaterial()
    {
        /** @var Template $template */
        $template = Template::model()->findByPk((int)$this->_getRequest()->getParam('id'));
        $result = array();

        if (!empty($template)) {
            if ($group = $template->group(array('joinType' => 'INNER JOIN'))) {
                $result[] = array(
                    'id' => $group->id,
                    'title' => $group->title,
                    'type' => 'Группа',
                );
            } else {
                foreach ($template->materials(array('joinType' => 'INNER JOIN', 'together' => true)) as $material) {
                    $result[] = array(
                        'id' => $material->id,
                        'title' => $material->title,
                        'type' => 'Материал',
                    );
                }
            }
        }

        Yii::app()->ajax->raw(
            array(
                'success' => true,
                'data' => $result,
            )
        );
    }
}