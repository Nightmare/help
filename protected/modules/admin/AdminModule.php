<?php

class AdminModule extends CWebModule
{
    public function init()
    {
        $this->setImport(array(
            'admin.models.*',
            'admin.components.*',
        ));

        Yii::app()->theme = 'extjs';
//        $subDomain = Url::getSubDomain();
//        if (!empty($subDomain)) {
//            Yii::app()->request->redirect('http://'. Yii::app()->params['site'] .'/admin');
//        }
    }
}
