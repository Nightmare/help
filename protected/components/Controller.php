<?php
/** @method getHelper */
class Controller extends \CController
{
    const SECONDS_IN_DAY = 86400;
    public $layout = '//layouts/sideLeft';
    public $breadcrumbs = array();
    public $widgets = array();
    public $contentWidgets = array();

    /** @var Site */
    protected $_site;
    /** @var Category */
    protected $_category;
    /** @var Group */
    protected $_group;
    /** @var Material */
    protected $_material;
    /** @var Item */
    protected $_item;

    /**
     * @return Site
     */
    public function getSite()
    {
        return $this->_site;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->_category;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->_group;
    }

    /**
     * @return Material
     */
    public function getMaterial()
    {
        return $this->_material;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->_item;
    }

    public function init()
    {
        $domain = str_replace('.' . Yii::app()->params['site'], '', $_SERVER['HTTP_HOST']);
        $domain = $domain == Yii::app()->params['site'] ? '' : $domain;

        /** @var Site $site */
        $site = Site::model()->cache(self::SECONDS_IN_DAY)->findByAttributes(array('name' => $domain));

        if ((!$this->_site instanceof Site || $this->_site->name !== $domain) && !$domain) {
//            $this->redirect(array('/index/index'));
        }

        if ($site instanceof Site) {
            $this->_site = $site;
            Site::setCurrentSite($site);
        }
    }

    public function behaviors()
    {
        return array(
            'HelperBehavior' => array('class' => 'application.behaviors.HelperBehavior'),
        );
    }
}