<?php
Yii::import('application.components.CImageHandler');
class ImageUploader
{
    public function upload($path, $filename, CUploadedFile $uploader)
    {
        if (!is_dir($path)) {
            mkdir($path, 1755, true);
        }
        try {
            $uploader->saveAs($path  . '/' . $filename);
            $ih = new CImageHandler();
            $ih
                ->load($path . '/' . $filename)
                ->thumb(Photo::THUMB_SIZE, false)
                ->save($path . sprintf('/%dx%d-%s', Photo::THUMB_SIZE, Photo::THUMB_SIZE, $filename), false, 100)
                ->reload()
                ->adaptiveThumb(Photo::THUMB_SMALL_SIZE, Photo::THUMB_SMALL_SIZE)
                ->save($path . sprintf('/%dx%d-%s', Photo::THUMB_SMALL_SIZE, Photo::THUMB_SMALL_SIZE, $filename), false, 100)
                ->reload()
                ->thumb(Photo::THUMB_LARGE_SIZE, Photo::THUMB_LARGE_SIZE)
                ->save($path . sprintf('/%dx%d-%s', Photo::THUMB_LARGE_SIZE, Photo::THUMB_LARGE_SIZE, $filename), false, 100)
                ->reload()
                ->thumb(Photo::THUMB_ITEM_SIZE_WIDTH, Photo::THUMB_ITEM_SIZE_HEIGHT)
                ->save($path . sprintf('/%dx%d-%s', Photo::THUMB_ITEM_SIZE_WIDTH, Photo::THUMB_ITEM_SIZE_HEIGHT, $filename), false, 100);
        } catch (Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            return false;
        }
        return true;
    }

    public function uploadIcon($path, $filename, CUploadedFile $uploader)
    {
        if (!file_exists($path)) {
            mkdir($path, 1755, true);
            mkdir($path.'thumbs/');
            mkdir($path.'thumbs_small/');
        }
        $uploader->saveAs($path.$filename);
        //Используем функции расширения CImageHandler;
        $ih = new CImageHandler();
        $ih
            ->load($path.$filename)
            ->thumb(32, 32)
            ->save($path . $filename);
    }
}