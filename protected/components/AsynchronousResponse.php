<?php
class AsynchronousResponse extends CApplicationComponent
{
    public $success = true;
    public $failure = false;
    public $extSuccess = 'success';
    public $resultParamName = 'success';
    public $dataParamName = 'data';

    public function init()
    {
        parent::init();
        header('Content-type: application/json; charset=utf-8');
    }

    public function success($data = null)
    {
        echo json_encode(array(
            $this->resultParamName => $this->success,
            $this->dataParamName => $data,
        ));

        Yii::app()->end();
    }

    public function failure($data = null)
    {
        echo json_encode(array(
            $this->resultParamName => $this->failure,
            $this->dataParamName => $data,
        ));

        Yii::app()->end();
    }

    public function raw($data)
    {
        echo json_encode($data);
        Yii::app()->end();
    }

    public function rawText($data)
    {
        echo $data;
        Yii::app()->end();
    }

    public function extSuccess($data = null)
    {
        echo json_encode(array(
            $this->extSuccess => true,
            $this->dataParamName => $data,
        ));

        Yii::app()->end();
    }

    public function extFailure($data = null)
    {
        echo json_encode(array(
            $this->dataParamName => $data,
        ));

        Yii::app()->end();
    }
}