<?php
class TabView extends CTabView
{
    /**
     * Registers the needed CSS and JavaScript.
     */
    public function registerClientScript()
    {
        $cs = Yii::app()->clientScript;
        $cs->registerScriptFile('yiiTabLive');
        $id = $this->getId();
        $cs->registerScript('Yii.TabView#' . $id, "jQuery(\"#{$id}\").yiiTabLive();", CClientScript::POS_END);

        if ($this->cssFile !== false) {
            self::registerCssFile($this->cssFile);
        }
    }
}